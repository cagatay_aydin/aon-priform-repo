function [out,meta] = load_ni_whisper_file(fname,memorymap,writable)
% LOAD_WHISPER_FILE Maps a whisper file to memory using memmap
% [out,meta] = load_whisper_file(fname,writable)
% Writable is true or false.
% To plot the first 10000 samples of channel 1:
%       plot(out.Data.data(1,1:100000))
% To save the first minute:
%       data = double(out.Data.data(:,[1:60*meta.srate]));
%       data = meta.smpl2mvolt(data);
%       srate = meta.srate;
%       save('datafile.mat','data','srate')
% 
% See also LOAD_BINARY_FILE
%
% Joao Couto, April 2015. NERF 

if ~exist('memorymap','var')
    memorymap = false;
end
if ~exist('writable','var')
    writable = false;
end

[foldername,filename,ext] = fileparts(fname);
% Load whisper file
[foldername,'/',filename,'.meta'];
mm = fopen([foldername,'/',filename,'.meta'],'r');

txt = fgetl(mm);
meta.range = [];
while isstr(txt);
    txt = fgetl(mm);
%     if strfind(txt,'nChans')
    if strfind(txt,'nSavedChans')
        tmp = strsplit(txt,'=');
        meta.nchan = str2num(tmp{end});
%     elseif strfind(txt,'sRateHz')
    elseif strfind(txt,'imSampRate')
        tmp = strsplit(txt,'=');
        meta.srate = str2num(tmp{end});
%     elseif strfind(txt,'range')
    elseif strfind(txt,'imAiRangeMax')
        tmp = strsplit(txt,'=');
        meta.range = [meta.range,str2num(tmp{end})];
    elseif strfind(txt,'imAiRangeMin')
        tmp = strsplit(txt,'=');
        meta.range = [meta.range,str2num(tmp{end})];
%     elseif strfind(txt,'auxGain') 
     end
end
fclose(mm);
srate = meta.srate;
nchans = meta.nchan;
meta.smpl2mvolt = @(x)x*1e6/meta.gain*sum(abs(meta.range))/2^16;
out = load_binary_file(fname,meta.nchan,[],[],memorymap,writable);


