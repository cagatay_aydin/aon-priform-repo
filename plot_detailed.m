clear all
close all
clc


%
varnames = {'exp_name','ts','session_name',...
    'stim_type',...
    'ts_trials',...
    'f0','trial_type','trial_idx',...
    'chemical_list','left_right','trial_occur',...
    'recording_site','sig_val','sig','resp_h','mod_ind_fam',...
    'mod_ind_nov'};
dataset = 'lr';
foldername = '.';
[data] = get_data(foldername,dataset,varnames);
disp('Data loaded.')
%%

cc = colormap(jet(16));
printfigure = true;

for iexpt = 1:length(data)
    %     keyboard
    dd = data(iexpt);
    
    ttypes = unique(dd.trial_type);
    ntypes = length(ttypes);
    
    if ~isempty(dd.ts)
        for c = 1:length(dd.ts)
            if sum(dd.f0{c})>0
                sorted_f0 = dd.f0{c}(dd.trial_idx);
                tf0 = [];
                sf0 = [];
                axmeanfr = [];
                
                count = 0;
                for ii = 1:ntypes
                    it = ttypes(ii);
                    tf0(ii) = mean(sorted_f0(dd.trial_type==it));
                    sf0(ii) = std(sorted_f0(dd.trial_type==it))./sqrt(sum(dd.trial_type==it));
                    count = count+sum(dd.trial_type==it);
                    line_sep(ii) = count;
                    
                end
                clf
                
                axCCG = axes('position',[.3,.8,.1,.1]);
                tmpspk = dd.ts{c};
                [xLin, nLin, xLog, nLog] = myACG(tmpspk,[],axCCG);
                
                sig = nansum(dd.sig(c,:),2);
                sig_resp = dd.resp_h(c);
                %             keyboard
                sig_val = nanmean(dd.sig_val(c,:),2);
                reg = dd.recording_site{c};
                
                axmeanfr = axes('position',[.1,.8,.1,.1]);
                %             ax = [ax,subplot(3,2,1)];
                errorbar(1:length(tf0),tf0,sf0);
                
                title(sprintf('exp %s cell %d site %s',dd.exp_name,c,reg),'fontsize',7)
                ylabel('Firing rate (spikes/s)','fontsize',7)
                set(axmeanfr(end),'xtick',[1:ntypes])
                xlim([0.9 ntypes+.1])
                
                ax = axes('position',[.5,.1,.10,.60]);
                
                title(sprintf('sig %d sig_resp %1.5f',sig,sig_resp),'fontsize',7)
                
                
                %             position, [leftx,lefty,width, height]
                axraster = axes('position',[.1,.1,.1,.6]);
                sorted_trials = dd.ts_trials{c}(dd.trial_idx);
                if  length(cell2mat(sorted_trials))<30
                    continue
                else
                    plot_rastergram(sorted_trials,0,1,'convimg','color',[0 0 0]),hold all
                end
                for ii = 1:length(line_sep)
                    plot([1 5000],[line_sep(ii) line_sep(ii)],'color',cc(ii,:),'linewidth',1)
                end
                plot([-1 -1],[0 find(dd.trial_type==7,1,'last')],'k')
                set(ax(end),'visible','off')
                keyboard
                
                
                set(ax,'box','off','fontsize',7)
                if printfigure
                    set(gcf,'papersize',[29,21],'paperposition',[0,0,29,21],'color','white','paperunits','centimeters')
                    figfolder = 'figures/cell_rasters';
                    %         animal = '170912_JMCA001';
                    print(gcf,'-dpdf',sprintf('%s/%s_cell_%d_cond_%s.pdf',figfolder,dd.exp_name,c,reg))
                    
                end
                
            end
            keyboard
        end
    end
    
    
end

%% check left right selectivity
rres = [];
rste = [];
lres = [];
ste = [];
rpeak = [];
lpeak = [];
for iexpt = 1:length(data)
    
    dd = data(iexpt);
    
    ttypes = unique(dd.trial_type);
    ntypes = length(ttypes);
    
    if ~isempty(dd.ts)
        for c = 1:length(dd.ts)
            sorted_f0 = dd.f0{c}(dd.trial_idx);
            tf0 = [];
            sf0 = [];
            
            count = 0;
            for ii = 1:ntypes
                it = ttypes(ii);
                
                tf0(ii) = mean(sorted_f0(dd.trial_type==it));
                
                sf0(ii) = std(sorted_f0(dd.trial_type==it))./sqrt(sum(dd.trial_type==it));
                count = count+sum(dd.trial_type==it);
                line_sep(ii) = count;
                
            end
            
            %             keyboard
            rres = [rres mean(tf0(2:7))];
            rpeak = [rpeak max(tf0(2:7))];
            
            rste = [rste std(tf0(2:7))/sqrt(length(rres))];
            lres = [lres mean(tf0(8:end))];
            lpeak = [lpeak max(tf0(8:end))];
            lste = [lste std(tf0(8:end))/sqrt(length(lres))];
            
        end
    end
    
end

%% left right comparison

clf,
ax = [];
ax = [ax,subplot(1,2,1)];
plot(rres,lres,'ok'),hold all
mm = max([rres lres]);
xlabel(sprintf('Right\n Mean firing rate '),'fontsize',7)
ylabel('Left','fontsize',7)
title('Mean','fontsize',7)
plot([0 mm],[0 mm],'color',[.5 .5 .5])

ax = [ax,subplot(1,2,2)];
plot(rpeak,lpeak,'ok'),hold all
mm = max([rpeak lpeak]);
plot([0 mm],[0 mm],'color',[.5 .5 .5])
title('Peak','fontsize',7)
xlabel('Right','fontsize',7)
ylabel('Left','fontsize',7)


ylim([0 mm])
xlim([0 mm])
axis(ax,'square')

%% plot selectivity

all_tf0 = [];
all_sf0 = [];
all_af0 = [];
for iexpt = 1:length(data)
    
    dd = data(iexpt);
    
    ttypes = unique(dd.trial_type);
    ntypes = length(ttypes);
    
    if ~isempty(dd.ts)
        for c = 1:length(dd.ts)
            sorted_f0 = dd.f0{c}(dd.trial_idx);
            tf0 = [];
            sf0 = [];
            
            count = 0;
            for ii = 1:ntypes
                it = ttypes(ii);
                
                tf0(ii) = mean(sorted_f0(dd.trial_type==it));
                af0{ii} = sorted_f0(dd.trial_type==it);
                sf0(ii) = std(sorted_f0(dd.trial_type==it))./sqrt(sum(dd.trial_type==it));
                count = count+sum(dd.trial_type==it);
                line_sep(ii) = count;
                
            end
            
            %             keyboard
            all_af0 = [all_af0; af0];
            all_tf0 = [all_tf0 ;tf0];
            all_sf0 = [all_sf0 ;sf0];
        end
    end
    
end

%%

for ic = 1:length(all_tf0)
    clf,
    errorbar(1:14,all_tf0(ic,:),all_sf0(ic,:))
    keyboard
end



