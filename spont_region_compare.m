clear all
close all
clc


%
varnames = {'exp_name','ts','session_name',...
    'stim_type',...
    'ts_trials',...
    'f0','trial_type','trial_idx',...
    'chemical_list','left_right','trial_occur',...
    'recording_site','mf0','sf0','novelty','blank','sig','resp_h'};
dataset = 'lr';
foldername = '.';
[data] = get_data(foldername,dataset,varnames);
disp('Data loaded.')


%%

tmp = arrayfun(@(x)(x.recording_site),data,'uniformoutput',0);
rsite = horzcat(tmp{:});

blank = logical(data(1).blank);
novelty = logical(data(1).novelty);

all_f0 = [];
all_sig = [];
all_resp = [];
all_mag = [];
for expt = 1:length(data)
    dd = data(expt);
    tmp = dd.mf0;
    if ~isempty(tmp)
        mag = [];
        for ic = 1:length(dd.ts)
            % positive and negative index
            bl = mean(dd.mf0(ic,blank),2);
            fl = mean(dd.mf0(ic,~blank),2);
            mag(ic) = (fl-bl)/fl+bl;
        end
%         keyboard
        all_mag = [all_mag,mag];
        all_f0 = [all_f0;tmp(:,1:14)];
        all_sig = [all_sig;nansum(dd.sig,2)];
        all_resp = [all_resp;dd.resp_h];
    end
    %     all_f0 = vertcat(data.mf0);
end
% all_f0 = vertcat(data.mf0);


%% with 2 groups
sig_idx = [all_sig>=1]';
resp_idx = [all_resp>0]';
% cond = {strcmp(rsite,'aon')&sig_idx,...
%     strcmp(rsite,'piriform')&sig_idx};

cond = {strcmp(rsite,'aon')&resp_idx,...
    strcmp(rsite,'piriform')&resp_idx};


% cond = {strcmp(rsite,'aon')&resp_idx&[all_mag>0],...
%     strcmp(rsite,'piriform')&resp_idx&[all_mag>0]};
bl = {};
fl = {};
n = {};

for ii = 1:length(cond)
   idx = cond{ii};
   bl{ii} = nanmean(all_f0(idx,blank),2);
   mbl(ii) = nanmean(bl{ii});
   sbl(ii) = std(bl{ii})./sqrt(length(bl{ii}));
   nbl(ii) = length(bl{ii});
   
   fl{ii} = nanmean(all_f0(idx,(~novelty&~blank)),2);
   mfl(ii) = nanmean(fl{ii});
   sfl(ii) = std(fl{ii})./sqrt(length(fl{ii}));
   
   n{ii} = nanmean(all_f0(idx,(novelty&~blank)),2);
   mn(ii) = nanmean(n{ii});
   sn(ii) = std(n{ii})./sqrt(length(n{ii}));
end


%%

clf
clib = {[146/255,71/255,149/255],[0 0 1],[63/255,143/255,56/255]};

hold on
printfigure = true;
ax = [];
ax = [ax,subplot(1,3,1)];


for ii = 1:2
    bb = bar(ii,mbl(ii));
    set(bb,'edgecolor','none','facecolor',clib{ii}),hold all
    ee = errorbar(ii,mbl(ii),sbl(ii));
    set(ee,'color','k','markerfacecolor','none');
    text(ii,8,sprintf('N=%d',nbl(ii)),'horizontalalignment','center')
end

ylabel(sprintf('Spon. firing rate\n (spikes/s)'))

ax = [ax,subplot(1,3,2)];

for ii = 1:2
    bb = bar(ii,mfl(ii));
    set(bb,'edgecolor','none','facecolor',clib{ii}), hold all
    ee = errorbar(ii,mfl(ii),sfl(ii));
    set(ee,'color','k','markerfacecolor','none');
    text(ii,8,sprintf('N=%d',nbl(ii)),'horizontalalignment','center')
end

ylabel(sprintf('Fam. firing rate\n (spikes/s)'))

ax = [ax,subplot(1,3,3)];
   

for ii = 1:2
    bb = bar(ii,mn(ii));
    set(bb,'edgecolor','none','facecolor',clib{ii}), hold all
    ee = errorbar(ii,mn(ii),sn(ii));
    set(ee,'color','k','markerfacecolor','none');
    text(ii,8,sprintf('N=%d',nbl(ii)),'horizontalalignment','center')
end

ylabel(sprintf('Nov. firing rate\n (spikes/s)'))


set(ax,'box','off','xtick',1:2,'xticklabel',{'AON','PIR'},...
'tickdir','out','ticklength',get(ax(1),'ticklength').*5),

set(gcf,'paperunits','centimeters','papersize',[4.2*3,3*1],...
    'paperposition',[0,0,4.2*3,3*1])

set(findall(gcf,'-property','FontSize'),'FontSize',8)

printfolder = 'E:\local\users\cagatay\aon-priform\figures\manuscript';
if printfigure
    print(gcf,'-dpdf',sprintf('%s\\population_compare.pdf',printfolder))
end

%% all of firing rates in the same figure

clf
ax = [];

for ii = 1:2
    ax = [ax,subplot(1,1,1)];
    ee = errorbar(1:3,[mbl(ii),mfl(ii),mn(ii)],[sbl(ii),sfl(ii),sn(ii)]);hold all,
    set(ee,'color',clib{ii},'markerfacecolor','none');
    
end

set(ax,'box','off','xtick',1:3,'xticklabel',{'BLA','FAM','NOV'},...
'tickdir','out','ticklength',get(ax(1),'ticklength').*5,...
'xlim',[.9 3.1],'ylim',[0 11]),

set(gcf,'paperunits','centimeters','papersize',[4.2*3,3*1],...
    'paperposition',[0,0,4.2*3,3*1])

set(findall(gcf,'-property','FontSize'),'FontSize',8)

printfolder = 'E:\local\users\cagatay\aon-priform\figures\manuscript';
if printfigure
    print(gcf,'-dpdf',sprintf('%s\\population_compare_both.pdf',printfolder))
end


%% with 3 groups
clib = {[146/255,71/255,149/255],[0 0 1],[63/255,143/255,56/255]};
sig_idx = [all_sig>=1]';
cond = {strcmp(rsite,'aon')&sig_idx,...
    strcmp(rsite,'endopiriform')&sig_idx,...
    strcmp(rsite,'piriform')&sig_idx};



for ii = 1:length(cond)
   idx = cond{ii};
   bl = mean(all_f0(idx,blank),2);
   mbl(ii) = mean(bl);
   sbl(ii) = std(bl)./sqrt(length(bl));
   nbl(ii) = length(bl);
   
   fl = mean(all_f0(idx,(~novelty&~blank)),2);
   mfl(ii) = mean(fl);
   sfl(ii) = std(fl)./sqrt(length(fl));
   
   n = mean(all_f0(idx,(novelty&~blank)),2);
   mn(ii) = mean(n);
   sn(ii) = std(mn)./sqrt(length(mn));
end


%%

clf
hold on
printfigure = true;
ax = [];
ax = [ax,subplot(1,3,1)];


for ii = 1:3
    bb = bar(ii,mbl(ii));
    set(bb,'edgecolor','none','facecolor',clib{ii}),hold all
    ee = errorbar(ii,mbl(ii),sbl(ii));
    set(ee,'color','k','markerfacecolor','none');
    text(ii,8,sprintf('N=%d',nbl(ii)),'horizontalalignment','center')
end

ylabel(sprintf('Spon. firing rate\n (spikes/s)'))

ax = [ax,subplot(1,3,2)];

for ii = 1:3
    bb = bar(ii,mfl(ii));
    set(bb,'edgecolor','none','facecolor',clib{ii}), hold all
    ee = errorbar(ii,mfl(ii),sfl(ii));
    set(ee,'color','k','markerfacecolor','none');
    text(ii,8,sprintf('N=%d',nbl(ii)),'horizontalalignment','center')
end

ylabel(sprintf('Fam. firing rate\n (spikes/s)'))

ax = [ax,subplot(1,3,3)];
   

for ii = 1:3
    bb = bar(ii,mn(ii));
    set(bb,'edgecolor','none','facecolor',clib{ii}), hold all
    ee = errorbar(ii,mn(ii),sn(ii));
    set(ee,'color','k','markerfacecolor','none');
    text(ii,8,sprintf('N=%d',nbl(ii)),'horizontalalignment','center')
end

ylabel(sprintf('Nov. firing rate\n (spikes/s)'))


set(ax,'box','off','xtick',1:3,'xticklabel',{'AON','E-PIR','PIR'},...
'tickdir','out','ticklength',get(ax(1),'ticklength').*5),

set(gcf,'paperunits','centimeters','papersize',[4.2*3,3*1],...
    'paperposition',[0,0,4.2*3,3*1])

set(findall(gcf,'-property','FontSize'),'FontSize',8)

printfolder = 'E:\local\users\cagatay\aon-priform\figures\manuscript';
if printfigure
    print(gcf,'-dpdf',sprintf('%s\\population_compare.pdf',printfolder))
end

