function [spks,unit_ids,ss,clu,amps,pcFeat,depths] = KILO_get_units_ts(sortfolder,sample_rate,selection)

% ss = ts ;
% clu = clu;

% Reads units from scGUI file format
% [spks,unit_ids,ts,clu] = scGUI_get_units_ts(sortfolder,selection)

spks={};unit_ids=[];ss=[];clu = [];amps = [];
% 
depths = [];
if isempty(selection);selection = 'all';end

if ~exist(sortfolder,'dir')
    disp(['Folder does not exist: ',sortfolder])
    return
end

items = dir(sortfolder);
items = {items(~[items.isdir]).name}';


% folder = sortfolder;
% ts = loadnpy(fullfile(folder,'spike_times.npy'));
% if exist(fullfile(folder,'spike_clusters.npy'),'file')
%     % Use spike_clusters
%     clu = loadnpy(fullfile(folder,'spike_clusters.npy'));
% else
%     % Use spike_templates
%     clu = loadnpy(fullfile(folder,'spike_templates.npy'));
% end
% if strcmp(selection,'all')
%     unit_ids = unique(clu);
% end
% % keyboard
% if strcmp(selection,'good')
%     if sum(strcmp(items,'cluster_groups.csv'))>0
%         out = textread(fullfile(folder,'cluster_groups.csv'), '%s');
%     else
%         out = textread(fullfile(folder,'cluster_group.tsv'), '%s');
%     end
%     
%     ids = out(1:2:end);
%     cond = out(2:2:end);
%     unit_ids = ids(find(strcmp('good',cond)));
%     unit_ids = cell2mat(cellfun(@str2num,unit_ids(1:end),'un',0));
% end
% spks = cell(length(unit_ids),1);
% for i = 1:length(unit_ids)
%     spks{i} = ts(clu == unit_ids(i));
% end
% if nargout>4
%     amps = loadnpy(fullfile(folder,'amplitudes.npy'));
% end
% if nargout>5
%     fets = loadnpy(fullfile(folder,'pc_features.npy'));
% end

ss = readNPY(fullfile(sortfolder, 'spike_times.npy'));
st = double(ss)/sample_rate;
spikeTemplates = readNPY(fullfile(sortfolder, 'spike_templates.npy')); % note: zero-indexed

if exist(fullfile(sortfolder, 'spike_clusters.npy'))
    clu = readNPY(fullfile(sortfolder, 'spike_clusters.npy'));
else
    clu = spikeTemplates;
end

amps = readNPY(fullfile(sortfolder, 'amplitudes.npy'));


    pcFeat = readNPY(fullfile(sortfolder,'pc_features.npy')); % nSpikes x nFeatures x nLocalChannels
    pcFeatInd = readNPY(fullfile(sortfolder,'pc_feature_ind.npy')); % nTemplates x nLocalChannels


pcFeat = squeeze(pcFeat(:,1,:)); % take first PC only
pcFeat(pcFeat<0) = 0; % some entries are negative, but we don't really want to push the CoM away from there.
    
    
cgsFile = '';
if exist(fullfile(sortfolder, 'cluster_groups.csv')) 
    cgsFile = fullfile(sortfolder, 'cluster_groups.csv');
end
if exist(fullfile(sortfolder, 'cluster_group.tsv')) 
   cgsFile = fullfile(sortfolder, 'cluster_group.tsv');
end 

 if strcmp(selection,'good')
    if sum(strcmp(items,'cluster_groups.csv'))>0
        out = textread(cgsFile, '%s');
    else
        out = textread(cgsFile, '%s');
    end
    
    ids = out(1:2:end);
    cond = out(2:2:end);
    unit_ids = ids(find(strcmp('good',cond)));
    unit_ids = cell2mat(cellfun(@str2num,unit_ids(1:end),'un',0));
 end
 
[cids, cgs] = readClusterGroupsCSV(cgsFile);
 

coords = readNPY(fullfile(sortfolder, 'channel_positions.npy'));
ycoords = coords(:,2); xcoords = coords(:,1);
temps = readNPY(fullfile(sortfolder, 'templates.npy'));


% which channels for each spike?
spikeFeatInd = pcFeatInd(spikeTemplates+1,:);

% ycoords of those channels?
spikeFeatYcoords = ycoords(spikeFeatInd+1); % 2D matrix of size #spikes x 12
% center of mass is sum(coords.*features)/sum(features)
spikeDepths = sum(spikeFeatYcoords.*pcFeat.^2,2)./sum(pcFeat.^2,2);

% 

spks = cell(length(unit_ids),1);
depths = nan(length(unit_ids),1);
for i = 1:length(unit_ids)
    spks{i} = ss(clu == unit_ids(i));
    tmp_sp = clu(clu==unit_ids(i));
    tmp_depth = spikeDepths(clu==unit_ids(i));
    if isempty(tmp_sp)
        depths(i) = nan;
    else
    depths(i) = clusterAverage(tmp_sp,tmp_depth);
    end
end



