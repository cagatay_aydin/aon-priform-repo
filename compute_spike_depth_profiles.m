clear all
close all
clc

%%
parent_folder = 'E:\local\users\cagatay\kilosort-aon-pir\';
ext_KILO_folder = 'ext_KILO_data\';
events_folder = 'events_data\';
d = dir(sprintf('%s%s',parent_folder,ext_KILO_folder));
isub = [d(:).isdir];
nameFolds = {d(isub).name}';
nameFolds(ismember(nameFolds,{'.','..'})) = [];

%%

depthBins = 0:40:3840;
window = [-2 5]; % look at spike times from 0.3 sec before each event to 1 sec after
depthBinSize =80; % in units of the channel coordinates, in this case �m
timeBinSize = 0.02; % seconds
bslWin = [-2 -0.5]; % window in which to compute "baseline" rates for normalization
psthType = 'norm'; % show the normalized version
eventName = 'stimulus onset'; % for figure labeling
printfigure = 1;

for ii =7:length(nameFolds)
    sp = {};
    
    myKsDir = sprintf('%s%s%s',parent_folder,ext_KILO_folder,nameFolds{ii});
    sp = loadKSdir(myKsDir);
    [spikeTimes, spikeAmps, spikeDepths, spikeSites] = ksDriftmap(myKsDir,'true');
    ampBins = 0:30:min(max(spikeAmps),800);
    recordingDur = sp.st(end);
        
%     [pdfs, cdfs] = computeWFampsOverDepth(spikeAmps, spikeDepths, ampBins, depthBins, recordingDur);
%     plotWFampCDFs(pdfs, cdfs, ampBins, depthBins);

    tmp = list_files(sprintf('%s%s%s',parent_folder,events_folder,nameFolds{ii}),'*.mat');
    tm = load(tmp{1});
    
    a = tm.allTriggersS{1}{1};
    all_events = a(diff(a)>.015);
    eventTimes = all_events(2:4:end);
       

    [timeBins, depthBins, allP, normVals] = psthByDepth(spikeTimes, spikeDepths, ...
        depthBinSize, timeBinSize, eventTimes, window, bslWin);
    
    plotPSTHbyDepth(timeBins, depthBins, allP, eventName, psthType);
    title(sprintf('%s',nameFolds{ii}),'interpreter','none')
    set(gcf,'paperunits','centimeters','papersize',[4.2*3,4.2*3],...
        'paperposition',[0,0,4.2*3,4.2*3])
    
    set(findall(gcf,'-property','FontSize'),'FontSize',8)
    
    printfolder = 'E:\local\users\cagatay\kilosort-aon-pir\figures\spike_depth_profile\';
    if printfigure
        print(gcf,'-dpdf',sprintf('%s%s_selec.pdf',printfolder,nameFolds{ii}))
    end
    clf
    
end



%%



%%

myKsDir = 'E:\local\users\cagatay\kilosort-aon-pir\ext_KILO_data\181122_MB052';
sp = loadKSdir(myKsDir);
%%
[spikeTimes, spikeAmps, spikeDepths, spikeSites] = ksDriftmap(myKsDir,'true');
figure; plotDriftmap(spikeTimes, spikeAmps, spikeDepths);

%%

depthBins = 0:40:3840;
ampBins = 0:30:min(max(spikeAmps),800);
recordingDur = sp.st(end);

[pdfs, cdfs] = computeWFampsOverDepth(spikeAmps, spikeDepths, ampBins, depthBins, recordingDur);
plotWFampCDFs(pdfs, cdfs, ampBins, depthBins);

%%

proc_dir = 'E:\local\users\cagatay\kilosort-aon-pir\processed_data\181122_MB052\181122_MB052_behaviourdata_lr.mat';
a = load(proc_dir);
eventTimes = a.onsets;

%%

[timeBins, depthBins, allP, normVals] = psthByDepth(spikeTimes, spikeDepths, ...
    depthBinSize, timeBinSize, eventTimes, window, bslWin);

figure;
plotPSTHbyDepth(timeBins, depthBins, allP, eventName, psthType);