

clear all
close all
clc

% get spike times
% animal = '181116_CA111';
% animal = '190809_MB085';
% animal = '190822_MB083_1';
animal = '190828_MB100_2';
sortfolder = 'ext_KILO_data\';
tmpfolder = sprintf('%s%s',sortfolder,animal);
[units, ts, waves, fets, probeinfo, param] = processDataFromKILO_oe(fullfile(tmpfolder),...
    'ATLAS', 2000, {'AON'}, {14}, 20.0);

%%

eventfolder = 'events_data\';
tmp = list_files(sprintf('%s%s',eventfolder,animal),'*.mat');

tall = [];
lall = [];
for ii = 1:length(tmp)
   a = load(tmp{ii},'time_68');
   tall = [tall; a.time_68];
   lall = [lall; length(a.time_68)];
%    keyboard
end

%%
min_time = min(tall);
abs_time = tall - min_time;

sel_session = 2;
ss = load(tmp{2});  
% onsets = ss.onsets_68-min_time;

srate = 30000;
start_time = min(ss.time_68)-min_time;
% start_time = (lall(sel_session))/srate;
onsets = (ss.onsets_69-min(ss.time_69))+start_time;


%%
% ffile = 'E:\local\users\cagatay\opto_aon\raw_oedata\190809_MB085\test.mat'
% ffile = 'E:\local\users\cagatay\opto_aon\raw_oedata\190822_MB083_1\test.mat'
ffile = 'E:\local\users\cagatay\opto_aon\raw_oedata\190828_MB100_2\test.mat'
ss = load(ffile);
% onsets = [ss.onsets_69(1:500:750000)]';
%%
limm = [1*30000:(length(ss.raw_68)/30000)*30000];
sdat = diff(ss.raw_68(limm));
tdat = ss.time_68(limm);
tdat = tdat(1:length(sdat));

 th = mean(abs(sdat)).*1000;
    [tmp_val, tmp_idx]=findpeaks(sdat,tdat,...
        'minpeakprominence',th);

clf,plot(tdat,sdat),hold on
plot(tmp_idx,tmp_val,'ro')
% onsets = tmp_idx';

%%

ssdat =ss.raw_69(limm);
ttdat = ss.time_69(limm);

th = mean(ssdat);
    [ttmp_val, ttmp_idx]=findpeaks(ssdat,ttdat,...
        'minpeakprominence',th);
    
asd = [];
for ii= 1:length(tmp_idx)
    asd = [asd,ttmp_idx(find(ttmp_idx>tmp_idx(ii),[1],'first'))];    
end

fth = diff(asd)<10;
tsd = asd(logical([1,~fth]));
clf,plot(ttdat,ssdat),hold on
plot(tsd,ones(1,length(tsd)).*max(ssdat)/2,'mo')

onsets = tsd';
%%

% th = mean(abs(sdat));
%     [tmp_val, tmp_idx]=findpeaks(sdat,tdat,...
%         'minpeakprominence',th);
% 
% clf,plot(tdat,sdat),hold on
% plot(tmp_idx,tmp_val,'ro'),hold on
% tht = find(diff(tmp_idx)>3&diff(tmp_idx)<=150);
% plot(tmp_idx(tht),tmp_val(tht),'bo')
% % onsets = tmp_idx';


%%

ncells = length(ts);
ts_trials = cell(length(ts),1);
ob = 1;
oa = 12;
for c = 1:ncells
    ts_trials{c} = extract_trial_spikes(ts{c},onsets-ob,onsets+oa);
    
    [f0{c},f1{c},f2{c}]=compute_fourier_visual_responses(ts_trials{c},...
        [oa+ob],[0],5,1);
end


%%
rr = [];
for ii = 1:length(onsets)
    tidx = find(ss.time_68>onsets(ii)-ob& ss.time_68<onsets(ii)+oa);
    trr = ss.raw_69(tidx);
    rr = [rr;trr];
%     keyboard
    
end
%%
cc = colormap(jet(22));
ntrials = length(onsets);
% ttypes = unique(nn);
% ntypes = length(ttypes);
% 
% figure,

% ic = 7;
printfigure = true;
for ic = 1:ncells
        clf
    ax = [];
    
    ax = [ax,subplot(5,1,1:4)];
    [r, i] =  sort(f0{ic},'descend');
    sorted_trials = ts_trials{ic};
    if length(vertcat(sorted_trials{:}))<30
        continue
    else
    sel_trials = length(sorted_trials);
    plot_rastergram(sorted_trials,0,1,'line','color',[0 0 0]),hold all
%     line([2000 2000],[1 160]);
%     line([min(xlim) max(xlim)],[sel_trials sel_trials]);
    count = 0;
    
    text(mean(xlim),min(ylim),sprintf('Cell#%d',ic),'horizontalalignment','center',...
        'verticalalignment','bottom','fontsize',7)
    xlabel('time(ms)')
      

    
    ax = [ax,subplot(5,1,5)];
    
%     sorted_trials = ts_trials{ic}(i);
    sigma = 1;
    x = -2*sigma:2*sigma;
    kernel = exp(-x .^ 2 / (2*sigma ^ 2)); kernel = kernel./sum(kernel);

    % bar(edges,bins,'histc')
    clear avgresp edges
    avgresp = [];
    sel_trials = length(sorted_trials);
    for ii = 1:sel_trials
        spks = cell2mat(sorted_trials(ii));
        edges = 0:0.1:ob+oa;
        bin = histcounts(spks,edges);
        temp = conv(bin,kernel,'same');
        avgresp = [avgresp;temp];
    end
    mb = nanmean(avgresp);
    sb = nanstd(avgresp)./sqrt(size(avgresp,1));
%    clf,
    shadedErrorBar(edges(1:end-1),mb,sb,{'color',[0.5 0.5 0.5]});hold all
    plot([2 2],[0,max(ylim)])
    xlim ([min(xlim)+0.5 max(xlim)-0.5])
    ylim([0 max(ylim)])
    
    if printfigure
        set(gcf,'papersize',[3*3,3*3],'paperposition',[0,0,3*3,3*3],'color','white','paperunits','centimeters')
        figfolder = 'figures/';
%         animal = '170912_JMCA001';
        print(gcf,'-dpdf',sprintf('%s/%s/cell_%d_cond.pdf',figfolder,animal,ic))
    end
    end
end


%% plot nicer

bonsets = [onsets;onsets+4;onsets+6;onsets+8;onsets+10];
ncells = length(ts);
ts_trials = cell(length(ts),1);
ob = 0.1;
oa = 1.1;
for c = 1:ncells
    ts_trials{c} = extract_trial_spikes(ts{c},bonsets-ob,bonsets+oa);
    
    [f0{c},f1{c},f2{c}]=compute_fourier_visual_responses(ts_trials{c},...
        [oa+ob],[0],5,1);
end


%% compute monitor signal

  rr = [];
    [yy, tty] = resample(ss.raw_69,ss.time_69,500);
    for ii = 1:length(bonsets)
        tidx = find(tty>bonsets(ii)-ob& tty<bonsets(ii)+oa);
        trr = yy(tidx);
        rr = [rr;trr];
    end
%%


cc = colormap(jet(22));
ntrials = length(onsets);
% ttypes = unique(nn);
% ntypes = length(ttypes);
% 
% figure,

% ic = 7;
printfigure = true;
llist = [8,11,15,17,18,19,20,29,30,31,32,33,35,39,47];
for ic = 1:ncells%llist
        clf
    ax = [];
    
    
    ax = [ax,subplot(4,2,[1,3,5,7])];    
  
    imagesc(flipud(rr));
    cc = colormap('gray');
    colormap(flipud(cc));
    set(ax(end),'visible','off')
    
    
    ax = [ax,subplot(4,2,[2,4,6,8])];
    [r, i] =  sort(f0{ic},'descend');
    sorted_trials = ts_trials{ic};
    if length(vertcat(sorted_trials{:}))<30
        continue
    else
    sel_trials = length(sorted_trials);
    plot_rastergram(sorted_trials,0,1,'line','color',[0 0 0]),hold all
    axis('tight')
%     line([2000 2000],[1 160]);
%     line([min(xlim) max(xlim)],[sel_trials sel_trials]);
    count = 0;
    
    text(mean(xlim),min(ylim),sprintf('Cell#%d',ic),'horizontalalignment','center',...
        'verticalalignment','bottom','fontsize',7)
    xlabel('time(ms)')
      

%     
%     ax = [ax,subplot(5,1,5)];
%     
% %     sorted_trials = ts_trials{ic}(i);
%     sigma = 1;
%     x = -2*sigma:2*sigma;
%     kernel = exp(-x .^ 2 / (2*sigma ^ 2)); kernel = kernel./sum(kernel);
% 
%     % bar(edges,bins,'histc')
%     clear avgresp edges
%     avgresp = [];
%     sel_trials = length(sorted_trials);
%     for ii = 1:sel_trials
%         spks = cell2mat(sorted_trials(ii));
%         edges = 0:0.1:ob+oa;
%         bin = histcounts(spks,edges);
%         temp = conv(bin,kernel,'same');
%         avgresp = [avgresp;temp];
%     end
%     mb = nanmean(avgresp);
%     sb = nanstd(avgresp)./sqrt(size(avgresp,1));
% %    clf,
%     shadedErrorBar(edges(1:end-1),mb,sb,{'color',[0.5 0.5 0.5]});hold all
%     plot([2 2],[0,max(ylim)])
%     xlim ([min(xlim)+0.5 max(xlim)-0.5])
%     ylim([0 max(ylim)])

set(ax,'fontsize',7,'box','off')
    
    if printfigure
        set(gcf,'papersize',[3*3,3*2],'paperposition',[0,0,3*3,3*2],'color','white','paperunits','centimeters')
        figfolder = 'figures/';
%         animal = '170912_JMCA001';
        print(gcf,'-dpdf',sprintf('%s/%s/good_cell_%d_cond.pdf',figfolder,animal,ic))
    end
    end
end

