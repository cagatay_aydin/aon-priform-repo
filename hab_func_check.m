clear all
close all
clc


% [hab_resp, hab_ste] = compute_habituation(condition,noccur,trial_type,trial_idx,trial_occur,f0)
noccur_check = 5;
nodors = 15;
create_noccur = 10;

condition =  [0;0;0;1;1;1;1;0;0;0;1;1;1;1;0];
trial_occur = repmat([1:create_noccur],[1,nodors]);
trial_type = [];for ii = 1:15; trial_type = [trial_type,ones(1,create_noccur).*ii]; end
trial_idx = 1:length(trial_type);

f0 = [] ; for ii = 1:15; f0 = [f0,([10:-1:1]+rand(1,10))]; end
%%
[a,b] = compute_habituation(logical(condition),noccur_check,trial_type,trial_idx,trial_occur,f0)


errorbar(1:noccur_check,a,b)

%%

clear all
close all
clc


%
varnames = {'exp_name','ts','session_name',...
    'stim_type',...
    'ts_trials',...
    'f0','trial_type','trial_idx',...
    'chemical_list','left_right','trial_occur',...
    'recording_site','mf0','sf0','novelty','blank','sig','resp_h'};
dataset = 'lr';
foldername = '.';
[data] = get_data(foldername,dataset,varnames);

disp('Data loaded.')

%%


tmp = arrayfun(@(x)(x.recording_site),data,'uniformoutput',0);
rsite = horzcat(tmp{:});
blank = logical(data(1).blank);
novelty = logical(data(1).novelty);
familiar = logical(~data(1).novelty&~data(1).blank);
%%

all_hab = [];
all_fam = [];
ste_hab = [];
ste_fam = [];
all_sig = [];
norm_hab = [];
norm_fam = [];
all_resp_sig = [];

for iexpt = 1:length(data)
    %     keyboard
    dd = data(iexpt);
    if ~isempty(dd.f0)
        
        oc = dd.trial_occur;
        ttype = dd.trial_type;
        tidx = dd.trial_idx;
        
        noccur = 15;%max(unique(oc));
        hab_resp = nan(length(dd.ts),noccur);
        norm_hab_resp = nan(length(dd.ts),noccur);
        hab_ste = nan(length(dd.ts),noccur);
        fam_resp = nan(length(dd.ts),noccur);
        norm_fam_resp = nan(length(dd.ts),noccur);
        fam_ste = nan(length(dd.ts),noccur);
        sig = nan(length(dd.ts),1);
        resp_sig = nan(length(dd.ts),1);
        mag = nan(length(dd.ts),1);
        
        for ic = 1:length(dd.ts)
            resp_sig(ic) = dd.resp_h(ic);
            sf0 = dd.f0{ic}(tidx);
            
            [hab_resp(ic,:),~] = compute_habituation(novelty,noccur,ttype,oc,sf0);
            [fam_resp(ic,:),~] = compute_habituation(familiar,noccur,ttype,oc,sf0);
            
              check_till = 6;
        norm_hab_resp(ic,1:check_till) =  (hab_resp(ic,1:check_till)-nanmin(hab_resp(ic,1:check_till)))...
            ./(nanmax(hab_resp(ic,1:check_till))-nanmin(hab_resp(ic,1:check_till)));
        
        norm_fam_resp(ic,1:check_till) =  (fam_resp(ic,1:check_till)-nanmin(fam_resp(ic,1:check_till)))...
            ./(nanmax(fam_resp(ic,1:check_till))-nanmin(fam_resp(ic,1:check_till)));
            
        end
        
        
    else
        continue
    end
    norm_hab = [norm_hab;norm_hab_resp];
    norm_fam = [norm_fam;norm_fam_resp];
    all_hab = [all_hab; hab_resp];
    all_fam = [all_fam; fam_resp];
    ste_hab = [ste_hab;hab_ste];
    ste_fam = [ste_fam;fam_ste];
    all_sig = [all_sig;sig];
    all_resp_sig = [all_resp_sig;resp_sig];
    
    
end

%%

printfigure = false;
clf,
clib = {[146/255,71/255,149/255],[63/255,143/255,56/255]};

resp_idx = [all_resp_sig>0]';
cond = {strcmp(rsite,'aon')&resp_idx,...
    strcmp(rsite,'piriform')&resp_idx};

for c = 1:length(cond)
    idx = cond{c};
    mm = nanmean(norm_hab(idx,:));
    ss = nanstd(norm_hab(idx,:))./sqrt(sum(idx));
    sum(idx)
    e = errorbar(1:15,mm,ss);hold all
    set(e,'color',clib{c},'linewidth',1,'capsize',2)
    
    %     mm = nanmean(norm_fam(idx,:));
    %     ss = nanstd(norm_fam(idx,:))./sqrt(sum(idx));
    %     sum(idx)
    %     e = errorbar(1:15,mm,ss);hold all
    %     set(e,'color',[.5 .5 .5],'markerfacecolor',clib{c},'linewidth',1)
    
end

ylabel(sprintf('Firing rate (Normalized)'),'fontsize',7)
xlabel(sprintf('Novel odor presentation \n(#count)'),'fontsize',7)
set(gca,'xlim',[0.5 6.5],'box','off','tickdir','out',...
    'ticklength',get(gca(1),'ticklength').*5,'xtick',[1:6],'ylim',[0.2 0.8])

axis('square')
set(gcf,'paperunits','centimeters','papersize',[4.2*1,4.2*1],...
    'paperposition',[0,0,4.2*1,4.2*1])

set(findall(gcf,'-property','FontSize'),'FontSize',8)

printfolder = 'E:\local\users\cagatay\aon-priform\figures\manuscript';
if printfigure
    print(gcf,'-dpdf',sprintf('%s\\novel_habituation_cumsum_compare.pdf',printfolder))
end
