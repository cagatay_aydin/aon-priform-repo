 clear all
close all
clc


%
varnames = {'exp_name','ts','session_name',...
    'stim_type','events',...
    'ts_trials','ts',...
    'f0','trial_type','trial_idx',...
    'chemical_list','left_right','trial_occur',...
    'recording_site','mf0','sf0','novelty','blank','sig',...
    'resp_h','sig_val',...
    'cam','breath'};
dataset = 'lr';
foldername = '.';
[data] = get_data(foldername,dataset,varnames);
disp('Data loaded.')



%%


nbins = 12;
samp_freq=60;
plotfigure = false;
printfigure = false;

allang = [];
allmag = [];
alltune = [];
all_f0 = [];
all_sig = [];
all_resp = [];
rsite = [];
allrespdir = [];

for expt =  1:length(data)
dd = data(expt);

dd.cam.onsets  =dd.cam.onsets+min(dd.events.raw);
tt = dd.mf0;

if ~isempty(dd.breath)&&~isempty(tt)

tmp = arrayfun(@(x)(x.recording_site),data(expt),'uniformoutput',0);
rsite = [rsite, horzcat(tmp{:})];
all_f0 = [all_f0;tt(:,1:14)];
all_sig = [all_sig;nansum(dd.sig,2)];
all_resp = [all_resp;dd.resp_h];

aligned_onsets = dd.events.raw(2:4:end)';
diffevents = dd.events.onsets- aligned_onsets(1:length(dd.events.onsets));
recwind =  length(dd.breath.cam_resp{1});
if recwind == 0
    recwind = 663;
end

ntrials = length(diffevents);
tstamps = nan(ntrials,recwind);
finhtime = nan(ntrials,1);
sinhtime = nan(ntrials,1);
tinhtime = nan(ntrials,1);
breathcycle = nan(ntrials,2*(nbins-1));
breathtime = {};

for it = 1:ntrials-1
    tstamps(it,:)= linspace(dd.cam.onsets(it),dd.cam.onsets(it)+(recwind/samp_freq),recwind);
    inhstamps = tstamps(it,dd.breath.inh_onsets{it});
    finhtime(it) = inhstamps(find(inhstamps>aligned_onsets(it)+0.3,1,'first'));
    sinhtime(it) = inhstamps(find(inhstamps>finhtime(it),1,'first'));
    tinhtime(it) = inhstamps(find(inhstamps>sinhtime(it),1,'first'));
    tmpb = dd.breath.cam_resp{it};
%     eachcycle = tmpb(tstamps(it,:)>(finhtime(it))&tstamps(it,:)<(tinhtime(it)));
%     
%     ori = linspace(0,(tinhtime(it)-finhtime(it)),length(eachcycle));
%     des = linspace(0,(tinhtime(it)-finhtime(it)),2*(nbins-1));
%     ibre = interp1(ori,zscore(eachcycle),des);
% %     eachtime = tstamps(tstamps(it,:)>(finhtime(it)-.5)&tstamps(it,:)<(finhtime(it)+1));
% %     linspace()
%     breathcycle(it,:) = ibre;
%     breathtime{it} = eachtime-nanmin(eachtime)';
end
ncells = length(dd.ts);

ang = nan(ncells,1);
tune = nan(ncells,1);
mag = nan(ncells,1);

for ic = 1:length(dd.ts)

tts_trials{ic} = extract_trial_spikes(dd.ts{ic},finhtime-.5,finhtime+1);
[b, in] = sort(sinhtime-finhtime,'ascend');

tmp_spk = {};
for ij = 1:length(tts_trials{ic})
    tmp_spk{ij} = tts_trials{ic}{ij}-diffevents(ij);
end

sniffsortedspk = tmp_spk(in)';
diffinh = (sinhtime-finhtime);
sdiffinh = (tinhtime-finhtime);

spks = sniffsortedspk;
cyclestimate = nan(2*(nbins-1),length(spks));
for trial = 1:length(spks)
    
    edges = linspace(0.5,diffinh(trial)+.5,nbins)';
    dt = min(diff(edges));
    if isempty(spks{trial})
        tmp1 = zeros(1,length(edges)-1);
    else
        tmp1 = histcounts(spks{trial}, edges)./dt;
    end
        
    edges = linspace(diffinh(trial)+.5,sdiffinh(trial)+.5,nbins)';
    dt = min(diff(edges));
    if isempty(spks{trial})
        tmp2 = zeros(1,length(edges)-1);
    else
        tmp2 = histcounts(spks{trial}, edges)./dt;
    end
    
    if ~isempty(tmp1)
%          cyclestimate(:,trial) = tmp1';
         cyclestimate(:,trial) = [tmp1, tmp2]';
    end
end

% keyboard

sigma = 5*1e-3;
binsize = 4;
dt = binsize*1e-3;

kernel = normpdf(-sigma*5:dt:5*sigma,0,sigma);
kernel = kernel/sum(kernel);

mm = mean(cyclestimate(1:end-1,:)');
convolved = conv(mm,kernel,'same');
piedges = linspace(0,4*pi,2*(nbins-1));hold all
degeges = rad2deg(piedges);

for ij = 1:length(degeges)-1
    axx(ij) = mm(ij)*cos(degeges(ij)*pi/180);
    ayy(ij) = mm(ij)*sin(degeges(ij)*pi/180);
end

x = sum(axx);
y = sum(ayy);

mag(ic) = (x^2+y^2)^(1/2);
if atan2(y,x)<0
    ang(ic) = 360+atan2(y,x)*180/pi;
else
    ang(ic) = atan2(y,x)*180/pi;
end
tune(ic) = mag(ic)/sum(mm);


if plotfigure
    ax = [];
    clf
    ax = [ax,subplot(1,3,1)];
    plot_rastergram(sniffsortedspk,0,1,'line'),hold all
    plot(diffinh(in)+.5,1:length(sinhtime),'color',[.7 .7 .7])
    plot(sdiffinh(in)+.5,1:length(sinhtime),'color',[.7 .7 .7])
    plot([0.5 0.5],[1 length(sinhtime)],'k')
    set(ax(end),'xlim',[0 1.5],'ylim',[0 length(sinhtime)],'box','off')

    ax = [ax subplot(1,3,2)];
%     plot(piedges(1:end-1),mm);
hold all
    plot(piedges(1:end-1),convolved)
    plot(piedges,mean(breathcycle)*mean(convolved)/2,'color',[0 0 0])
    plot(piedges(1:end-1),(cos(piedges(1:end-1))*4+mean(mm)),'color',[.5 .5 .5])
    set(ax(end),'xlim',[0 12])
    axis(ax(end),'tight')
    
    ax = [ax,subplot(1,3,3)];
    polar([piedges],[mm mm(1)]),hold on
    polar(deg2rad([ang(ic) ang(ic)]),[0 mag(ic)])
    title(sprintf('%2.2f ',tune(ic)))
    
    keyboard
        
    set(ax,'box','off','tickdir','out',...
    'ticklength',get(ax(end),'ticklength').*5)

    
    if printfigure 
%         axis(ax,'tight')
    set(gcf,'paperunits','centimeters','papersize',[4.2*3,4.2*1],...
    'paperposition',[0,0,4.2*3,4.2*1])

set(findall(gcf,'-property','FontSize'),'FontSize',8)

printfolder = 'E:\local\users\cagatay\aon-priform\figures\manuscript\sniff_example';
if printfigure
    print(gcf,'-dpdf',sprintf('%s\\sniff_dataset_%s_cell_%d_reg_%s.pdf',printfolder,dd.exp_name,ic,dd.recording_site{1}))
end

        
    end
    
end


end
allmag = [allmag; mag];
alltune = [alltune; tune];
allang = [allang; ang];
end
end

%%
printfigure = true;
clf
clib = {[146/255,71/255,149/255],[63/255,143/255,56/255]};
resp_idx = [all_resp>0]';
% cond = {strcmp(rsite,'aon')&sig_idx,...
%         strcmp(rsite,'piriform')&sig_idx};
cond = {strcmp(rsite,'aon')&resp_idx,...
        strcmp(rsite,'piriform')&resp_idx};
edges = 0:30:360;
ax = [];
mm = {};
for c = 1:length(cond)
    idx = cond{c};
    mm{c} = allang(idx);
    bins=histc(mm{c},edges);
    ss = cumsum(bins);
    ss = ss/max(ss);
    ax = [ax,subplot(2,1,1)];
    plot(edges,ss,'color',clib{c},'linewidth',1);hold all
    ax = [ax,subplot(2,1,2)];
    b(c) = bar(edges,bins/sum(bins));hold all
    set(b(c),'edgecolor',clib{c},'facecolor','none')
end

[h, p] = kstest2(mm{1},mm{2})
set(ax,'box','off','tickdir','out',...
    'ticklength',get(ax(end),'ticklength').*5)

set(gcf,'paperunits','centimeters','papersize',[4.2*1,4.2*2],...
    'paperposition',[0,0,4.2*1,4.2*2])

set(findall(gcf,'-property','FontSize'),'FontSize',8)

printfolder = 'E:\local\users\cagatay\aon-priform\figures\manuscript';
if printfigure
    print(gcf,'-dpdf',sprintf('%s\\sniff_angle_aon_pir.pdf',printfolder))
end

%%
clf
ax = [];

ax = [ax,subplot(2,1,1)];
idx = cond{1};
rose(deg2rad(allang(idx)));
ax = [ax,subplot(2,1,2)];
idx = cond{2};
rose(deg2rad(allang(idx)));

%%
clf
ax = [];

ax = [ax,subplot(2,1,1)];
idx = cond{1};
polar(deg2rad(allang(idx)),ones(sum(idx),1),'*');
title(sprintf('%d',sum(idx)))
ax = [ax,subplot(2,1,2)];
idx = cond{2};
polar(deg2rad(allang(idx)),ones(sum(idx),1),'*');
title(sprintf('%d',sum(idx)))




%% tuning
clf
clib = {[146/255,71/255,149/255],[63/255,143/255,56/255]};
resp_idx = [all_resp>0]';
% cond = {strcmp(rsite,'aon')&sig_idx,...
%         strcmp(rsite,'piriform')&sig_idx};
cond = {strcmp(rsite,'aon')&resp_idx,...
        strcmp(rsite,'piriform')&resp_idx};
edges = 0:0.02:1;
ax = [];
mm = {};
for c = 1:length(cond)
    idx = cond{c};
    mm{c} = alltune(idx);
    bins=histc(mm{c},edges);
    ss = cumsum(bins);
    ss = ss/max(ss);
    ax = [ax,subplot(2,1,1)];
    plot(edges,ss,'color',clib{c},'linewidth',1);hold all
    set(ax(end),'ylim',[0 1])
    ax = [ax, subplot(2,1,2)];
    b(c) = bar(edges,bins/sum(bins));hold all
    set(b(c),'edgecolor',clib{c},'facecolor','none')
end

[h, p] = kstest2(mm{1},mm{2})
set(ax,'box','off','tickdir','out',...
    'ticklength',get(ax(end),'ticklength').*5)

set(gcf,'paperunits','centimeters','papersize',[4.2*1,4.2*2],...
    'paperposition',[0,0,4.2*1,4.2*2])

set(findall(gcf,'-property','FontSize'),'FontSize',8)

printfolder = 'E:\local\users\cagatay\aon-priform\figures\manuscript';
if printfigure
    print(gcf,'-dpdf',sprintf('%s\\sniff_tuning_aon_pir.pdf',printfolder))
end
