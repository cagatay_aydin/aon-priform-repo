function [mean_bursts,bursts] = compute_bursts(ts,trials)

% COMPUTE BURSTS Computes number of burst as a fraction and returns
% spiketimes cellarray for a visual stimulus
% ts = dd.ts_trials{c,t};
% c =  cell index
% t = stimulus index eg. 1st stimulus
%[mean_bursts,bursts]=compute_bursts(ts,trials)
%
%
% Joao Couto and Cagatay Aydin
% May 2017

burst_count = 0;
isis = cellfun(@diff,ts(trials),'uniformoutput',0);
            
bursts = cell(size(isis));
for b = 1:length(isis)
    burst_idx = find(isis{b}<0.004);
    for ii = 1:length(burst_idx);
        try
            if isis{b}(burst_idx(ii)+1)>0.1
                burst_count = burst_count+1;
                bursts{b} = [bursts{b},ts{b}(burst_idx(ii))];
            end
        end
    end
end
 mean_bursts = burst_count./length(trials);