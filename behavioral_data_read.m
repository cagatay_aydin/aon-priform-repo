cd E:\local\users\cagatay\stimulation\first_batch_pir

olfactometer_list = [22 23 24 25 26 29 30 31 32 33];

fam = logical([0 1 1 0 0 0 1 1 0 0 ]);
nov = logical([0 0 0 1 1 0 0 0 1 1 ]);
opt = logical([0 0 0 0 0 1 0 0 0 0 ]);
bla = logical([1 0 0 0 0 0 0 0 0 0 ]);


%%

sum([fam;nov;opt;bla])

%%

tmp = list_files(pwd);
 ic = 1;
 hab_resp = {};
 hab_ste = {};
for expt = 1:length(tmp)
    
    dd = load(fullfile(tmp{expt}));
    
    ttype = dd.trial.odor_ID;
    ntype = unique(ttype);
    oc = dd.trial.count;
    noccur = 40; %max number of representations
    
    if length(ntype)<10
        continue
    else
        %       keyboard
       
        %compute number of sniffs for each trial
        stim_on = dd.times.odor_ON/1000;
        cam_on = dd.times.video_start/1000;
        
        ntrials = length(stim_on);
        recwind =  length(dd.signal.breath(1).cam_resp)+2;
        tstamps = nan(ntrials,recwind);
        sf0 = nan(ntrials,1);
        
        samp_freq = 60;
        
        for it = 1:ntrials
            tstamps(it,:)= linspace(cam_on(it),cam_on(it)+(recwind/samp_freq),recwind);
            inhstamps = tstamps(it,dd.signal.breath(it).inh);
%             base = length(inhstamps(find(inhstamps>stim_on(it)-4.3 & inhstamps<stim_on(it)+0.3)))./4;
             base = 0;
            resp = length(inhstamps(find(inhstamps>stim_on(it)+0.3 & inhstamps<stim_on(it)+3.3)))./3;
            sf0(it) = resp - base;
        end
        
        
        cond = {ntype(nov),ntype(fam),ntype(bla),ntype(opt)};
        
        for icond = 1:length(cond)
        novel_odors = cond{icond};
        ocidx = nan(noccur,length(novel_odors));
        
        for io = 1:noccur
            
            for in = 1:length(novel_odors)
                try
                    n = novel_odors(in);
                    ocidx(io,in) = find(io==oc&ttype == n);
                    odidx(io,in) = ttype(find(io==oc&ttype==n));
                catch fprintf('no trials for %d rep for odor %d\n',io,n)
                end
                
            end
            
            idx = ocidx(io,:);
            if sum(isnan(idx))>0
                idx = idx(~isnan(idx));
                hab_resp{icond}(ic,io) = mean(sf0(idx));
                hab_ste{icond}(ic,io) = std(sf0(idx))./sqrt(length(idx));
                
            else if sum(isnan(idx))==length(novel_odors)
                    hab_resp{icond}(ic,io) = nan;
                    hab_ste{icond}(ic,io) = nan;
                    
                else
                    hab_resp{icond}(ic,io)= nanmean(sf0(idx));
                    hab_ste{icond}(ic,io)= nanstd(sf0(idx))./sqrt(length(idx));
                end
            end
            
        end
        
        end
        ic = ic+1;
%         keyboard
    end
end

%%

max_all = max(max(vertcat(hab_resp{:})));
min_all = min(min(vertcat(hab_resp{:})));
ttitle = {'Novel','Familiar','Blank','Stim'}

clf,
ax = [];
for ii = 1:length(cond)
    
    ax = [ax,subplot(1,length(cond),ii)];
    
    mm = nanmean(hab_resp{ii});
    ss = nanstd(hab_resp{ii})./sqrt(length(hab_resp{ii}));
    e = errorbar(1:noccur,mm,ss);hold all
    set(e,'color',[0 0 0]) 
    plot([0 noccur],[0 0],'k')
    title(sprintf('%s',ttitle{ii}))
end

axis(ax,'square');

set(ax,'xlim',[0 noccur],'ylim',[min_all max_all],'box','off','tickdir','out')


%%
% clf
nob =1:40;
ss = mean(hab_resp{4});
nn = mean(hab_resp{3});
plot(ss(nob)-nn(nob)),hold on

%%
