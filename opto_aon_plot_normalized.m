clear all
close all
clc


%
varnames = {'exp_name','ts','session_name',...
    'stim_type',...
    'ts_trials',...
    'f0','trial_type','trial_idx',...
    'chemical_list','left_right','trial_occur',...
    'recording_site','mf0','sf0','novelty','blank','sig',...
    'sig_val','resp_h','mod_ind_fam','mod_ind_nov','ot','ol','inh','events'};
dataset = 'lr';
foldername = '.';
[data] = get_data(foldername,dataset,varnames);
disp('Data loaded.')


%%

tmp = arrayfun(@(x)(x.recording_site),data,'uniformoutput',0);
rsite = horzcat(tmp{:});

blank = logical(data(1).blank);
novelty = logical(data(1).novelty);

all_f0 = [];
all_opto = [];
all_inh = [];
all_lat = [];
all_sig = [];
all_resp_sig = [];
all_mag = [];
all_bsp = [];
all_mod_ind_nov = [];
all_mod_ind_fam = [];

% stimdur = 3;
minaf = 3;
miner=1;
stimdur = miner+minaf;
binsize = 0.02;
edges = 0:binsize:stimdur;

sigma = 4;
x = -4*sigma:4*sigma;
kernel = exp(-x .^ 2 / (2*sigma ^ 2)); kernel = kernel./sum(kernel);

for expt = 1:length(data)
    dd = data(expt);
    mag = nan(length(dd.ts),1);
    if ~isempty(tmp)
        bin_resp = nan(length(dd.ts),length(edges)-1);
        for ic = 1:length(dd.ts)
            bsp = nan(length(edges),1);
            bl_bsp = nan(length(edges),1);
            % positive and negative index
            bl = mean(dd.mf0(ic,blank),2);
            fl = mean(dd.mf0(ic,~blank),2);
            mag(ic) = (fl-bl)/fl+bl;
            
%            [~,pidx] = nanmax(dd.mf0(ic,:)); %find the most significant response
%            tidx = find(dd.trial_type==pidx);
        
           
            %compute cycle averages
            finhtime = dd.events.onsets;
            tts_trials{ic} = extract_trial_spikes(dd.ts{ic},finhtime-miner,finhtime+stimdur);
            bl_trials{ic} = extract_trial_spikes(dd.ts{ic},finhtime-stimdur-miner,finhtime);
            %             keyboard
            bsp = nan(length(edges),1);
            bl_bsp = nan(length(edges),1);
            % positive and negative index
            bl = mean(dd.mf0(ic,blank),2);
            fl = mean(dd.mf0(ic,~blank),2);
            mag(ic) = (fl-bl)/fl+bl;
            %             keyboard
            %            [~,pidx] = nanmax(dd.mf0(ic,:)); %find the most significant response
            %            tidx = find(dd.trial_type==pidx);
            ttrial = unique(dd.trial_type);
            
%             sel_ttrial = [2,3,9,10];
          
%             r_idx = [find(dd.trial_type==ttrial(2));find(dd.trial_type==ttrial(3));...
%                 find(dd.trial_type==ttrial(4));find(dd.trial_type==ttrial(5));...
%                 find(dd.trial_type==ttrial(6));find(dd.trial_type==ttrial(7));...
%                 find(dd.trial_type==ttrial(9));find(dd.trial_type==ttrial(10));...
%                 find(dd.trial_type==ttrial(11));find(dd.trial_type==ttrial(12));...
%                 find(dd.trial_type==ttrial(13));find(dd.trial_type==ttrial(14))];
            
                   
                        
            tmp_ts = cell2mat(tts_trials{ic}(dd.trial_idx));
            %            tmp_ts = cell2mat(dd.ts_trials{ic});
            bsp = histcounts(tmp_ts,edges)./length(dd.trial_idx);
            bsp_conv = nan(1,length(bsp));
            bsp_conv = squeeze(conv(bsp,kernel,'same'));
            
       %             bsp = histcounts(tmp_ts,edges)./length(dd.trial_idx(r_idx));
%             bsp_conv = nan(1,length(bsp));
%             bsp_conv = squeeze(conv(bsp,kernel,'same'));
%             
%             tmp_bl = cell2mat(bl_trials{ic}(dd.trial_idx(r_idx))');
%             %            tmp_ts = cell2mat(dd.ts_trials{ic});
%             bsp = histcounts(tmp_bl,edges)./length(dd.trial_idx(r_idx));
%             bsp_bl = nan(1,length(bsp));
%             bsp_bl = squeeze(conv(bsp,kernel,'same'));
%             tmp_resp = bsp_conv-bsp_bl;

            bin_resp(ic,:) = smooth(((bsp_conv-min(bsp_conv))*2)./(max(bsp_conv)-min(bsp_conv))-1,15)';
            
            %            bin_resp(ic,:) = (tmp_resp-min(tmp_resp))*2./(max(tmp_resp)-min(tmp_resp))-1;
         

       
        end
        all_bsp = [all_bsp;bin_resp];
        all_mag = [all_mag;mag];
        all_sig = [all_sig;nansum(dd.sig,2)];
        all_resp_sig = [all_resp_sig;dd.resp_h];
        all_mod_ind_fam = [all_mod_ind_fam,dd.mod_ind_fam];
        all_mod_ind_nov = [all_mod_ind_nov,dd.mod_ind_nov];
        all_opto = [all_opto,dd.ot];
        all_inh = [all_inh,dd.inh];
        all_lat = [all_lat,dd.ol];
    end
%     all_f0 = vertcat(data.mf0);
end
% all_f0 = vertcat(data.mf0);

%%

% resp_idx = [all_resp_sig>0]';

% cond = {strcmp(rsite,'aon')&sig_idx&resp_idx,...
%     strcmp(rsite,'piriform')&sig_idx&resp_idx};
cond = {[strcmp(rsite,'aon')],...
    strcmp(rsite,'piriform')};
a = nan(length(all_resp_sig),1);
a(find (strcmp(rsite,'aon')))=1;
% a(find (strcmp(rsite,'aon')&resp_idx))=1;
% a(find (strcmp(rsite,'piriform')&resp_idx))=2;

% cell_idx = cond{1}|cond{2};
cell_idx = cond{1};
dat = all_bsp(cell_idx,:);
expind = a(cell_idx);
mm_nov = all_mod_ind_nov(cell_idx);
mm_fam = all_mod_ind_fam(cell_idx);
mm_opto = all_opto(cell_idx);
mm_inh = all_inh(cell_idx);
mm_lat = all_lat(cell_idx);
% imagesc(all_bsp(cond{1}|cond{2},:))
% dat = all_bsp(cond{1}|cond{2},:);
isnan(mean(dat))
%%
clf


linewidth = 1;
ncluster = 12;
clib = jet(ncluster+5);
[c, ~,sumd]= kmeans(dat,ncluster,'Replicates',10);
idx = 1:length(c);
[v,ii] = sort(c);
sortedidx = idx(ii);
sorted_cycle_avg = dat(sortedidx,:);
clind = arrayfun(@(x) find(v==x,1,'last'),unique(v));
printfigure = false;


clf
clib = jet(ncluster+5);
%  {[146/255,71/255,149/255],[0 0 1],[63/255,143/255,56/255]};
% PLOT CYCLE AVERAGES ALL
axclusters = axes('position',[.02,.2,.28,.77]);
imagesc([sorted_cycle_avg, zeros(length(idx),3)],[-1,1]),hold all
% colormap(darkb2r(-1,1))
% colormap(b2r(-1,1))
colormap(hot)

% colormap(gray)
set(axclusters,'visible','off','box','off')
% colorbar
ttlen = get(gca,'ticklength')*5;
cb = colorbar('location','manual','position',[0.92,0.04,0.01,0.1],...
    'ticks',[-1,0,1],'tickdirection','out',...
    'ticklength',ttlen(1),'ticklabels',{'-1','0','1'});
xlabel(cb,sprintf('Normalized\nFiring Rate'))

plot(1:size(dat,2),repmat(clind,1,size(dat,2)),'color',[1,1,1],'linewidth',1)
axis tight

tmp_time = linspace(-1,stimdur,size(sorted_cycle_avg,2));
zz = find(tmp_time>=0,1,'first');
plot([zz,zz],ylim,'w--','linewidth',1)


cc = lines(max(expind));
expinds = zeros(length(expind),1,3);

% for i = 1:2
%     expinds(i==expind,:,:) = repmat(cc(i,:),[sum(expind == i),1,1]);
% end

% axexp = axes('position',[.31,.01,.01,.98]);
% imagesc(expinds(sortedidx,:,:),[0 1]),hold all
% set(axexp,'box','off','visible','off')
% text(min(xlim),min(ylim),sprintf('Exp'),'fontsize',7)

% PLOT MEAN CYCLE AVERAGES
% 
% 
% m_ax = [0.70,0.45,0.20];
% 
% % axmeanresponses = axes('position',[.35,.54,.24,.47]);
% hold all
% cont = 1;
% cutbins = 3;
% for kk = length(unique(v)):-1:1%1:length(unique(v))
%     mean_resp = nanmean(sorted_cycle_avg(v==kk,:));
%     axmeanresponses = axes('position',[.35,m_ax(kk),.15,.10]); hold on
%     
%     plot(tmp_time,mean_resp,'color',clib(kk,:),'linewidth',3),
%     plot([2 4],[-0.9 -0.9],'k')
%     text(3,-0.9,sprintf('2s'))
%     text(-1,-0.9,sprintf('n=%d',sum(v==kk)))
%     plot([5 5],[0 1],'k')
%     plot([0,0],[-1 1],'color','r')
%     plot([-1,5],[0 0],'--','color',[.5 .5 .5])
%     set (axmeanresponses,'visible','off')
%     set(gca,'fontsize',7,'tickdir','out','ylim',[-1 1],...
%         'xlim',[min(tmp_time)+0.4 max(tmp_time)-0.5],'color','none')
%     cont = cont+1;
% end



% axmodf0 = axes('position',[.68,.50,.1,.2]);
% for kk = length(unique(v)):-1:1    
%     all_idx = sortedidx(find(v==kk)');
%     edges = [-1:binsize:2];
%     tempdd{kk} = mm_nov(all_idx);
%     bins = histc(tempdd{kk},edges);
%     ss = cumsum(bins);
%     ss = ss/max(ss);
%     plot(edges,ss,'color',clib(kk,:),'linewidth',linewidth),hold all,
%     ylim([0,1.01])
%     xlim([min(edges),max(edges)])
%     %         set(gca,'color','none','box','off','fontsize',8)
% end
% ylabel('CDF','fontsize',8)
% xlabel('Modulation (%)','fontsize',8)
% title('F0','fontweight','bold','fontsize',8)
% set(axmodf0,'fontsize',8,'box','off','tickdir','out',...
%     'color','none','ticklength',get(gca,'ticklength')*5,'xtick',[-1 0 1 2],...
%     'xticklabel',[-100 0 100 200])

% PLOT RESPONSE MODULATION (F1)
% 
% axmodf1 = axes('position',[.85,.5,.1,.2]);
% for kk = length(unique(v)):-1:1
%     all_idx = sortedidx(find(v==kk'));
%     edges = [-1:binsize:2];
%     tempdd{kk} = mm_fam(all_idx);
%     bins = histc(tempdd{kk},edges);
%     ss = cumsum(bins);
%     ss = ss/max(ss);
%     plot(edges,ss,'color',clib(kk,:),'linewidth',linewidth),hold all,
%     ylim([0,1.01])
%     xlim([min(edges),max(edges)])
%     %         set(gca,'color','none','box','off','fontsize',8)
% end
% 
% 
% title('F1','fontweight','bold')
% set(axmodf1,'fontsize',8,'box','off','tickdir','out',...
%     'color','none','ticklength',get(gca,'ticklength')*5,'xtick',[-1 0 1 2],...
%     'xticklabel',[-100 0 100 200])


% 
% m2_ax = [0.70,0.45,0.20];
% axmodf0 = [];
% % axmodf0 = axes('position',[.68,.50,.1,.2]);
% for kk = length(unique(v)):-1:1
%     all_idx = sortedidx(find(v==kk)');
%     edges = [-1:binsize:2];
%     tempdd{kk} = mm_nov(all_idx);
%     bins = histc(tempdd{kk},edges);
%     ss = cumsum(bins);
%     ss = ss/max(ss);
%     axmodf0 = [axmodf0,axes('position',[.58,m2_ax(kk),.1,.2])]; hold on
%     plot(edges,ss,'color',clib(kk,:),'linewidth',linewidth),hold all,
%     tempfd{kk} = mm_fam(all_idx);
%     bins = histc(tempfd{kk},edges);
%     ss = cumsum(bins);
%     ss = ss/max(ss);
%     plot(edges,ss,'color',[.5 .5 .5],'linewidth',linewidth),hold all,
%     ylim([0,1.01])
%     xlim([min(edges),1])
%     [h p] = kstest2(tempdd{kk},tempfd{kk});
%     text(0.1,0.1,sprintf('p = %1.4f',p),'fontsize',8);
%     %         set(gca,'color','none','box','off','fontsize',8)
% end
% axis(axmodf0,'square')
% ylabel('CDF','fontsize',8)
% % xlabel('Modulation (%)','fontsize',8)
% % title('F0','fontweight','bold','fontsize',8)
% set(axmodf0,'fontsize',8,'box','off','tickdir','out',...
%     'color','none','ticklength',get(gca,'ticklength')*5,'xtick',[-1 0 1],...
%     'xticklabel',[-100 0 100])


% m2_ax = [0.80,0.55,0.30];
% axmodf0 = [];

axmodopto = axes('position',[.85,.70,.1,.2]);
for kk = length(unique(v)):-1:1    
    all_idx = sortedidx(find(v==kk)');
    edges = [-1:binsize:2];
    tempdd{kk} = mm_opto(all_idx);
    bb = bar(kk,sum(tempdd{kk}));hold on
    set(bb,'facecolor',clib(kk,:),'edgecolor','none')
    
%     bins = histc(tempdd{kk},edges);
%     ss = cumsum(bins);
%     ss = ss/max(ss);
%     plot(edges,ss,'color',clib(kk,:),'linewidth',linewidth),hold all,
%     ylim([0,1.01])
%     xlim([min(edges),max(edges)])
    %         set(gca,'color','none','box','off','fontsize',8)
end
axis(axmodopto,'square')
ylabel('# cells','fontsize',8)
% xlabel('Modulation (%)','fontsize',8)
title('Opto-tagged','fontweight','bold','fontsize',8)
set(axmodopto,'fontsize',8,'box','off','tickdir','out',...
    'color','none','ticklength',get(gca,'ticklength')*5)


axinh = axes('position',[.85,.45,.1,.2]);
td = [];
for kk = length(unique(v)):-1:1    
    all_idx = sortedidx(find(v==kk)');
    edges = [-1:binsize:2];
    t_all = sum(mm_opto(all_idx));
    t_inh = sum(mm_inh(all_idx)&mm_opto(all_idx));
    td= [td; t_inh/t_all (1-t_inh/t_all)];

%     bins = histc(tempdd{kk},edges);
%     ss = cumsum(bins);
%     ss = ss/max(ss);
%     plot(edges,ss,'color',clib(kk,:),'linewidth',linewidth),hold all,
%     ylim([0,1.01])
%     xlim([min(edges),max(edges)])
    %         set(gca,'color','none','box','off','fontsize',8)
end
bb = bar([length(td):-1:1],td,'stacked'),hold on;
    set(bb(1),'facecolor','k','edgecolor','k')
    set(bb(2),'facecolor','w','edgecolor','k')
ylabel('fraction','fontsize',8)
axis(axinh,'square')
% xlabel('Modulation (%)','fontsize',8)
title('Inhibitory','fontweight','bold','fontsize',8)
set(axinh,'fontsize',8,'box','off','tickdir','out',...
    'color','none','ticklength',get(gca,'ticklength')*5)



axlat = axes('position',[.85,.20,.1,.2]);
td = [];
for kk = length(unique(v)):-1:1    
    all_idx = sortedidx(find(v==kk)');
    edges = [0:.1:10];
    tempdd{kk} = mm_lat(all_idx)*1000;

    bins = histc(tempdd{kk},edges);
    ss = cumsum(bins);
    ss = ss/max(ss);
    plot(edges,ss,'color',clib(kk,:),'linewidth',linewidth),hold all,
%     ylim([0,1.01])
%     xlim([min(edges),max(edges)])
    %         set(gca,'color','none','box','off','fontsize',8)
end
axis(axlat,'square')

ylabel('CDF','fontsize',8)
xlabel('Latency (ms)','fontsize',8)
% title('Inhibitory','fontweight','bold','fontsize',8)
set(axlat,'fontsize',8,'box','off','tickdir','out',...
    'color','none','ticklength',get(gca,'ticklength')*5)



if printfigure
set(gcf,'paperunits','centimeter','paperposition',[0,0,22,12],'papersize',[22,12])
foldername = 'figures\';
fname = sprintf('%s\\cycle_average_clusters_sta_only%d%s.pdf',...
    foldername,ncluster,'aon');
print(gcf,'-dpdf','-painters','-loose',...
    fname)
end