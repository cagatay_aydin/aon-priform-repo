clear all
close all
clc


%
varnames = {'exp_name','ts','session_name',...
    'stim_type','f0',...
    'ts_trials',...
    'f0','trial_type','trial_idx',...
    'chemical_list','left_right','trial_occur',...
    'recording_site','mf0','sf0','novelty','blank','sig','resp_h'};
dataset = 'lr';
foldername = '.';
[data] = get_data(foldername,dataset,varnames);
disp('Data loaded.')


%%



all_accuracy = [];
all_recall = [];
all_precision = [];
all_auc = [];
all_y = [];

for ii = 1:length(data)
    dd = data(ii);
    ttype = unique(dd.trial_type);

    tmp_identity = nan(length(dd.f0{1}),1);
    for it = 1:length(ttype)
        tt = ttype(it);
        if dd.novelty(it)==1 
           tmp_identity(find(tt==dd.trial_type&(dd.trial_occur<=4)))=1;
           tmp_identity(find(tt==dd.trial_type&(dd.trial_occur>4)))=0;
      else if ~dd.novelty(it)&&~dd.blank(it)
            tmp_identity(find(tt==dd.trial_type))=0;
            else
                tmp_identity(find(tt==dd.trial_type))=nan;
            end
        end
    end
%     keyboard
    

accuracy  = [];
recall = [];
precision = [];
tmp_y = [];
auc = [];
rep = 1;
    for ic =1:length(dd.f0)

%         we will do the ananylsis
        ntrials = length(dd.trial_idx);
        sorted_f0 = dd.f0{ic}(dd.trial_idx);
        
        tmp_accuracy = [];
        tmp_recall = [];
        tmp_precision = [];
        ttmp_y = [];
        tmp_auc = [];
        
        for ir = 1:rep
            
            
        dummy_index = randperm(ntrials)';
        twperc = dummy_index(1:floor(ntrials*20/100));
        eiperc = dummy_index(floor(ntrials*20/100)+1:end);
        
        mdl = fitglm(sorted_f0(eiperc),tmp_identity(eiperc),'distribution','binomial');
        ypred = predict(mdl,sorted_f0(twperc)');
        
        dummy_compare = nan(length(ypred),1);
        dummy_compare(ypred>=0.5)=1;
        dummy_compare(ypred<0.5)=0; 
        
        nanidx = find(~isnan(tmp_identity(twperc)));
%         compare_array = [tmp_identity(twperc(nanidx)), dummy_compare(nanidx)];
        
        stat = confusionmatStats(tmp_identity(twperc(nanidx)), dummy_compare(nanidx));
        tmp_accuracy(ir) = stat.accuracy - 1;
        tmp_recall(ir) = stat.recall(1);
        tmp_precision(ir) = stat.precision(1);
        
        
%         tmp_analysis= mdl.Coefficients.tStat(2);
         try 
             [X,Y,T,AUC]=perfcurve(tmp_identity(twperc(nanidx)), ypred(nanidx), 1);
         catch fprintf('not enough observation %s cell:%d\n',dd.exp_name,ic)
         end
         tmp_auc(ir) = AUC;
         
         tmp_x = linspace(0,1,100)';
         [cc,ia,~] = unique(X);
         ttmp_y(ir,:) = spline(X(ia),Y(ia),tmp_x);
         
        end 
        accuracy(ic) = mean(tmp_accuracy);
        recall(ic) = mean(tmp_recall);
        precision(ic) = mean(tmp_precision);
        auc(ic) = mean(tmp_auc);
%         keyboard
        tmp_y(ic,:) = mean(ttmp_y,1);
         
%         keyboard
%         cured_data = [cured_data;accuracy_neuron];
    end

    all_accuracy = [all_accuracy ;accuracy'];
    all_recall = [all_recall;recall'];
    all_precision = [all_precision ;precision'];
    all_auc = [all_auc;auc'];
    all_y = [all_y;tmp_y];
    
%     keyboard
    clear dd
end

%% novel 1 familiar 0 blank nan


tmp = arrayfun(@(x)(x.recording_site),data,'uniformoutput',0);
rsite = horzcat(tmp{:});


all_sig = [];
all_resp = [];
for expt = 1:length(data)
    dd = data(expt);
    tmp = dd.mf0;
    if ~isempty(tmp)
    all_sig = [all_sig;nansum(dd.sig,2)];
    all_resp = [all_resp;dd.resp_h];
    end

end

resp_idx = [all_resp>0]';
%%

clf

clib = {[146/255,71/255,149/255],[63/255,143/255,56/255]};
cond = {strcmp(rsite,'aon')&resp_idx,...
        strcmp(rsite,'piriform')&resp_idx};

ax = [];
mm = {};
tmp1 = {};
tmp2 = {};
tmp3 = {};
tmp4 = {};

edges = [0:0.02:1];
ll = 1;
printfigure = true;

for c = 1:length(cond)
    
    tmp1{c} = (all_accuracy(cond{c}));
    bb = histcounts(tmp1{c},edges);
    s = cumsum(bb);
    s = s/max(s);
    ax = [ax, subplot(2,4,1)];
    plot(edges(1:end-1),bb,'color',clib{c},'linewidth',ll),hold on
    plot([0 0],[0 8],'--')
    title('accuracy')
    ax = [ax, subplot(2,4,5)];
    plot(edges(1:end-1),s,'color',clib{c},'linewidth',ll),hold on
    plot([0 0],[0 1],'--')
    
    
    tmp2{c} = (all_recall(cond{c}));
    bb = histcounts(tmp2{c},edges);
    s = cumsum(bb);
    s = s/max(s);
    ax = [ax, subplot(2,4,2)];
    plot(edges(1:end-1),bb,'color',clib{c},'linewidth',ll),hold on
    plot([0 0],[0 8],'--')
    title('recall')
    ax = [ax, subplot(2,4,6)];
    plot(edges(1:end-1),s,'color',clib{c},'linewidth',ll),hold on
    plot([0 0],[0 1],'--')
    
    tmp3{c} = (all_precision(cond{c}));
    bb = histcounts(tmp3{c},edges);
    s = cumsum(bb);
    s = s/max(s);
    ax = [ax, subplot(2,4,3)];
    plot(edges(1:end-1),bb,'color',clib{c},'linewidth',ll),hold on
    plot([0 0],[0 8],'--')
    title('precision')
    ax = [ax,subplot(2,4,7)];
    plot(edges(1:end-1),s,'color',clib{c},'linewidth',ll),hold on
    plot([0 0],[0 1],'--')
    
    
    tmp4{c} = (all_auc(cond{c}));
    bb = histcounts(tmp4{c},edges);
    s = cumsum(bb);
    s = s/max(s);
    ax = [ax, subplot(2,4,4)];
    plot(edges(1:end-1),bb,'color',clib{c},'linewidth',ll),hold on
    plot([0 0],[0 8],'--')
    title('auc')
    ax = [ax, subplot(2,4,8)];
    plot(edges(1:end-1),s,'color',clib{c},'linewidth',ll),hold on
    plot([0 0],[0 1],'--')
    
%     b(c) = bar(edges(1:end-1),bb);
%     set(b(c),'edgecolor',clib{c},'facecolor','none'),hold on
end

[~, p1] = kstest2(tmp1{1},tmp1{2})
[~, p2] = kstest2(tmp2{1},tmp2{2})
[~, p3] = kstest2(tmp3{1},tmp3{2})
[~, p4] = kstest2(tmp4{1},tmp4{2})

set(ax,'box','off','tickdir','out',...
    'ticklength',get(ax(end),'ticklength').*5,'xlim',[min(edges),max(edges)],...
    'linewidth',0.5)

set(ax,'xlim',[.5,1])


axis(ax,'square')
set(gcf,'paperunits','centimeters','papersize',[4.2*4,4.2*2],...
    'paperposition',[0,0,4.2*4,4.2*2])

set(findall(gcf,'-property','FontSize'),'FontSize',8)

printfolder = 'E:\local\users\cagatay\aon-priform\figures\manuscript';
if printfigure
    print(gcf,'-dpdf',sprintf('%s\\linear_regression_100rep_4trials.pdf',printfolder))
end


%%


clf,
ax = [];

ax = [ax,subplot(1,1,1)];
mm{1} =  mean(all_y(cond{1},:));
ss{1} = std(all_y(cond{1},:))./sqrt(length(cond{1}));
mm{2} =  mean(all_y(cond{2},:));
ss{2} = std(all_y(cond{2},:))./sqrt(length(cond{2}));

    plot(tmp_x,mm{1},'color',clib{1}),hold all
    plot(tmp_x,mm{1}+ss{1},'--','color',clib{1}),
    plot(tmp_x,mm{1}-ss{1},'--','color',clib{1}),
    plot(tmp_x,mm{2},'color',clib{2})
    plot(tmp_x,mm{2}+ss{2},'--','color',clib{2}),
    plot(tmp_x,mm{2}-ss{2},'--','color',clib{2}),

plot([0 1],[0 1],'--','color',[.5 .5 .5])
 xlabel('False positive rate')
 ylabel('True positive rate')
set(ax,'box','off','tickdir','out',...
    'ticklength',get(ax(end),'ticklength').*5,...
    'linewidth',0.5,'xtick',[0 0.5 1],'ytick',[0 0.5 1])

set(gcf,'paperunits','centimeters','papersize',[4.2*1,4.2*1],...
    'paperposition',[0,0,4.2*1,4.2*1])

set(findall(gcf,'-property','FontSize'),'FontSize',8)

printfolder = 'E:\local\users\cagatay\aon-priform\figures\manuscript';
if printfigure
    print(gcf,'-dpdf',sprintf('%s\\linear_regression_100rep_4trials_auc.pdf',printfolder))
end


%%
clf
% linear regression plot
ax = [];
    
ax = [ax,subplot(1,1,1)];
imagesc(rand(83,1))
colormap gray
colorbar

 xlabel('False positive rate')
 ylabel('True positive rate')
set(ax,'box','off','tickdir','out','visible','off',...
    'ticklength',get(ax(end),'ticklength').*5,...
    'linewidth',0.5,'xtick',[0 0.5 1],'ytick',[0 0.5 1])

set(gcf,'paperunits','centimeters','papersize',[1.5*1,3*1],...
    'paperposition',[0,0,1.5*1,3*1])

set(findall(gcf,'-property','FontSize'),'FontSize',8)

printfolder = 'E:\local\users\cagatay\aon-priform\figures\manuscript';
if printfigure
    print(gcf,'-dpdf',sprintf('%s\\linear_regression_example.pdf',printfolder))
end

    

%%

clear all
close all
clc


%
varnames = {'exp_name','ts','session_name',...
    'stim_type','f0',...
    'ts_trials',...
    'f0','trial_type','trial_idx',...
    'chemical_list','left_right','trial_occur',...
    'recording_site','mf0','sf0','novelty','blank','sig','resp_h'};
dataset = 'lr';
foldername = '.';
[data] = get_data(foldername,dataset,varnames);
disp('Data loaded.')


%%

expname = {'180322_MB019','180329_MB018','180329_MB021','180329_MB022',...
    '180330_MB018','180330_MB021','180330_MB022','180504_MB023',...
    '180510_MB026','181114_MB050','181115_MB049', '181115_MB050',...
    '181122_MB052','181123_MB052','181130_MB053', '181201_MB053',...
    'AON001','AON002','AON003','AON005','AON006','AON007','AON008',...
    'AON009','AON010','AON011','AON012','AON013'};

% expname = {'180322_MB019','180329_MB018','180329_MB021','180329_MB022',...
%     '180330_MB018','180330_MB021','180330_MB022','180504_MB023',...
%     '180510_MB026','181114_MB050','181115_MB049', '181115_MB050',...
%     '181122_MB052','181123_MB052','181130_MB053', '181201_MB053',...
%     'AON001','AON002','AON003','AON005','AON006','AON007','AON008',...
%     'AON009','AON010','AON011','AON012','AON013','AON014','AON015',...
%     'AON016','AON018'};


odorset = [1,1,1,1,...
   2,2,2,2,...
   1,1,2,2,...
   1,2,1,2,...
   1,1,1,1,1,1,1,...
   1,1,1,1,1,1,1,...
   1,1];


all_f0= [];
all_type= [];
all_region= [];
all_responsive= [];
asd = [];

cells = struct;
all_cells = struct;

binsize = 0.06;
edges = 0:binsize:3;

sigma =2;
x = -4*sigma:4*sigma;
kernel = exp(-x .^ 2 / (2*sigma ^ 2));
kernel = kernel./sum(kernel);

for ii = 1:length(data)
    dd = data(ii);
    dd.exp_name
    tmpf0 = [];
    tmptype = [];
    tmpid = [];
    tmpconv = [];
    ttype = unique(dd.trial_type);

    tmp_identity = nan(length(dd.f0{1}),1);
    for it = 1:length(ttype)
        tt = ttype(it);
        if dd.novelty(it)==1 
            tmp_identity(find(tt==dd.trial_type))=1; % changed for identity
%            tmp_identity(find(tt==dd.trial_type&(dd.trial_occur<=3)))=1;
%            tmp_identity(find(tt==dd.trial_type&(dd.trial_occur>3)))=0;
      else if ~dd.novelty(it)&&~dd.blank(it)
            tmp_identity(find(tt==dd.trial_type))=0;
            else
                tmp_identity(find(tt==dd.trial_type))=nan;
            end
        end
    end
    
    
    bsp = nan(length(edges),1);
    conv_trials ={};
    spike_times = {};
    
    for ic =1:length(dd.f0)
        ntrials = length(dd.trial_idx);
        sorted_f0 = dd.f0{ic}(dd.trial_idx);
        tmpf0(ic,:) = sorted_f0;
        tmptype(ic,:) = tmp_identity';
        
        
        sorted_trials = dd.ts_trials{ic}(dd.trial_idx);
        for it = 1:length(sorted_trials)
            bsp = histcounts(sorted_trials{it},edges)./binsize/3;
            bsp_conv = nan(1,length(bsp));
            bsp_conv = smooth(bsp,5);
%             bsp_conv = squeeze(conv(bsp,kernel,'same'));
            tmpconv(it,:) = bsp_conv;
        end
        conv_trials{ic}= tmpconv;
        spike_times{ic} = sorted_trials;
%         tmpid(ic,:) = 
    end
    
    cells(ii).f0= tmpf0;
    cells(ii).type = tmptype;
%     cells(ii).trialtype = dd.trial_type';
    cells(ii).region = dd.recording_site';
    cells(ii).responsive =  dd.resp_h;
    cells(ii).olfactometer_id = dd.trial_type;
    cells(ii).occur = dd.trial_occur;
    
    tmp = [];
    if odorset(ii)==1 && dd.trial_type(1)>10 
        tmp = dd.trial_type-21;
        tmp(tmp==1|tmp==8)=0;
        cells(ii).odor_id = tmp;
    else if odorset(ii)==2 && dd.trial_type(1)>10 
        tmp = dd.trial_type;
        tmp(tmp==22|tmp==29)=0;
        tmp(tmp==23)=2;
        tmp(tmp==24)=3;
        tmp(tmp==30)=9;
        tmp(tmp==31)=10;
        cells(ii).odor_id = tmp;
        
        else
        tmp = dd.trial_type;
        tmp(tmp==1|tmp==8)=0;
        cells(ii).odor_id = tmp;
        end
    end
%     if ii>=17
%         odorset(ii)
%         expname(ii)
%         cells(ii).odor_id(1:30)'
%         find(tmp==1|tmp==8|tmp==22|tmp==29)
%             keyboard
%     end
%         asd = [asd;cells(ii).odor_id'];

%         colorbar
        

    cells(ii).spike_times = spike_times';
    cells(ii).conv_times = conv_trials';
    
%     keyboard
end
%%
asd = 'data_s_removed.mat';
save (asd,'cells')

%%

clear all
load ('data_s_removed.mat')

%%

for ii = 1:length(cells)
    ncells = length(cells(ii).spike_times);
    figure
    for ic = 1:ncells
        subplot(7,10,ic)
        imagesc(cells(ii).conv_times{ic})
        colorbar
    end
    
end
%%
expname = {'180322_MB019','180329_MB018','180329_MB021','180329_MB022',...
    '180330_MB018','180330_MB021','180330_MB022','180504_MB023',...
    '180510_MB026','181114_MB050','181115_MB049', '181115_MB050',...
    '181122_MB052','181123_MB052','181130_MB053', '181201_MB053',...
    'AON001','AON002','AON003','AON005','AON006','AON007','AON008',...
    'AON009','AON010','AON011','AON012','AON013','AON014','AON015',...
    'AON016','AON018'};

odorset = [1,1,1,1,...
   2,2,2,2,...
   1,1,2,2,...
   1,2,1,2,...
   1,1,1,1,1,1,1,...
   1,1,1,1,1,1,1,...
   1,1];

%%



