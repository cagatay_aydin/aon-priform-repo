function S = load_t_files(filename)

S.tsflag = 'sec';
tfiles = list_files(filename,'*.t','temp');
% keyboard
nFiles = length(tfiles);

for iF = 1:nFiles    
    tfn = tfiles{iF};
    
    if ~isempty(tfn)
        
        tfp = fopen(tfn, 'rb','b');
        if (tfp == -1)
            error(['LoadSpikes: Could not open tfile ' tfn]);
        end
        
        ReadHeader(tfp);
        %S.t{iF} = fread(tfp,inf,'uint64');	% read as 64 bit ints
        S.t{iF} = fread(tfp,inf,'uint32');	% read as 64 bit ints
        
        % set appropriate time units
        switch S.tsflag
            case 'sec'
                S.t{iF} = S.t{iF}/10000;
            case 'ts'
                S.t{iF} = S.t{iF};
            case 'ms'
                S.t{iF} = S.t{iF}*10000*1000;
            otherwise
                error('LoadSpikes: invalid tsflag.');
        end
        
        % add filenames
        [~,fname,fe] = fileparts(tfn);
        S.label{iF} = cat(2,fname,fe);
        
        fclose(tfp);
        
    end 		% if tfn valid
end		% for all fi