function [unit, ts, waves, fets, probeinfo, param] = processDataFromKILO_neuropixels(...
    filebase, probename, probedepth, recordingsite, referencesite, mahalThreshold)
% [units, ts, waves, fets, param] = processDataFromKlusters(filebase, probename, mahalThreshold, probedepth)
%   Takes data from a folder that contains files from Klusters and finds the 
% units respect the criteria for isolation distance and that have
% less that 1% of the spikes in the first 3ms.
%
% Example of inputs
%       - filebase = [pwd,'\datafile0001']
%       - probename = {'N2T-A1x16-poly2s-5mm-50-177','N2T-A1x16-5mm-50-177'};
%       - probedepth = {2500,900};
%       - mahalThreshold = 20;
% Outputs:
%       - units - structure array with information on the units
%       - ts - cell array with timestamps for each unit
%       - waves - cell array with waveforms for each unit
%       - fets - cell array with features for each unit
%       - probeinfo - information on the probe(s)
%       - param - parameters loaded from the xml file
%

% [foldername,filename] = fileparts(filebase);
% Numbers should be zero-based!
% [probe,probeinfo] = loadProbeMap(probename,0);
% unprobes = unique(probe(:,2));

% 
% for i = 1:length(unprobes)
%     idx = find(probe(:,2)==unprobes(i));
%     probeinfo(i).probe(1:length(idx),:) = probe(idx,:);
%     probe(idx,5) = probe(idx,5) - probedepth(i);
%     probeinfo(i).probe(1:length(idx),5) = probe(idx,5);
%     
% end

%  % Add probe and reference sites
%  for p = 1:length(referencesite)           
%      probeinfo(p).recording_site = recordingsite{p};
%      probeinfo(p).reference_sites = referencesite{p};
%  end
probeinfo = [];
srate = 30000; %since neuropix has sampling rate 

[spks,unit_ids,res,clu,amps,pcFeat,cluDepths] = KILO_get_units_ts(filebase,srate,'good');

% keyboard
% [depth] = scGUI_get_principle_electrode(filebase,'good');

% ngroups = 1; % since there is no simultaneous recording with neuropix probe


param.samplingRate  = srate;

% param = parseKlustersXML(fullfile(foldername,[filename,'.xml']));
% param = [];
unit = [];
ts = {};
fets = {};
waves = {};
trashclusters = [0,1];
isiThresh = 0.01;%1; % percentage of the all isis is the threshold
isiedges = 0:0.0005:0.1;
unitcount = 1;
% keyboard

for k = 1:length(unit_ids)
    if isempty(spks{k})||isnan(cluDepths(k))||length(spks{k})<2
    disp('HELLO')
   continue
    else
       
%     Removing overlapping spikes
    temp_sp = double(res(clu == unit_ids(k)));
    nsamples = 5;
    isis = diff(temp_sp);
    ntrash = find(isis<nsamples);
    if ntrash ~=0
        fprintf('Getting rid of: %5.0f spikes\n',length(ntrash))
        temp = temp_sp;
        temp(ntrash+1)=nan;
        sp = temp(~isnan(temp))./srate;
    else
        sp = temp_sp./srate;
    end
    
%     res = double(res)./srate; 
    bins = histc(diff(sp),isiedges);
    N = length(sp)-1;%sum(bins)
    
    ts{unitcount} = sp;
    
    unit(unitcount).isis = sum(bins(isiedges<=0.002))/N;
    unit(unitcount).electrode = 0; %will be added
    unit(unitcount).shank = 1; %will be added
    unit(unitcount).depth = cluDepths(k); 
    unit(unitcount).HisolationDistance = 20; %will be added
    unit(unitcount).recording_site = [];
%     unit(k).recording_site = probeinfo.recording_site;
    
    fets{unitcount} = []; %will be added
    waves{unitcount} = []; %will be added
    unitcount = unitcount +1;
    end
    
end
% 
