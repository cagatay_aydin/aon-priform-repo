function extract_neuropixels_tiggers_lfp(raw_folder,destination_folder)

disp('extracting events from the raw data')
fprintf('raw_folder: %s\ndestination_folder:%s\n',raw_folder,destination_folder)
tic
listRaw = dir(fullfile(raw_folder,'*lf.bin'));

allTriggers = cell(length(listRaw),1);
allTriggersS = cell(length(listRaw),1);
% keyboard
for l = 1:length(listRaw)
    [out,meta] = load_ni_whisper_file(fullfile(raw_folder,listRaw(l).name),1,false);
    stimInfo = out.Data.data(end,:);
    med = median(stimInfo(1:100)); %median, i.e. background value
    
    trigs = unique(stimInfo); %all unique values
    trigs = setdiff(trigs,med); %remove background
    trigTime = zeros(length(trigs),size(stimInfo,2));
    for t = 1:length(trigs) % go through the different trigger types
        tmp = find(stimInfo==trigs(t)); %find time points for this trigger
        trigTime(t,tmp)=1; % set them to 1
        ups = diff(trigTime(t,:)); %find the start of the trigger
        up = find(ups==1);
        down = find(ups==-1);
        if length(up)>length(down)
            up=up(1:length(down));
        end
        for u = 1:length(up)
            trigTime(t,up(u)+2:down(u))=0; %keep only the start of the trigger
        end
    end
    
    [tmp1,tmp2] = find(trigTime==1);
    triggers = cell(size(trigTime,1),1);
    triggersS = cell(size(trigTime,1),1);
    
    L = [];
    for t = 1:size(trigTime,1)
        L = [L;length(find(tmp1==t))];
        triggers{t} = tmp2(tmp1==t);
        triggersS{t} = tmp2(tmp1==t)./2500;
    end
    allTriggers{l}=triggers;
    allTriggersS{l}=triggersS;
end
save(fullfile(destination_folder,'triggers'),'allTriggers','allTriggersS')
toc

end