clear all
close all
clc


%
varnames = {'exp_name','ts','session_name',...
    'stim_type',...
    'ts_trials',...
    'f0','trial_type','trial_idx',...
    'chemical_list','left_right','trial_occur',...
    'recording_site','mf0','sf0','novelty','blank','sig',...
    'sig_val','resp_h','mod_ind_fam','mod_ind_nov','opto_tagged'};
dataset = 'lr';
foldername = '.';
[data] = get_data(foldername,dataset,varnames);
disp('Data loaded.')


%%

tmp = arrayfun(@(x)(x.recording_site),data,'uniformoutput',0);
rsite = horzcat(tmp{:});

blank = logical(data(1).blank);
novelty = logical(data(1).novelty);

all_f0 = [];
all_sig = [];
all_resp_sig = [];
all_mag = [];
all_bsp = [];
all_mod_ind_nov = [];
all_mod_ind_fam = [];
stimdur = 3;
binsize = 0.02;
edges = 0:binsize:stimdur;

sigma = 4;
x = -4*sigma:4*sigma;
kernel = exp(-x .^ 2 / (2*sigma ^ 2)); kernel = kernel./sum(kernel);

for expt = 1:length(data)
    dd = data(expt);
    mag = nan(length(dd.ts),1);
    if ~isempty(tmp)
        bin_resp = nan(length(dd.ts),length(edges));
        for ic = 1:length(dd.ts)
            bsp = nan(length(edges),1);
            bl_bsp = nan(length(edges),1);
            % positive and negative index
            bl = mean(dd.mf0(ic,blank),2);
            fl = mean(dd.mf0(ic,~blank),2);
            mag(ic) = (fl-bl)/fl+bl;
            
%            [~,pidx] = nanmax(dd.mf0(ic,:)); %find the most significant response
%            tidx = find(dd.trial_type==pidx);
           ttrial = unique(dd.trial_type);
           r_idx = [find(dd.trial_type==ttrial(2));find(dd.trial_type==ttrial(3));...
               find(dd.trial_type==ttrial(4));find(dd.trial_type==ttrial(5));...
               find(dd.trial_type==ttrial(6));find(dd.trial_type==ttrial(7));...
               find(dd.trial_type==ttrial(9));find(dd.trial_type==ttrial(10));...
               find(dd.trial_type==ttrial(11));find(dd.trial_type==ttrial(12));...
               find(dd.trial_type==ttrial(13));find(dd.trial_type==ttrial(14))];
           
           
         
           
           tmp_ts = cell2mat(dd.ts_trials{ic}(dd.trial_idx(r_idx)));
           
%            tmp_ts = cell2mat(dd.ts_trials{ic});
           bsp = histc(tmp_ts,edges)./length(dd.trial_idx(r_idx));
           bsp_conv = nan(1,length(bsp));
           bsp_conv = squeeze(conv(bsp,kernel,'same'));
% keyboard
             
           bl_conv = nan(1,length(bsp));
           bl_idx = [find(dd.trial_type==ttrial(1));find(dd.trial_type==ttrial(8))];
           tmp_blank = cell2mat(dd.ts_trials{ic}(dd.trial_idx(bl_idx)));
           bl_bsp = histc(tmp_blank,edges)./length(bl_idx);
           bl_conv = squeeze(conv(bl_bsp,kernel,'same'));
%            keyboard
           
%            bin_resp(ic,:) = bsp_conv./max(bsp_conv);
           if isequal(size(bl_conv),size(bsp_conv))
           tmp_conv  = bsxfun(@minus,bsp_conv,bl_conv);
%            tmp_conv  = bsxfun(@minus,bsp_conv,bl_conv)./(bsp_conv+bl_conv);
            
           bin_resp(ic,:) = zscore(tmp_conv);
           else
%            tmp_conv  = bsxfun(@minus,bsp_conv,bl_conv')./(bsp_conv+bl_conv');
           tmp_conv  = bsxfun(@minus,bsp_conv,bl_conv');
           bin_resp(ic,:) = zscore(tmp_conv);
           end      

        end
        all_bsp = [all_bsp;bin_resp];
        all_mag = [all_mag;mag];
        all_sig = [all_sig;nansum(dd.sig,2)];
        all_resp_sig = [all_resp_sig;dd.resp_h];
        all_mod_ind_fam = [all_mod_ind_fam,dd.mod_ind_fam];
        all_mod_ind_nov = [all_mod_ind_nov,dd.mod_ind_nov];
    end
%     all_f0 = vertcat(data.mf0);
end
% all_f0 = vertcat(data.mf0);

%%

resp_idx = [all_resp_sig>0]';
% cond = {strcmp(rsite,'aon')&sig_idx&resp_idx,...
%     strcmp(rsite,'piriform')&sig_idx&resp_idx};
cond = {[strcmp(rsite,'aon')&resp_idx],...
    strcmp(rsite,'piriform')&resp_idx};
a = nan(length(all_resp_sig),1);
a(find (strcmp(rsite,'aon')&resp_idx))=1;
a(find (strcmp(rsite,'piriform')&resp_idx))=2;

% cell_idx = cond{1}|cond{2};
cell_idx = cond{2};
dat = all_bsp(cell_idx,:);
expind = a(cell_idx);
mm_nov = all_mod_ind_nov(cell_idx);
mm_fam = all_mod_ind_fam(cell_idx);
% imagesc(all_bsp(cond{1}|cond{2},:))
% dat = all_bsp(cond{1}|cond{2},:);
isnan(mean(dat))
%%
clf


linewidth = 1;
ncluster = 3;
[c, ~,sumd]= kmeans(dat,ncluster,'Replicates',10);
idx = 1:length(c);
[v,ii] = sort(c);
sortedidx = idx(ii);
sorted_cycle_avg = dat(sortedidx,:);
clind = arrayfun(@(x) find(v==x,1,'last'),unique(v));
printfigure = true;

% figure,
clib = lines(ncluster);
%  {[146/255,71/255,149/255],[0 0 1],[63/255,143/255,56/255]};
% PLOT CYCLE AVERAGES ALL
axclusters = axes('position',[.02,.1,.28,.80]);
imagesc([sorted_cycle_avg, zeros(length(idx),3)],[0,1]),hold all
colormap(gray)
set(axclusters,'visible','off','box','off')
% colorbar

cb = colorbar('location','manual','position',[0.35,0.1,0.01,0.1],...
    'ticks',[0,1],'tickdirection','out',...
    'ticklength',get(gca,'ticklength')*5,'ticklabels',{'0','1'});
xlabel(cb,sprintf('Normalized\nFiring Rate'))
plot(1:size(dat,2),repmat(clind,1,size(dat,2)),'color',[1,0,0],'linewidth',1)
axis tight

    plot(size(dat,2)+[0,0],ylim,'r--','linewidth',1)


cc = lines(max(expind));
expinds = zeros(length(expind),1,3);

  

% for i = 1:2
%     expinds(i==expind,:,:) = repmat(cc(i,:),[sum(expind == i),1,1]);
% end

% axexp = axes('position',[.31,.01,.01,.98]);
% imagesc(expinds(sortedidx,:,:),[0 1]),hold all
% set(axexp,'box','off','visible','off')
% text(min(xlim),min(ylim),sprintf('Exp'),'fontsize',7)

% PLOT MEAN CYCLE AVERAGES
axmeanresponses = axes('position',[.35,.30,.24,.50]);

hold all
cont = 1;
inc = 3;
for kk = length(unique(v)):-1:1%1:length(unique(v))
    
    mean_resp = nanmean(sorted_cycle_avg(v==kk,:));
    plot(mean_resp+(inc*cont),'color',clib(kk,:),'linewidth',1),hold on
    
    plot(min(mean_resp).*ones(1,length(mean_resp))+(inc*cont),'color',[.5 .5 .5])

  
%     text(0,max(nanmean(sorted_cycle_avg(v==kk,:)))+offset*cont,...
%         sprintf('mean MI=%2.2f region LGN=%1.f V1=%1.f',nanmean(abs(sortedmodind(v==kk))),...
%         sum(sortedregionind(v==kk)),sum(sortedregionind(v==kk)==0)),'fontsize',7)
    cont = cont+1;
end
  plot([100 150],[0.5 0.5],'k')
    text([125],[0],sprintf('1 s'),'horizontalalignment','center',...
'verticalalignment','top')
xlim([0 250])
set(axmeanresponses,'box','off','visible','off')


axmodf0 = axes('position',[.68,.50,.1,.2]);
for kk = length(unique(v)):-1:1    
    all_idx = sortedidx(find(v==kk)');
    edges = [-1:binsize:2];
    tempdd{kk} = mm_nov(all_idx);
    bins = histc(tempdd{kk},edges);
    ss = cumsum(bins);
    ss = ss/max(ss);
    plot(edges,ss,'color',clib(kk,:),'linewidth',linewidth),hold all,
    ylim([0,1.01])
    xlim([min(edges),max(edges)])
    %         set(gca,'color','none','box','off','fontsize',8)
end
ylabel('CDF','fontsize',8)
xlabel('Modulation (%)','fontsize',8)
title('F0','fontweight','bold','fontsize',8)
set(axmodf0,'fontsize',8,'box','off','tickdir','out',...
    'color','none','ticklength',get(gca,'ticklength')*5,'xtick',[-1 0 1 2],...
    'xticklabel',[-100 0 100 200])

% PLOT RESPONSE MODULATION (F1)

axmodf1 = axes('position',[.85,.5,.1,.2]);
for kk = length(unique(v)):-1:1
    all_idx = sortedidx(find(v==kk'));
    edges = [-1:binsize:2];
    tempdd{kk} = mm_fam(all_idx);
    bins = histc(tempdd{kk},edges);
    ss = cumsum(bins);
    ss = ss/max(ss);
    plot(edges,ss,'color',clib(kk,:),'linewidth',linewidth),hold all,
    ylim([0,1.01])
    xlim([min(edges),max(edges)])
    %         set(gca,'color','none','box','off','fontsize',8)
end
title('F1','fontweight','bold')
set(axmodf1,'fontsize',8,'box','off','tickdir','out',...
    'color','none','ticklength',get(gca,'ticklength')*5,'xtick',[-1 0 1 2],...
    'xticklabel',[-100 0 100 200])



if printfigure
set(gcf,'paperunits','centimeter','paperposition',[0,0,22,12],'papersize',[22,12])
foldername = 'figures\';
fname = sprintf('%s\\cycle_average_clusters_sta_only%d%s.pdf',...
    foldername,ncluster,'aon');
print(gcf,'-dpdf','-painters','-loose',...
    fname)
end