cd e:\local\users\cagatay\kilosort-aon-pir\
clear all
close all
clc

experiments = {
    '181114_MB050',...
    '181115_MB049',...
    '181115_MB050',...
    '181122_MB052',...
    '181123_MB052',...
    '181130_MB053',...
    '181201_MB053'
    };



tval_idx = [1;1;1;1;1;1;1];
stim_type = {'lr'};

%%

raw_sniff_folder = 'raw_sniff_data'; %.seq files
sniff_folder = 'sniff_data'; % converted .mat files from .seq files
processed_folder = 'processed_data';
stimlog_folder = 'stimlog_data';

update_process_data = false;
mat_converted = false;

for e = 7%1:length(experiments)
    exp_name = experiments{e};
    fprintf('%s\n',exp_name)
    %     1. check whether sniff data is processed
    tt = {};
    cam = {};
    breath = {};
    tmp = sprintf('%s/%s/%s_sniffdata_%s.mat',processed_folder,...
        experiments{e},exp_name,stim_type{:});
    
    %     if update_process_data ==true
    %         fprintf('file deleted %s\n',tmp)
    %         delete(tmp)
    %     else
    %         continue
    %     end
    
    %         2. check converted .mat file
    mat_converted = 0;
    
    while mat_converted <1
        tmp = list_files([sniff_folder,'/',experiments{e}],'*.mat');
        if isempty(tmp)
            ttmp = list_files([raw_sniff_folder,'/',experiments{e}],'*.seq');
            if ~isempty(ttmp)
                fprintf('seq file found and converting ... \n')
                for ii = 1:length(ttmp)
                    [raw_data,frames] = read_FLIR_seq(ttmp{ii});
                    
                    [pathstr, name, ext] = fileparts(ttmp{ii});
                    file_name = sprintf('%s/%s/%s%s',sniff_folder,...
                        experiments{e},name,'.mat');
                    mkdir([sniff_folder,'/',experiments{e}])
                    save(file_name,'raw_data','frames');
                end
                
            else
                tttmp = list_files([stimlog_folder,'/',experiments{e}],'*.mat');
                fprintf('converted mat-file not found\nsearching for old experiment file\n')
                fprintf('loading experiment mat file %s\n',tttmp{1});
                        
                mp = load(fullfile(tttmp{1}));
                
                ntrials = size(mp.signal.breath,2);
                tt = [];
                inh_onsets = {};
                cam_resp = {};
                for ii = 1:ntrials
                    tmp_name = mp.signal.breath(ii).filename;
                    [pathstr, name, ext] = fileparts(tmp_name);
                    [a rm]= strtok(name,'_');
                    tr = datetime(rm,'InputFormat','_HH_mm_ss_SSS');
                    tt(ii) =  datenum(tr)*24*3600;
                    inh_onsets{ii} = mp.signal.breath(ii).inh;
                    cam_resp{ii} = mp.signal.breath(ii).cam_resp;
                end
                
                cam.onsets = [(tt-tt(1))]';
                cam.center_nostril = [];
                
                breath.inh_onsets = inh_onsets;
                breath.cam_resp = cam_resp;
                   pname = sprintf('%s/%s/%s_sniffdata_%s.mat',processed_folder,...
                experiments{e},exp_name,stim_type{:});
                save(pname,'cam','breath');
%                 cam.
                %load mat file and assign struct will be used in the next step
                mat_converted = true;
            end
            
        else
            fprintf('converted mat-file found\n')
            mat_converted = true;
            
            % defining user input -  center nostril
            pname = sprintf('%s/%s/%s_sniffdata_%s.mat',processed_folder,...
                experiments{e},exp_name,stim_type{:});
            pfile.cam.center_nostril = [];
            try
                pfile = load(pname);
            catch fprintf('no processed file found: %s\n',pname)
            end
            
            
            
            if isempty(pfile.cam.center_nostril)
                vid_file = tmp{1};
                disp(vid_file);
                center_nostril = spe_cen(vid_file);
                cam.center_nostril = center_nostril;
                
                mkdir([processed_folder,'/',experiments{e}])
                
                save(pname,'cam','breath');
            else
                %                 continue
                pfile =  load(pname);
                tt = [];
                for dir_ind = 1:length(tmp)
                    
                    vid_file = tmp{dir_ind};
                    disp(vid_file)
                    %                 extract inhalation onsets
                    [inh,synth_sig]=ext_inh_mouse_v1(vid_file,pfile.cam.center_nostril);
                    inh_onsets{dir_ind}=inh;
                    cam_resp{dir_ind}=synth_sig;
                    
                    samp_freq=60;
                    
                    tmp = list_files([sniff_folder,'/',experiments{e}],'*.mat');
                    
                    
                    [pathstr, name, ext] = fileparts(tmp{dir_ind});
                    [a rm]= strtok(name,'_');
                    tr = datetime(rm,'InputFormat','_HH_mm_ss_SSS');
                    tt(dir_ind) =  datenum(tr)*24*3600;
                end
                
                cam.onsets = [(tt-tt(1))]';
                %                 cam.center_nostril = center_nostril;
                
                breath.inh_onsets = inh_onsets;
                breath.cam_resp = cam_resp;
                save(pname,'cam','breath','-append');
            end
            
            
            
        end
    end
    
    
    
    
end























%%

sort_folder = 'scGUI_data';
events_folder = 'raw_nlx_data';
tdms_folder = 'tdms_data';
stimlog_folder = 'stimlog_data';
raw_nlx_folder = 'raw_nlx_data';
raw_sniff_folder = 'raw_sniff_data';
sniff_folder = 'sniff_data' ;
processed_folder = 'processed_data';

% left right
session_name = {'lr'};

ts = [];

for e = 1:14%length(experiments):-1:1
    
    
    exp_name = experiments{e};
    exp_name
    stim_type = session_name;
    
    %LOADING NLX EVENTS
    tmp = list_files([raw_nlx_folder,'/',experiments{e}],'*.nev');
    hdr = ft_read_event(tmp{1});
    
    ss = size(hdr,1);
    tstamp = nan(ss,1);
    tval = nan(ss,1);
    for ii = 1:length(hdr)
        tstamp(ii,:) = double(hdr(ii).timestamp)/1e6; %converting to seconds
        tval(ii,:) = double(hdr(ii).value);
    end
    
    start_event = tstamp(1);
    tstamp = tstamp-start_event;
    
    idx = find(tval == tval_idx(e));
    all_events = tstamp(idx);
    
    %     LOADING TDMS FILE
    tmp = list_files([tdms_folder,'/',experiments{e}],'*.mat');
    if isempty(tmp)
        tmp = list_files([tdms_folder,'/',experiments{e}],'*.tdms');
        tm = convertTDMS(true,tmp{1});
        tmp = list_files([tdms_folder,'/',experiments{e}],'*.mat');
        tm = load(tmp{1});
    else
        tmp = list_files([tdms_folder,'/',experiments{e}],'*.mat');
        tm = load(tmp{1});
    end
    
    a=tm.ConvertedData.Data.MeasuredData(3).Data;
    
    srate = 1000;
    time = linspace(0,length(a)/srate,length(a));
    
    ttmp= (a>0.5);
    d_events = diff(ttmp)>0;
    
    tdms_events = time(find(d_events));
    diff_events = min(all_events)-min(tdms_events);
    aligned_events = (tdms_events-min(tdms_events))+min(all_events);
    
    fprintf('NEV file has %d events\n',length(all_events));
    fprintf('TDMS file has %d events\n',length(tdms_events));
    fprintf('Difference in time %4.1f s\n',diff_events);
    
    events.onsets = all_events(2:4:end);
    events.offsets = events.onsets(2:end);
    events.raw = aligned_events;
    
    breath= {};
    cam= {};
    
    % 1. Check whether processed sniff file is created
    tt = {};
    tmp = sprintf('%s/%s/%s_sniffdata_%s.mat',processed_folder,...
        experiments{e},exp_name,stim_type{:});
    try
        tt = load(fullfile(tmp));
    catch fprintf('no sniff file found\n will create one\n')
    end
    % 2. Check whether the sniff file is filled
    if ~isempty(tt.breath)
        continue
        
    else
        % 3. Check whether seq files are converted to mat file
        tmp = list_files([sniff_folder,'/',experiments{e}],'*.mat');
        
        if isempty(tmp)
            tmp = list_files([raw_sniff_folder,'/',experiments{e}],'*.seq');
            if ~isempty(tmp)
                
                for ii = 1:length(tmp)
                    [raw_data,frames] = read_FLIR_seq(tmp{ii});
                    
                    [pathstr, name, ext] = fileparts(tmp{ii});
                    file_name = sprintf('%s/%s/%s%s',sniff_folder,...
                        experiments{e},name,'.mat');
                    mkdir([sniff_folder,'/',experiments{e}])
                    save(file_name,'raw_data','frames');
                end
            else
                % 4. Check whether there is not sniff file and load old
                % experiment mat file
                ttmp = list_files([stimlog_folder,'/',experiments{e}],'*.mat');
                fprintf('no trial sniff file found %s\nloading experiment mat file %s',exp_name,ttmp{1});
                mp = load(fullfile(ttmp{1}));
                
                ntrials = size(mp.signal.breath,2);
                tt = [];
                inh_onsets = {};
                cam_resp = {};
                for ii = 1:ntrials
                    tmp_name = mp.signal.breath(ii).filename;
                    [pathstr, name, ext] = fileparts(tmp_name);
                    [a rm]= strtok(name,'_');
                    tr = datetime(rm,'InputFormat','_HH_mm_ss_SSS');
                    tt(ii) =  datenum(tr)*24*3600;
                    inh_onsets{ii} = mp.signal.breath(ii).inh;
                    cam_resp{ii} = mp.signal.breath(ii).cam_resp;
                end
                
                cam.onsets = [(tt-tt(1)) + min(all_events)]';
                cam.pulse = all_events(1:4:end);
                cam.center_nostril = [];
                
                breath.inh_onsets = inh_onsets;
                breath.cam_resp = cam_resp;
                
            end
        else
            
            % Check whether center of nostril defined
            ttmp = list_files([processed_folder,'/',experiments{e}],'*.mat');
            file_name = sprintf('%s/%s/%s_sniffdata_%s.mat',processed_folder,...
                experiments{e},exp_name,stim_type{:});
            ffile = load(fullfile(file_name));
            tmp = list_files([sniff_folder,'/',experiments{e}],'*.mat');
            if isempty(ffile.cam.center_nostril)
                
                keyboard
                %             first check whether there is center nostril defined
                vid_file = tmp{1};
                disp(vid_file);
                center_nostril = spe_cen(vid_file);
                
            else
                
                %                keyboard
                tt = [];
                for dir_ind = 1:length(tmp)
                    center_nostril = a.cam.center_nostril;
                    vid_file = tmp{dir_ind};
                    disp(vid_file)
                    %                 extract inhalation onsets
                    
                    [inh,synth_sig]=ext_inh_mouse_v1(vid_file,ffile.cam.center_nostril);
                    inh_onsets{dir_ind}=inh;
                    cam_resp{dir_ind}=synth_sig;
                    
                    samp_freq=60;
                    
                    tmp = list_files([sniff_folder,'/',experiments{e}],'*.mat');
                    
                    
                    [pathstr, name, ext] = fileparts(tmp{dir_ind});
                    [a rm]= strtok(name,'_');
                    tr = datetime(rm,'InputFormat','_HH_mm_ss_SSS');
                    tt(dir_ind) =  datenum(tr)*24*3600;
                end
                
                cam.onsets = [(tt-tt(1)) + min(all_events)]';
                cam.pulse = all_events(1:4:end);
                cam.center_nostril = center_nostril;
                
                breath.inh_onsets = inh_onsets;
                breath.cam_resp = cam_resp;
            end
        end
        
        
        
        file_name = sprintf('%s/%s/%s_sniffdata_%s.mat',processed_folder,...
            experiments{e},exp_name,stim_type{:});
        
        mkdir([processed_folder,'/',experiments{e}])
        
        
        save(file_name,'cam','breath');
    end
    clear breath cam center_nostril
end



