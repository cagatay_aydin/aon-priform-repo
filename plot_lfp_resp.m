clear all
close all
clc

%%
parent_folder = 'E:\local\users\cagatay\kilosort-aon-pir\';
ext_KILO_folder = 'ext_KILO_data\';
raw_KILO_folder = 'raw_neuropixels_data\';
events_folder = 'events_data\';
d = dir(sprintf('%s%s',parent_folder,ext_KILO_folder));
isub = [d(:).isdir];
nameFolds = {d(isub).name}';
nameFolds(ismember(nameFolds,{'.','..'})) = [];

%%
ii = 1;

lfpFs = 2500;  % neuropixels phase3a
nChansInFile = 385;  % neuropixels phase3a, from spikeGLX

tmp = list_files(sprintf('%s%s%s',parent_folder,ext_KILO_folder,nameFolds{ii}),'*.channelmap');

% dlmread(tmp{1},',',1,0)
fileID= fopen(tmp{1});

C = textscan(fileID,'%d %d %d %d %d %d','delimiter',',','headerlines',1);

fclose(fileID);
c_idx = find(C{3}==0&C{5}<3300);
ch_name= C{1}(c_idx);
ch_connect = C{3}(c_idx);
ch_x = C{4}(c_idx);
ch_y = C{5}(c_idx);



%%

tmp = list_files(sprintf('%s%s%s',parent_folder,ext_KILO_folder,nameFolds{ii}),'*.lf_CAR.bin');
lfpFilename = tmp{1};


%%

nChansInFile = length(c_idx);
clipDur = 0.35;

d= dir(lfpFilename);
nSamps = d.bytes/2/nChansInFile;
%%
tmp = list_files(sprintf('%s%s%s',parent_folder,events_folder,nameFolds{ii}),'*.mat');
tm = load(tmp{1});

a = tm.allTriggersS{1}{1};
all_events = a(diff(a)>.015);
eventTimes = all_events(2:4:end);

ts = linspace(1,nSamps/lfpFs,nSamps);
for it = 1:length(eventTimes)
    sampStarts(it) = find(ts>=eventTimes(it)+1,[1],'first');
end
nClips = length(eventTimes);

%% downsample

mmf = memmapfile(lfpFilename, 'Format', {'int16', [nChansInFile nSamps], 'x'});

%%

dlfp = mmf.Data.x(:,1:lfpFs/1250:end);
disp('downsample complete')

%% whiten

for ii = 1:size(dlfp,1)
    wlfp(:,ii) = whitenlfp(dlfp(ii,:),1250*2000);
end
%%
ax = [];
for ch = 300 % based on 120 channels maybe you can use it some other way
    nlfp = wlfp(1:300000,ch)';
    [y,f,t,sy] = mtspec(nlfp);
    fig = figure();
    ax = [ax,subplot(6,1,1)];
    imagesc(t,f(f>5 & f<9),abs(20*log10(sy(:,f>5 & f<9)))')
    title('Theta')
    ax = [ax,subplot(6,1,2)];
    plot(t,mean(abs(20*log10(sy(:,f>5 & f<9))),2))
    ylim([0,20])
    linkaxes(ax,'x')
    ax = [ax,subplot(6,1,3)];
    imagesc(t,f(f>1 & f<4),abs(20*log10(sy(:,f>1 & f<4)))')
    title('Delta')
    ax = [ax,subplot(6,1,4)];
    plot(t,mean(abs(20*log10(sy(:,f>1 & f<4))),2))
    ylim([0,20])
    ax = [ax,subplot(6,1,5)];
    plot(t,mean(abs(20*log10(sy(:,f>5 & f<9))),2)/mean(abs(20*log10(sy(:,f>1 & f<4))),2))
    title('Theta/Delta')
%     tmp = 20*log10(sy(idx,:)/max(max(20*log10(sy(idx,:)))));
end


%%
[y,f,t,sy] = mtspec(nlfp,3072,1250,1250,0,[],[],[],[0,100]);
figure()
a = subplot(10,1,1);
plot([0:length(nlfp(ch,:))-1]/1250,nlfp(ch,:))
b  = subplot(10,1,2:10);
imagesc(t,f,20*log10(sy)')
set(gca,'ydir','normal')
colormap('jet')
%%

fig = figure();
a = axes('position',[0.12,0.15,0.3,0.75]);
tmp = reshape(((abs(stillpsds))),length(f),120);
imagesc(f(f>40 & f<100),1:120,tmp((f>40 & f<100),:)')
title('Standing')
ylabel('Channel # (sorted)')
xlabel('Frequency (Hz)')
b = axes('position',[0.5,0.15,0.3,0.75]);
tmp = reshape(((abs(locpsds))),length(f),120);
imagesc(f(f>40 & f<100),1:120,tmp((f>40 & f<100),:)')
set(a,'clim',get(b,'clim'))
title('Locomoting')
xlabel('Frequency (Hz)')
colormap(hot)
cb = colorbar(a,'position',[0.85,0.5,0.02,0.3]);
ylabel(cb,'Absolute power')
set([a,b,cb],'tickdir','out','box','off')
set(fig,'papersize',[5,4],'paperposition',[0,0,5,4],'paperunits','centimeter')


%%


nClipSamps = round(lfpFs*clipDur);

[b,a]=butter(3,[300]*2/(lfpFs),'low');



n_ch = length(ch_name); 

raw_lfp = zeros(nClips,n_ch,nClipSamps);
lfp = zeros(nClips,n_ch,nClipSamps);
filt_lfp = zeros(nClips,n_ch,nClipSamps);
med_lfp = zeros(nClips,n_ch,nClipSamps);
whit_lfp = zeros(nClips,n_ch,nClipSamps); 

for n = 1:nClips
    fprintf(1, 'clip%d\n', n);
    thisDat = double(mmf.Data.x(:, (1:nClipSamps)+sampStarts(n)));
    tmp = thisDat;
    raw_lfp(n,:,:) = tmp;
    tmp = bsxfun(@minus, tmp, mean(tmp,2));
    med_lfp(n,:,:) = tmp;
    tmp = tmp-repmat(median(tmp,1),size(tmp,1),1);
    lfp(n,:,:) = tmp;
    
%     for c = 1:n_ch
% %         filt_lfp(n,c,:) = filtfilt(b,a,thisDat(c,:));
%         filt_lfp(n,c,:) = filtfilt(b,a,tmp(c,:));
%         whit_lfp(n,c,:) = whitenlfp(squeeze(filt_lfp(n,c,:)),lfpFs*2000,2);
% %         keyboard
%     end
end