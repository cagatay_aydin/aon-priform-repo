clear all
close all
clc

cd E:\local\users\cagatay\eye_data\odor\181130_MB053

%% trial info

tmp = list_files(pwd,'Experiment*.mat');

trial_info = load(sprintf('%s',tmp{:}));

%% read tdms file

% tmp = list_files(pwd,'*.tdms');
% tm = convertTDMS(true,tmp{1});
tmp = list_files(pwd,'*.mat');
tm = load(tmp{1});

%%
a=tm.ConvertedData.Data.MeasuredData(3).Data;

srate = 1000;
time = linspace(0,length(a)/srate,length(a));

ttmp= (a>0.8);
d_events = diff(ttmp)>0.1;

tdms_events = time(find(d_events));

aligned_events = (tdms_events-min(tdms_events));

idx = find(diff(aligned_events)<0.1);
aligned_events(idx) = nan;
b = aligned_events(~isnan(aligned_events));


%% 

tmp = list_files(pwd,'*.hdf5');
hinfo = hdf5info(char(tmp));
eye_az = double(hdf5read(hinfo.GroupHierarchy.Datasets(1)));
eye_dia = double(hdf5read(hinfo.GroupHierarchy.Datasets(3)));
eye_diam = sgolayfilt(eye_dia,4,15);

%% read log time stamps for the eye data]
time =[];

tmp = list_files(pwd,'*.camlog');

fid = fopen(tmp{:});
C = textscan(fid, '%s', 'Delimiter', '\n', 'CommentStyle', '#');
C = C{:};
fclose(fid);

time = str2double(C);

%%

clf,
plot(time,eye_diam)

%%

eye_time = [];
samp_rate = 30;
eye_time = time/samp_rate;
eye_time = eye_time-min(eye_time);
% eye_time = eye_time;

%%
onsets = b(1:4:end);
% onsets = b(2:4:end);
all_trial_eye = [];
wii = 1:179;
sniptime = [];
for ii = 2:length(onsets)-1
    idx = find(eye_time>onsets(ii)&eye_time<onsets(ii)+6);
    all_trial_eye = [all_trial_eye, eye_diam(idx(wii))];
    if ii ==2
      sniptime= eye_time(idx)-min(eye_time(idx));
    end
end

%%

clf,
[aa,ind]= sort(trial_info.trial.odor_ID(2:end-1));
cc = trial_info.trial.count(2:end-1);
% ccount = cc(ind);
clf,imagesc(all_trial_eye(:,ind)')
sorted = all_trial_eye(:,ind);

%%
clf
% bl = [1,0,0,1,0,0];
% stype = [0,1,1,0,1,1];
% ttitle = {'BLA','A','B','BLA','C','D'};
ttype = aa;
utype = unique(aa);
mm = []
ax = [];
for ii = 1:length(utype)    
    ax = [ax,subplot(14,1,ii)];
    iidx = find(ttype == utype(ii))
    plot(sniptime(wii),all_trial_eye(:,iidx),'color',[.5 .5 .5])
    hold on
    plot(sniptime(wii),nanmean(all_trial_eye(:,iidx),2),'color',[1 0 0]),
    hold on
    plot([5 5],[min(ylim) max(ylim)],'--','color',[0.5 0.5 0.5])
%     title(sprintf('%s',ttitle{ii}))
end

% set(ax,'ylim',[0.5,2],'box','off')

%% plot novel vs familiar

clf
% bl = [1,0,0,1,0,0];
% stype = [0,1,1,0,1,1];
% ttitle = {'BLA','A','B','BLA','C','D'};
ttype = trial_info.trial.odor_ID(2:end-1);
utype = [25, 26, 27, 28, 32, 33, 34, 35];
nov_idx = [];
for ii = 1:length(utype)    
    iidx = find(ttype == utype(ii))
   cc(iidx)
%     find([1 2 3 4]==)
    
    nov_idx = [nov_idx;iidx(1:3)];
end

utype =  [23, 24, 30, 31];
fam_idx = [];
for ii = 1:length(utype)    
    iidx = find(ttype == utype(ii));
    fam_idx = [fam_idx;iidx(1:3)];
end


utype =  [22, 29];
bla_idx = [];
for ii = 1:length(utype)    
    iidx = find(ttype == utype(ii));
    bla_idx = [bla_idx;iidx];
end
hold all
ax = [];
ax = subplot(1,1,1);
x= sniptime(wii);

y=nanmean(all_trial_eye(:,fam_idx),2);
s =nanstd(all_trial_eye(:,fam_idx)')./sqrt(length(fam_idx));
shadedErrorBar(x,y,s,'k');

y=nanmean(all_trial_eye(:,nov_idx),2);
s =nanstd(all_trial_eye(:,nov_idx)')./sqrt(length(nov_idx));
shadedErrorBar(x,y,s,'r');

% plot(sniptime(wii),nanmean(all_trial_eye(:,nov_idx),2),'color',[1 0 0]),hold all
% plot(sniptime(wii),nanmean(all_trial_eye(:,fam_idx),2),'color',[0 0 0]),hold all
% plot(sniptime(wii),nanmean(all_trial_eye(:,bla_idx),2),'color',[0.5 0.5 0.5]),
hold on
set(ax,'box','off','tickdir','out','ticklength',get(gca,'ticklength').*5)
%     plot([5 5],[min(ylim) max(ylim)],'--','color',[0.5 0.5 0.5])

if true
    set(gcf,'paperunits','centimeter','paperposition',[0,0,6,4],'papersize',[6,4])
    foldername = pwd;

    fname = sprintf('%s\\odor_novel_vs_fam.pdf',...
        foldername);
    print(gcf,'-dpdf','-painters','-loose',...
        fname)
end
