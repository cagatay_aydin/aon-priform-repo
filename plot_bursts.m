clear all
close all
clc


%
varnames = {'exp_name','ts','session_name',...
    'stim_type','ts',...
    'ts_trials',...
    'f0','trial_type','trial_idx',...
    'chemical_list','left_right','trial_occur',...
    'recording_site','mf0','sf0','novelty','blank','sig','resp_h'};
dataset = 'lr';
foldername = '.';
[data] = get_data(foldername,dataset,varnames);
disp('Data loaded.')

%%

tmp = arrayfun(@(x)(x.recording_site),data,'uniformoutput',0);
rsite = horzcat(tmp{:});

blank = logical(data(1).blank);
novelty = logical(data(1).novelty);

all_f0 = [];
all_sig = [];
all_resp = [];
all_mag = [];
all_nbr = [];
for expt = 1:length(data)
    dd = data(expt);
    tmp = dd.mf0;
    if ~isempty(tmp)
        mag = [];
        nbr = [];
        for ic = 1:length(dd.ts)
            % positive and negative index
            bl = mean(dd.mf0(ic,blank),2);
            fl = mean(dd.mf0(ic,~blank),2);
            mag(ic) = (fl-bl)/fl+bl;
%             keyboard
            nbr(ic) = sum(diff(dd.ts{ic})<0.015);
            
        end
%         keyboard
        all_nbr = [all_nbr ,nbr];
        all_mag = [all_mag,mag];
        all_f0 = [all_f0;tmp(:,1:14)];
        all_sig = [all_sig;nansum(dd.sig,2)];
        all_resp = [all_resp;dd.resp_h];
    end
    %     all_f0 = vertcat(data.mf0);
end
% all_f0 = vertcat(data.mf0);

%%



clib = {[146/255,71/255,149/255],[63/255,143/255,56/255]};
% sig_idx = [all_sig>=1]';
resp_idx = [all_resp>0]';
% cond = {strcmp(rsite,'aon')&sig_idx&resp_idx,...
%     strcmp(rsite,'piriform')&sig_idx&resp_idx};
cond = {strcmp(rsite,'aon')&resp_idx,...
    strcmp(rsite,'piriform')&resp_idx};
clf
edges = [0:1e3:8e4];
for ii = 1:length(cond)
   idx = cond{ii};
    a = all_nbr(idx);
    bb = histc(a,edges);
    subplot(211)
    b = bar(edges,bb);,hold all
    set(b,'facecolor','none','edgecolor',clib{ii})
    
    
    tbb = cumsum(bb);
    tbb = tbb/max(tbb);
    subplot(212)
    plot(edges,tbb,'color',clib{ii}),hold on
end