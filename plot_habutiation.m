clear all
close all
clc


%
varnames = {'exp_name','ts','session_name',...
    'stim_type',...
    'ts_trials',...
    'f0','trial_type','trial_idx',...
    'chemical_list','left_right','trial_occur',...
    'recording_site','mf0','sf0','novelty','blank','sig','resp_h'};
dataset = 'lr';
foldername = '.';
[data] = get_data(foldername,dataset,varnames);
disp('Data loaded.')

%%


tmp = arrayfun(@(x)(x.recording_site),data,'uniformoutput',0);
rsite = horzcat(tmp{:});
blank = logical(data(1).blank);
novelty = logical(data(1).novelty);


%%


all_hab = [];
all_fam = [];
ste_hab = [];
ste_fam = [];
all_sig = [];
norm_hab = [];
norm_fam = [];
all_mag = [];
all_resp_sig = [];
for iexpt = 1:length(data)
%     keyboard
    dd = data(iexpt);
    if ~isempty(dd.f0)
        
    oc = dd.trial_occur;
    ttype = dd.trial_type;
    tidx = dd.trial_idx;
    ntype = unique(dd.trial_type);
    noccur = 15;%max(unique(oc));
    
    hab_resp = nan(length(dd.ts),noccur);
    norm_hab_resp = nan(length(dd.ts),noccur);
    hab_ste = nan(length(dd.ts),noccur);
    fam_resp = nan(length(dd.ts),noccur);
    norm_fam_resp = nan(length(dd.ts),noccur);
    fam_ste = nan(length(dd.ts),noccur);
    sig = nan(length(dd.ts),1);
    resp_sig = nan(length(dd.ts),1);
    mag = nan(length(dd.ts),1);
    for ic = 1:length(dd.ts)
        bl = mean(dd.mf0(ic,blank));
        resp = mean(dd.mf0(ic,~blank));
        mag(ic) = (resp-bl)/(resp+bl);
        
        sig(ic) = nansum(dd.sig(ic,:),2); 
        resp_sig(ic) = dd.resp_h(ic);
        sf0 = dd.f0{ic}(tidx);
        
        novel_odors = ntype(novelty);
%         keyboard
        
        indix = [];
        ocidx = nan(noccur,length(novel_odors));
        odidx = nan(noccur,length(novel_odors));
       
        for io = 1:noccur
            
            for in = 1:length(novel_odors)
                try
                n = novel_odors(in);
                ocidx(io,in) = find(io==oc&ttype == n);
                odidx(io,in) = ttype(find(io==oc&ttype==n));
                catch fprintf('no trials for %d rep for odor %d\n',io,n)
                end
                
            end
            
            idx = ocidx(io,:);
            if sum(isnan(idx))>0
                idx = idx(~isnan(idx));
                hab_resp(ic,io) = mean(sf0(idx));
                hab_ste(ic,io) = std(sf0(idx))./sqrt(length(idx));
                
            else if sum(isnan(idx))==length(novel_odors)
                hab_resp(ic,io) = nan;
                hab_ste (ic,io) = nan;
                
            else
                hab_resp(ic,io) = nanmean(sf0(idx));
                hab_ste(ic,io) = nanstd(sf0(idx))./sqrt(length(idx));
                end
            end
           
        end
        
        fam_odors = ntype(find(~novelty&~blank));
        
        indix = [];
        ocidx = nan(noccur,length(fam_odors));
        odidx = nan(noccur,length(fam_odors));
%        keyboard
        for io = 1:noccur
            
            for in = 1:length(fam_odors)
                try
                n = fam_odors(in);
                ocidx(io,in) = find(io==oc&ttype == n);
                odidx(io,in) = ttype(find(io==oc&ttype==n));
                catch fprintf('no trials for %d rep for odor %d\n',io,n)
                end
                
            end
            
            idx = ocidx(io,:);
            if sum(isnan(idx))>0
                idx = idx(~isnan(idx));
                fam_resp(ic,io) = mean(sf0(idx));
                fam_ste(ic,io) = std(sf0(idx))./sqrt(length(idx));
                
            else if sum(isnan(idx))==length(fam_odors)
                fam_resp(ic,io) = nan;
                fam_ste (ic,io) = nan;
                
            else
                fam_resp(ic,io) = nanmean(sf0(idx));
                fam_ste(ic,io) = nanstd(sf0(idx))./sqrt(length(idx));
                end
            end
           
        end
        
        check_till = 6;
       norm_hab_resp(ic,1:check_till) =  (hab_resp(ic,1:check_till)-nanmin(hab_resp(ic,1:check_till)))...
           ./(nanmax(hab_resp(ic,1:check_till))-nanmin(hab_resp(ic,1:check_till)));
       
       norm_fam_resp(ic,1:check_till) =  (fam_resp(ic,1:check_till)-nanmin(fam_resp(ic,1:check_till)))...
           ./(nanmax(fam_resp(ic,1:check_till))-nanmin(fam_resp(ic,1:check_till)));
        
        
        end
    else
        continue
    end
    all_mag = [all_mag; mag];
    norm_hab = [norm_hab;norm_hab_resp];
    norm_fam = [norm_fam;norm_fam_resp];
    all_hab = [all_hab; hab_resp]; 
    all_fam = [all_fam; fam_resp]; 
     ste_hab = [ste_hab;hab_ste];
     ste_fam = [ste_fam;fam_ste];
     all_sig = [all_sig;sig];
     all_resp_sig = [all_resp_sig;resp_sig];
    
end

%% example neurons with habutiation to do

% experiments = {'140322_CA018','140422_CA022','140322_CA018','150910_JC014'};
% cells = [14,36,15,8];
% 
% for sel = 1:length(experiments)
%     dd = data(strcmp({data.exp_name},experiments{sel}));
%     
%     
% end
% end

% sig_idx = [all_sig>=1]';
% idx = (strcmp(rsite,'piriform')&sig_idx);
resp_idx = [all_resp_sig>0]';
idx = (strcmp(rsite,'piriform')&resp_idx);


clf,
printfigure = false;
% ic = 7;
% cells = [7,57];
cells = [57];

for ii = 1:length(cells)
    ic = cells(ii);
ee = errorbar(all_hab(ic,1:6),ste_hab(ic,1:6));
set(ee,'color',[0 0 0],'capsize',1,'linewidth',1)
hold all
ee = errorbar(all_fam(ic,1:6),ste_fam(ic,1:6));
set(ee,'color',[.5 .5 .5],'capsize',1,'linewidth',1)

ylabel(sprintf('Firing rate (spikes/s)'),'fontsize',7)
xlabel(sprintf('Novel odor presentation \n(#count)'),'fontsize',7)
title(sprintf('%s',rsite{ic}))

set(gca,'xlim',[0.8 6.2],'box','off','tickdir','out','ticklength',get(gca(1),'ticklength').*5,...
    'ylim',[0 max(ylim)])


axis('square')
set(gcf,'paperunits','centimeters','papersize',[4.2*1,4.2*1],...
    'paperposition',[0,0,4.2*1,4.2*1])

set(findall(gcf,'-property','FontSize'),'FontSize',8)

printfolder = 'E:\local\users\cagatay\aon-priform\figures\manuscript';

if printfigure
    print(gcf,'-dpdf',sprintf('%s\\hab_fam_example_cell_%d_%s.pdf',printfolder,ic,rsite{ic}))
end
end

%% distribution comparison with habutiation
clf
clib = {[146/255,71/255,149/255],[63/255,143/255,56/255]};
% sig_idx = [all_sig>=1]';
% cond = {strcmp(rsite,'aon')&sig_idx,...
%         strcmp(rsite,'piriform')&sig_idx};


resp_idx = [all_resp_sig>0]';
cond = {strcmp(rsite,'aon')&resp_idx,...
        strcmp(rsite,'piriform')&resp_idx};
    


for c = 1:length(cond)
    idx = cond{c};
    mm = nanmean(all_hab(idx,:));
    ss = nanstd(all_hab(idx,:))./sqrt(sum(idx));
    sum(idx)
    e = errorbar(1:15,mm,ss);hold all
    set(e,'color',clib{c})
end
set(gca,'xlim',[1 6])


%% distribution comparison with habutiation and familiar

clib = {[146/255,71/255,149/255],[63/255,143/255,56/255]};
% sig_idx = [all_sig>=1]';
% cond = {strcmp(rsite,'aon')&sig_idx,...
%         strcmp(rsite,'piriform')&sig_idx};

resp_idx = [all_resp_sig>0]';
cond = {strcmp(rsite,'aon')&resp_idx,...
        strcmp(rsite,'piriform')&resp_idx};
    


for c = 1:length(cond)
    idx = cond{c};
    mm = nanmean(all_hab(idx,:));
    ss = nanstd(all_hab(idx,:))./sqrt(sum(idx));
    sum(idx)
    e = errorbar(1:15,mm,ss);hold all
    set(e,'color',clib{c})
    
    
    
    mm = nanmean(all_fam(idx,:));
    ss = nanstd(all_fam(idx,:))./sqrt(sum(idx));
    sum(idx)
    e = errorbar(1:15,mm,ss);hold all
    set(e,'color',[.5 .5 .5])
    
    
    
end
set(gca,'xlim',[1 6])


%% distribution comparison novel with habutiation


printfigure = true;
clf,
clib = {[146/255,71/255,149/255],[63/255,143/255,56/255]};
sig_idx = [all_sig>=1]';
% cond = {strcmp(rsite,'aon')&sig_idx,...
%         strcmp(rsite,'piriform')&sig_idx};

resp_idx = [all_resp_sig>0]';
cond = {strcmp(rsite,'aon')&resp_idx,...
        strcmp(rsite,'piriform')&resp_idx};
    
for c = 1:length(cond)
    idx = cond{c};
    mm = nanmean(norm_hab(idx,:));
    ss = nanstd(norm_fam(idx,:))./sqrt(sum(idx));
    sum(idx)
    e = errorbar(1:15,mm,ss);hold all
    set(e,'color',clib{c},'linewidth',1,'capsize',2)
    
%     mm = nanmean(norm_fam(idx,:));
%     ss = nanstd(norm_fam(idx,:))./sqrt(sum(idx));
%     sum(idx)
%     e = errorbar(1:15,mm,ss);hold all
%     set(e,'color',[.5 .5 .5],'markerfacecolor',clib{c},'linewidth',1)
    
end

ylabel(sprintf('Firing rate (Normalized)'),'fontsize',7)
xlabel(sprintf('Novel odor presentation \n(#count)'),'fontsize',7)
set(gca,'xlim',[0.5 6.5],'box','off','tickdir','out',...
    'ticklength',get(gca(1),'ticklength').*5,'xtick',[1:6])

axis('square')
set(gcf,'paperunits','centimeters','papersize',[4.2*1,4.2*1],...
    'paperposition',[0,0,4.2*1,4.2*1])

set(findall(gcf,'-property','FontSize'),'FontSize',8)

printfolder = '\figures';
if printfigure
    print(gcf,'-dpdf',sprintf('%s\\%s\\novel_habituation_cumsum_compare.pdf',pwd,printfolder))
end

%% distribution comparison familiar 


printfigure = true;
clf,
clib = {[146/255,71/255,149/255],[63/255,143/255,56/255]};
sig_idx = [all_sig>=1]';
% cond = {strcmp(rsite,'aon')&sig_idx,...
%         strcmp(rsite,'piriform')&sig_idx};
resp_idx = [all_resp_sig>0]';
cond = {strcmp(rsite,'aon')&resp_idx,...
        strcmp(rsite,'piriform')&resp_idx};


for c = 1:length(cond)
    idx = cond{c};
    mm = nanmean(norm_fam(idx,:));
    ss = nanstd(norm_fam(idx,:))./sqrt(sum(idx));
    sum(idx)
    e = errorbar(1:15,mm,ss);hold all
    set(e,'color',clib{c},'linewidth',1)
    
%     mm = nanmean(norm_fam(idx,:));
%     ss = nanstd(norm_fam(idx,:))./sqrt(sum(idx));
%     sum(idx)
%     e = errorbar(1:15,mm,ss);hold all
%     set(e,'color',[.5 .5 .5],'markerfacecolor',clib{c},'linewidth',1)
    
end

ylabel(sprintf('Firing rate (Normalized)'),'fontsize',7)
xlabel(sprintf('Novel odor presentation \n(#count)'),'fontsize',7)
set(gca,'xlim',[0.5 6],'box','off','tickdir','out',...
    'ticklength',get(gca(1),'ticklength').*5,'xtick',[1:6])

axis('square')
set(gcf,'paperunits','centimeters','papersize',[4.2*1,4.2*1],...
    'paperposition',[0,0,4.2*1,4.2*1])

set(findall(gcf,'-property','FontSize'),'FontSize',8)

printfolder = '\figures';
if printfigure
    print(gcf,'-dpdf',sprintf('%s\\%s\\familiar_habituation_cumsum_compare.pdf',pwd,printfolder))
end



%% normalized distribution comparison with habutiation

printfigure = false;
clf,
clib = {[146/255,71/255,149/255],[63/255,143/255,56/255]};
sig_idx = [all_sig>=1]';
% cond = {strcmp(rsite,'aon')&sig_idx,...
%         strcmp(rsite,'piriform')&sig_idx};
resp_idx = [all_resp_sig>0]';
cond = {strcmp(rsite,'aon')&resp_idx,...
        strcmp(rsite,'piriform')&resp_idx};


for c = 1:2%length(cond)
    idx = cond{c};
    mm = nanmean(norm_hab(idx,:));
    ss = nanstd(norm_hab(idx,:))./sqrt(sum(idx));
    sum(idx)
    e = errorbar(1:15,mm,ss);hold all
    set(e,'color',clib{c},'linewidth',1)
end

ylabel(sprintf('Firing rate (Normalized)'),'fontsize',7)
xlabel(sprintf('Novel odor presentation \n(#count)'),'fontsize',7)
set(gca,'xlim',[0.5 check_till],'box','off','tickdir','out',...
    'ticklength',get(gca(1),'ticklength').*5,'xtick',[1:check_till])

axis('square')
set(gcf,'paperunits','centimeters','papersize',[4.2*1,4.2*1],...
    'paperposition',[0,0,4.2*1,4.2*1])

set(findall(gcf,'-property','FontSize'),'FontSize',8)

printfolder = '\figures';
if printfigure
    print(gcf,'-dpdf',sprintf('%s\\%s\\habituation_cumsum_compare.pdf',pwd,printfolder))
end

%% plot distribution negative habutiation
printfigure = true;
clf,
clib = {[146/255,71/255,149/255],[63/255,143/255,56/255]};
sig_idx = [all_sig>=1]';

% cond = {strcmp(rsite,'aon')&sig_idx&[all_mag<0]',...
%         strcmp(rsite,'piriform')&sig_idx&[all_mag<0]'};

resp_idx = [all_resp_sig>0]';
cond = {strcmp(rsite,'aon')&resp_idx&[all_mag<0]',...
        strcmp(rsite,'piriform')&resp_idx&[all_mag<0]'};


for c = 1:length(cond)
    idx = cond{c};
    mm = nanmean(norm_hab(idx,:));
    ss = nanstd(norm_hab(idx,:))./sqrt(sum(idx));
    sum(idx)
    e = errorbar(1:15,mm,ss);hold all
    set(e,'color',clib{c},'linewidth',1)
end

ylabel(sprintf('Firing rate (Normalized)'),'fontsize',7)
xlabel(sprintf('Novel odor presentation \n(#count)'),'fontsize',7)
set(gca,'xlim',[0.5 6],'box','off','tickdir','out',...
    'ticklength',get(gca(1),'ticklength').*5,'xtick',[1:6])

axis('square')
set(gcf,'paperunits','centimeters','papersize',[4.2*1,4.2*1],...
    'paperposition',[0,0,4.2*1,4.2*1])

set(findall(gcf,'-property','FontSize'),'FontSize',8)

printfolder = '\figures';
if printfigure
    print(gcf,'-dpdf',sprintf('%s\\%s\\habituation_cumsum_compare_negative.pdf',pwd,printfolder))
end


%% plot distribution positive habutiation
printfigure = true;
clf,
clib = {[146/255,71/255,149/255],[63/255,143/255,56/255]};
sig_idx = [all_sig>=1]';

% cond = {strcmp(rsite,'aon')&sig_idx&[all_mag>0]',...
%         strcmp(rsite,'piriform')&sig_idx&[all_mag>0]'};
resp_idx = [all_resp_sig>0]';
cond = {strcmp(rsite,'aon')&resp_idx&[all_mag>0]',...
        strcmp(rsite,'piriform')&resp_idx&[all_mag>0]'};



for c = 1:length(cond)
    idx = cond{c};
    mm = nanmean(norm_hab(idx,:));
    ss = nanstd(norm_hab(idx,:))./sqrt(sum(idx));
   
    sum(idx)
    e = errorbar(1:15,mm,ss);hold all
    set(e,'color',clib{c},'linewidth',1)
end

ylabel(sprintf('Firing rate (Normalized)'),'fontsize',7)
xlabel(sprintf('Novel odor presentation \n(#count)'),'fontsize',7)
set(gca,'xlim',[0.5 6],'box','off','tickdir','out',...
    'ticklength',get(gca(1),'ticklength').*5,'xtick',[1:6])

axis('square')
set(gcf,'paperunits','centimeters','papersize',[4.2*1,4.2*1],...
    'paperposition',[0,0,4.2*1,4.2*1])

set(findall(gcf,'-property','FontSize'),'FontSize',8)

printfolder = '\figures';
if printfigure
    print(gcf,'-dpdf',sprintf('%s\\%s\\habituation_cumsum_compare_positive.pdf',pwd,printfolder))
end


%% plot distribution negative familiar habutiation
printfigure = true;
clf,
clib = {[146/255,71/255,149/255],[63/255,143/255,56/255]};
sig_idx = [all_sig>=1]';

% cond = {strcmp(rsite,'aon')&sig_idx&[all_mag<0]',...
%         strcmp(rsite,'piriform')&sig_idx&[all_mag<0]'};
    
    cond = {strcmp(rsite,'aon')&resp_idx&[all_mag<0]',...
        strcmp(rsite,'piriform')&resp_idx&[all_mag<0]'};

for c = 1:length(cond)
    idx = cond{c};
    mm = nanmean(norm_fam(idx,:));
    ss = nanstd(norm_fam(idx,:))./sqrt(sum(idx));
    sum(idx)
    e = errorbar(1:15,mm,ss);hold all
    set(e,'color',clib{c},'linewidth',1)
end

ylabel(sprintf('Firing rate (Normalized)'),'fontsize',7)
xlabel(sprintf('Novel odor presentation \n(#count)'),'fontsize',7)
set(gca,'xlim',[0.5 6],'box','off','tickdir','out',...
    'ticklength',get(gca(1),'ticklength').*5,'xtick',[1:6])

axis('square')
set(gcf,'paperunits','centimeters','papersize',[4.2*1,4.2*1],...
    'paperposition',[0,0,4.2*1,4.2*1])

set(findall(gcf,'-property','FontSize'),'FontSize',8)

printfolder = 'figures\';
if printfigure
    print(gcf,'-dpdf',sprintf('%s\\%s\\fam_habituation_cumsum_compare_negative.pdf',pwd,printfolder))
end


%% plot distribution positive familiar habutiation
printfigure = true;
clf,
clib = {[146/255,71/255,149/255],[63/255,143/255,56/255]};
sig_idx = [all_sig>=1]';

% cond = {strcmp(rsite,'aon')&sig_idx&[all_mag>0]',...
%         strcmp(rsite,'piriform')&sig_idx&[all_mag>0]'};
    cond = {strcmp(rsite,'aon')&resp_idx&[all_mag>0]',...
        strcmp(rsite,'piriform')&resp_idx&[all_mag>0]'};

for c = 1:length(cond)
    idx = cond{c};
    mm = nanmean(norm_fam(idx,:));
    ss = nanstd(norm_fam(idx,:))./sqrt(sum(idx));
   
    sum(idx)
    e = errorbar(1:15,mm,ss);hold all
    set(e,'color',clib{c},'linewidth',1)
end

ylabel(sprintf('Firing rate (Normalized)'),'fontsize',7)
xlabel(sprintf('Novel odor presentation \n(#count)'),'fontsize',7)
set(gca,'xlim',[0.5 6],'box','off','tickdir','out',...
    'ticklength',get(gca(1),'ticklength').*5,'xtick',[1:6])

axis('square')
set(gcf,'paperunits','centimeters','papersize',[4.2*1,4.2*1],...
    'paperposition',[0,0,4.2*1,4.2*1])

set(findall(gcf,'-property','FontSize'),'FontSize',8)

printfolder = '\figures';
if printfigure
    print(gcf,'-dpdf',sprintf('%s\\%s\\fam_habituation_cumsum_compare_positive.pdf',pwd,printfolder))
end










