% Animal name

cd e:\local\users\cagatay\aon-priform\
clear all
close all
clc

experiments = {'180322_MB019',...
    '180329_MB018',...
    '180329_MB021',...
    '180329_MB022',...
    '180330_MB018',...
    '180330_MB021',...
    '180330_MB022',...
    '180322_MB019',...
    '180504_MB023',...
    '180510_MB026'...
    };

% Depth

region = {'aon',...
    'aon',...
    'piriform',...
    'aon',...
    'aon',...
    'piriform',...
    'aon',...
    'aon',...
    'piriform',...
    'piriform'...
    };

bregma = [2.68,...
    2.05,...
    2.05,...
    2.05,...
    2.05,...
    2.05,...
    2.05,...
    2.05,...
    2.05
    ];

% depth = {'1000'};

chemical_list = {'Blank',...
    'Anisole',...
    'Ethyl valerate',...
    'Eugenol',...
    'Limonele',...
    '2,3,5-trimethylpyrazine',...
    'Benzyl acetate',...
    'Blank',...
    'Cinnamaldehyde',...
    'Pentenoic acid',...
    'Linalyl formate',...
    'Geraniol',...
    'Thiophene',...
    'Phenethyl alcohol'...
    };

%uses the channel that has proper bits
tval_idx = [1;1;1;1;1;1;1;1;1;1];

% % right = 0 % left = 1
% left_right = [0;0;0;0;0;0;0;1;1;1;1;1;1;1];
%
% % novelty = 1, not novel = 0
% novelty = [0;0;0;1;1;1;1;0;0;0;1;1;1;1];
%
% blank = [1;0;0;0;0;0;0;1;0;0;0;0;0;0];

%%

sort_folder = 'scGUI_data';
events_folder = 'raw_nlx_data';
tdms_folder = 'tdms_data';
stimlog_folder = 'stimlog_data';
raw_nlx_folder = 'raw_nlx_data';
processed_folder = 'processed_data';

% left right
session_name = {'lr'};

ts = [];

for e = 1:length(experiments)% 1:length(experiments)
    
    % no right left only from center
    left_right = [0;0;0;0;0;0;0;0;0;0;0;0;0;0];
    % novelty = 1, not novel = 0
    novelty = [0;0;0;1;1;1;1;0;0;0;1;1;1;1];
    blank =   [1;0;0;0;0;0;0;1;0;0;0;0;0;0];
    fam =     [0;1;1;0;0;0;0;0;1;1;0;0;0;0];
    
    exp_name = experiments{e};
    stim_type = session_name;
    
    
    %LOADING NLX EVENTS
    tmp = list_files([raw_nlx_folder,'/',experiments{e}],'*.nev');
    hdr = ft_read_event(tmp{1});
    
    ss = size(hdr,1);
    tstamp = nan(ss,1);
    tval = nan(ss,1);
    for ii = 1:length(hdr)
        tstamp(ii,:) = double(hdr(ii).timestamp)/1e6; %converting to seconds
        tval(ii,:) = double(hdr(ii).value);
    end
    
    start_event = tstamp(1);
    tstamp = tstamp-start_event;
    
    idx = find(tval == tval_idx(e));
    if sum(diff(tstamp(idx))<0.1)>0
        tb = find(diff(tstamp(idx))<0.1);
        tmp_events = tstamp(idx);
        all_events = tmp_events(1:tb(1));
        fprintf('broken NLX file %d events\n',sum(diff(tstamp(idx))<0.1));
    else
        all_events = tstamp(idx);
    end
    % keyboard
    %     LOADING TDMS FILE
    tmp = list_files([tdms_folder,'/',experiments{e}],'*.mat');
    if isempty(tmp)
        tmp = list_files([tdms_folder,'/',experiments{e}],'*.tdms');
        tm = convertTDMS(true,tmp{1});
        tmp = list_files([tdms_folder,'/',experiments{e}],'*.mat');
        tm = load(tmp{1});
    else
        tmp = list_files([tdms_folder,'/',experiments{e}],'*.mat');
        tm = load(tmp{1});
    end
    
    a=tm.ConvertedData.Data.MeasuredData(3).Data;
    
    srate = 1000;
    time = linspace(0,length(a)/srate,length(a));
    
    ttmp= (a>0.4);
    d_events = diff(ttmp)>0;
    
    %         keyboard
    tdms_events = time(find(d_events));
    diff_events = min(all_events)-min(tdms_events);
    aligned_events = (tdms_events-min(tdms_events))+min(all_events);
    
    fprintf('NEV file has %d events\n',length(all_events));
    fprintf('TDMS file has %d events\n',length(tdms_events));
    fprintf('Difference in time %4.1f s\n',diff_events);
    
    events.onsets = all_events(2:4:end);
    events.offsets = events.onsets(2:end);
    events.raw = aligned_events;
    
    file_name = sprintf('%s/%s/%s_behaviourdata_%s.mat',processed_folder,...
        experiments{e},exp_name,stim_type{:});
    
    mkdir([processed_folder,'/',experiments{e}])
    save(file_name,'-struct','events');
    save(file_name,'exp_name','session_name','stim_type','events','-append');
    
    %LOADING MCLUST DATA
    ts = {};
    ts_label = {};
    recording_site = {};
    
    
    tmpfolder = sprintf('%s\\%s',sort_folder,experiments{e});
    [units, ts, waves, fets, probeinfo, param] = processDataFromscGUI_NLX(fullfile(tmpfolder),...
        'N2T-A1x32-poly2s-5mm-50-177', 4500, {'vta'}, {14}, 20.0);
    ncells = length(ts);
    
    fprintf('Done getting units for %s N= %d\n',exp_name,ncells)
    
    
    
    file_name = sprintf('%s/%s/%s_spikedata_%s.mat',processed_folder,...
        experiments{e},exp_name,stim_type{:});
    save(file_name,...
        'ts','ts_label','exp_name','session_name','stim_type');
    
    
    % LOADING STIMLOG FILE ('.MAT')
    tmp = list_files([stimlog_folder,'/',experiments{e}],'*.mat');
    m = load(tmp{1});
    
    [~, tmp] = sort(m.trial.odor_ID);
    %     keyboard
    if length(tmp)==length(events.onsets)
        [trial_type, trial_idx] = sort(m.trial.odor_ID);
    else
        disp('TDMS events and NLX events are not matching')
        [trial_type, trial_idx] = sort(m.trial.odor_ID(1:length(events.onsets)));
    end
    
    
    trial_occur = m.trial.count(trial_idx);
    %     keyboard
    ttypes = unique(trial_type);
    ntypes = length(ttypes);
    mf0 = nan(length(ts),ntypes);
    blf0 = nan(length(ts),ntypes);
    sf0 = nan(length(ts),ntypes);
    sig_val = nan(length(ts),ntypes);
    sig = nan(length(ts),ntypes);
    mod_ind_nov = nan(1,length(ts));
    mod_ind_fam = nan(1,length(ts));
    resp_h = nan(length(ts),1);
    resp_p = nan(length(ts),1);
    ts_trials = {};
    ts_trials_before = {};
    ts_trials_after = {};
    ts_trials_selective = {};
    f0 = {};
    f0_before = {};
    f0_after = {};
    f0_bl = {};
    
    % COMPUTE TRIAL DATA
    %     keyboard
    if isempty(ts)
        ts_trials = {};
        ts_trials_before = {};
        ts_trials_after = {};
        ts_trials_selective = {};
        f0 = {};
        f0_before = {};
        f0_after = {};
        f0_bl = {};
        sig_val =[];
        sig = [];
        mod_ind_nov = nan(1,length(ts));
        mod_ind_fam = nan(1,length(ts));
        resp_h =[];
        resp_p =[];
    else
        for c = 1:ncells
            
            recording_site{c} = region{e};
            ts_trials{c} = extract_trial_spikes(ts{c},events.onsets,events.onsets+3);
            %             ts_trials{c} = extract_trial_spikes(ts{c},events.onsets-1,events.onsets+3);
            [f0{c},f1{c},f2{c}]=compute_fourier_visual_responses(ts_trials{c},...
                3,[0],5,1);
            %             [f0{c},f1{c},f2{c}]=compute_fourier_visual_responses(ts_trials{c},...
            %                 4,[0],5,1);
            
            ts_trials_before{c} = extract_trial_spikes(ts{c},events.onsets,events.onsets+1);
            [f0_before{c},f1{c},f2{c}]=compute_fourier_visual_responses(ts_trials_before{c},...
                3,[0],5,1);
            
            ts_trials_after{c} = extract_trial_spikes(ts{c},events.onsets+1,events.onsets+3);
            [f0_after{c},f1{c},f2{c}]=compute_fourier_visual_responses(ts_trials_after{c},...
                3,[0],5,1);
            
            ts_trials_selective{c} = extract_trial_spikes(ts{c},events.onsets-3,events.onsets);
            [f0_bl{c},f1{c},f2{c}]=compute_fourier_visual_responses(ts_trials_selective{c},...
                3,[0],5,1);
            
            [resp_h(c), resp_p(c)] = ttest2(f0_before{c},f0_after{c});
            %             keyboard
            sorted_f0 = f0{c}(trial_idx);
            sorted_f0_sel = f0_bl{c}(trial_idx);
            tmp_trials = cell(ntypes,1);
            tmp_trials_sel = cell(ntypes,1);
            count = 0;
            for ii = 1:ntypes
                it = ttypes(ii);
                tmp_trials{ii} = sorted_f0(trial_type==it);
                tmp_trials_sel{ii} = sorted_f0_sel(trial_type==it);
                mf0(c,ii) = mean(sorted_f0(trial_type==it));
                blf0(c,ii) = mean(sorted_f0(trial_type==it)-sorted_f0_sel(trial_type==it));
                sf0(c,ii) = std(sorted_f0(trial_type==it))./sqrt(sum(trial_type==it));
            end
            %            keyboard
            
            %           compute modulation
            tmp_resp = nan(length(tmp_trials),2);
            for ii = 1:length(tmp_trials)
                tmp = tmp_trials{ii};
                
                try
                    tmp_resp(ii,1) = nanmean(tmp(1:3));
                    tmp_resp(ii,2) = nanmean(tmp(4:6));
                    
                catch fprintf('no trials for %d rep for odor %d\n',ii)
                end
                
            end
            
            resp = tmp_resp(logical(novelty),1);
            resp = resp(~isnan(resp));
            base = tmp_resp(logical(novelty),2);
            base = base(~isnan(base));
            mm = min([length(base),length(resp)]);
            [mod_ind_nov(c),~] = modulation_index(base(1:mm),resp(1:mm));
            
            resp = tmp_resp(logical(fam),1);
            resp = resp(~isnan(resp));
            base = tmp_resp(logical(fam),2);
            base = base(~isnan(base));
            mm = min([length(base),length(resp)]);
            [mod_ind_fam(c),~] = modulation_index(base(1:mm),resp(1:mm));
            
            
            
            %           bl_trials = f0_selective{c};
            %            bl_trials = horzcat(tmp_trials{logical(blank)});
            odors = find(~blank);
            p = nan(ntypes,1);
            h = nan(ntypes,1);
            for io = 1:length(odors)
                resp_trials =tmp_trials{odors(io)};
                bl_trials = tmp_trials_sel{odors(io)};
                [tmp_h, tmp_p] = ttest2(bl_trials,resp_trials);
                p(odors(io)) = tmp_p;
                h(odors(io)) = tmp_h;
            end
            %            keyboard
            sig_val(c,:) = p;
            sig(c,:) = h;
            
            %compute habutiation
            
            
            
            
        end
    end
    
    
    file_name = sprintf('%s/%s/%s_odortrialdata_%s.mat',processed_folder,...
        experiments{e},exp_name,stim_type{:});
    
    save(file_name,...
        'exp_name','session_name','stim_type',...
        'ts_trials','f0','trial_idx','trial_type','chemical_list',...
        'left_right','trial_occur','recording_site','mf0','sf0','novelty',...
        'blank','sig_val','sig','resp_h','resp_p','mod_ind_fam','mod_ind_nov',...
        'blf0','f0_bl')
    
    
    
    
end