clear all
close all
clc

cd E:\local\users\cagatay\eye_data\acoustic\190118_MB057

%% trial info

tmp = list_files(pwd,'Experiment*.mat');

trial_info = load(sprintf('%s',tmp{:}));

%% read tdms file

% tmp = list_files(pwd,'*.tdms');
% tm = convertTDMS(true,tmp{1});
tmp = list_files(pwd,'*.mat');
tm = load(tmp{1});

%%
a=tm.ConvertedData.Data.MeasuredData(3).Data;

srate = 1000;
time = linspace(0,length(a)/srate,length(a));

ttmp= (a>0.8);
d_events = diff(ttmp)>0.1;

tdms_events = time(find(d_events));

aligned_events = (tdms_events-min(tdms_events));

idx = find(diff(aligned_events)<0.1);
aligned_events(idx) = nan;
b = aligned_events(~isnan(aligned_events));


%% 

tmp = list_files(pwd,'*.hdf5');
hinfo = hdf5info(char(tmp));
eye_az = double(hdf5read(hinfo.GroupHierarchy.Datasets(1)));
eye_diam = double(hdf5read(hinfo.GroupHierarchy.Datasets(3)));
% eye_diam = sgolayfilt(eye_dia,5,19);
% eye_diam = medfilt(eye_dia,5,19);

%% read log time stamps for the eye data]
time =[];

tmp = list_files(pwd,'*.camlog');

fid = fopen(tmp{:});
C = textscan(fid, '%s', 'Delimiter', '\n', 'CommentStyle', '#');
C = C{:};
fclose(fid);

time = str2double(C);

%%

eye_time = [];
samp_rate = 30;
eye_time = time/samp_rate;

%%

idx = find(abs(diff(eye_diam))>0.1);
tmp = eye_diam;
tmp(idx) = nan;
tmp(tmp<0.2) = nan;
tmp = medfilt1(tmp,15);

downsampled_time = min(eye_time):1/20:max(eye_time);
% cl_dia = interp1(eye_time,tmp,downsampled_time);
cl_dia = interp1(eye_time(~isnan(tmp)),tmp(~isnan(tmp)),downsampled_time);


dteye = nanmean(diff(eye_time));
sigma =150;
edges = -5*(sigma*1e-3):dteye:5*sigma*1e-3;
kernel = normpdf(edges,0,sigma*1e-3);
tmp_diam = conv(cl_dia,kernel/sum(kernel),'same');



%%
clf,
plot(eye_time,eye_diam,'color',[.5 .5 .5]),hold all
plot(downsampled_time,cl_dia,'b')
plot(downsampled_time,tmp_diam,'m')

%%
% 
% 
% 
% % eye_time = eye_time-min(eye_time);
% 

% downsampled_time = min(eye_time):1/20:max(eye_time);
% dw_pupil = interp1(eye_time,eye_diam,downsampled_time);
% 
% sigma =150;
% edges = -5*(sigma*1e-3):dteye:5*sigma*1e-3;
% kernel = normpdf(edges,0,sigma*1e-3);
% tmp_diam = conv(dw_pupil,kernel/sum(kernel),'same');



% eye_time = eye_time;

%%
eye_time = downsampled_time;
eye_diam = tmp_diam';
% alt_onsets = trial_info.times.odor_ON/1000;
onsets = b(1:4:end);
% onsets = alt_onsets;
all_trial_eye = [];
norm_trial_eye = [];
wii = 1:140;
sniptime = [];
for ii = 2:length(onsets)-1
    idx = find(eye_time>onsets(ii)-2&eye_time<onsets(ii)+7);
    idx_norm = find(eye_time>onsets(ii)-4&eye_time<onsets(ii)-2);
    all_trial_eye = [all_trial_eye eye_diam(idx(wii))];
%     tmp = (eye_diam(idx(wii))- min(eye_diam(idx(wii))))/ (max(eye_diam(idx(wii)))-min(eye_diam(idx(wii))));
%     tmp = eye_diam(idx(wii))- eye_diam(idx_norm(wii));
%     tmp = eye_diam(idx(wii))- nanmean(eye_diam(idx_norm));
    tmp = zscore(eye_diam(idx(wii)));
    norm_trial_eye = [norm_trial_eye tmp];
    if ii ==2
      sniptime= eye_time(idx)-min(eye_time(idx));
    end
end

%%

clf,
[aa,ind]= sort(trial_info.trial.odor_ID(2:end-1));
cc = trial_info.trial.count(2:end-1);
% ccount = cc(ind);
clf,imagesc(all_trial_eye(:,ind)')
sorted = all_trial_eye(:,ind);

%%
clf
% bl = [1,0,0,1,0,0];
% stype = [0,1,1,0,1,1];
% ttitle = {'BLA','A','B','BLA','C','D'};
ttype = aa;
utype = unique(aa);
mm = [];
ax = [];
for ii = 1:length(utype)    
    ax = [ax,subplot(4,1,ii)];
    iidx = find(ttype == utype(ii))
    plot(sniptime(wii),all_trial_eye(:,iidx),'color',[.5 .5 .5])
    hold on
    plot(sniptime(wii),nanmean(all_trial_eye(:,iidx),2),'color',[1 0 0]),
    hold on
    plot([5 5],[min(ylim) max(ylim)],'--','color',[0.5 0.5 0.5])
%     title(sprintf('%s',ttitle{ii}))
end

% set(ax,'ylim',[0.5,2],'box','off')

%% plot novel vs familiar

clf
% bl = [1,0,0,1,0,0];
% stype = [0,1,1,0,1,1];
% ttitle = {'BLA','A','B','BLA','C','D'} ;
ttype = trial_info.trial.odor_ID(2:end-1);
utype = [31, 32];
nov_idx = [];
for ii = 1:length(utype)    
    iidx = find(ttype == utype(ii))
%    cc(iidx)
%     find([1 2 3 4]==)
    
    nov_idx = [nov_idx;iidx(1:3)];
end

utype =  [23, 30];
fam_idx = [];
for ii = 1:length(utype)    
    iidx = find(ttype == utype(ii));
    fam_idx = [fam_idx;iidx(1:3)];
end


utype =  [22, 29];
bla_idx = [];
for ii = 1:length(utype)    
    iidx = find(ttype == utype(ii));
    bla_idx = [bla_idx;iidx];
end
hold all
ax = [];
ax = subplot(1,1,1);
x= sniptime(wii);

y=nanmean(all_trial_eye(:,fam_idx),2);
s =nanstd(all_trial_eye(:,fam_idx)')./sqrt(length(fam_idx));
shadedErrorBar(x,y,s,'k');

y=nanmean(all_trial_eye(:,nov_idx),2);
s =nanstd(all_trial_eye(:,nov_idx)')./sqrt(length(nov_idx));
shadedErrorBar(x,y,s,'b');

% plot(sniptime(wii),nanmean(all_trial_eye(:,nov_idx),2),'color',[1 0 0]),hold all
% plot(sniptime(wii),nanmean(all_trial_eye(:,fam_idx),2),'color',[0 0 0]),hold all
% plot(sniptime(wii),nanmean(all_trial_eye(:,bla_idx),2),'color',[0.5 0.5 0.5]),
hold on
set(ax,'box','off','fontsize',7,'ylim',[0.65 0.95],'xtick',[0:8],'xticklabel',[-2:7],...
    'xlim',[1,8])
%     plot([5 5],[min(ylim) max(ylim)],'--','color',[0.5 0.5 0.5])

if true
    set(gcf,'paperunits','centimeter','paperposition',[0,0,8,3],'papersize',[8,3])
    foldername = pwd;

    fname = sprintf('%s\\odor_novel_vs_fam_dw.pdf',...
        foldername);
    print(gcf,'-dpdf','-painters','-loose',...
        fname)
end

%% normalized plot novel vs familiar

clf
% bl = [1,0,0,1,0,0];
% stype = [0,1,1,0,1,1];
% ttitle = {'BLA','A','B','BLA','C','D'};
ttype = trial_info.trial.odor_ID(2:end-1);
utype = [31, 32];
nov_idx = [];
for ii = 1:length(utype)    
    iidx = find(ttype == utype(ii))
%    cc(iidx)
%     find([1 2 3 4]==)
    
    nov_idx = [nov_idx;iidx(1:3)];
end

utype =  [23, 30];
fam_idx = [];
for ii = 1:length(utype)    
    iidx = find(ttype == utype(ii));
    fam_idx = [fam_idx;iidx(1:3)];
end


utype =  [22, 29];
bla_idx = [];
for ii = 1:length(utype)    
    iidx = find(ttype == utype(ii));
    bla_idx = [bla_idx;iidx];
end
hold all
ax = [];
ax = subplot(1,1,1);
x= sniptime(wii);

y=nanmean(norm_trial_eye(:,fam_idx),2);
s =nanstd(norm_trial_eye(:,fam_idx)')./sqrt(length(fam_idx));
shadedErrorBar(x,y,s,'k');

y=nanmean(norm_trial_eye(:,nov_idx),2);
s =nanstd(norm_trial_eye(:,nov_idx)')./sqrt(length(nov_idx));
shadedErrorBar(x,y,s,'b');

% plot(sniptime(wii),nanmean(all_trial_eye(:,nov_idx),2),'color',[1 0 0]),hold all
% plot(sniptime(wii),nanmean(all_trial_eye(:,fam_idx),2),'color',[0 0 0]),hold all
% plot(sniptime(wii),nanmean(all_trial_eye(:,bla_idx),2),'color',[0.5 0.5 0.5]),
hold on
set(ax,'box','off','fontsize',7,'xtick',[0:8],'xticklabel',[-2:7],...
    'xlim',[1,8])
%     plot([5 5],[min(ylim) max(ylim)],'--','color',[0.5 0.5 0.5])

if true
    set(gcf,'paperunits','centimeter','paperposition',[0,0,8,3],'papersize',[8,3])
    foldername = pwd;

    fname = sprintf('%s\\odor_novel_vs_fam_norm.pdf',...
        foldername);
    print(gcf,'-dpdf','-painters','-loose',...
        fname)
end

%%
ttype = trial_info.trial.odor_ID(2:end-1);
cc = trial_info.trial.count(2:end-1);

clf,
c = 9;


plot(sniptime(wii),all_trial_eye(:,c))
title(sprintf('t=%d, occ=%d',ttype(c),cc(c)))


%%

clf

ax = [];
alt_onsets = trial_info.times.odor_ON/1000;
wind =1:20000;
idx = find(onsets>min(eye_time(wind))&onsets<max(eye_time(wind)));

ax = subplot(1,1,1);
plot(eye_time(wind),eye_diam(wind)),hold on
stem(onsets(idx),ones(length(idx),1),'color','b')
% stem(alt_onsets(idx),ones(length(idx),1),'color','r')

for ii = 1:length(idx)
text(onsets(idx(ii)),ones(length(idx(ii)),1).*1.4,sprintf('o%d t%d',cc(idx(ii)),ttype(idx(ii))))
end

set(ax,'ylim',[0.6 1.4])


