clear all
close all
clc


%
varnames = {'exp_name','ts','session_name',...
    'stim_type',...
    'ts_trials',...
    'f0','trial_type','trial_idx',...
    'chemical_list','left_right','trial_occur',...
    'recording_site','mf0','sf0','novelty','blank','sig'};
dataset = 'lr';
foldername = '.';
[data] = get_data(foldername,dataset,varnames);
disp('Data loaded.')

%%

tmp = arrayfun(@(x)(x.recording_site),data,'uniformoutput',0);
rsite = horzcat(tmp{:});
blank = logical(data(1).blank);
novelty = logical(data(1).novelty);


%% selected neurons for piriform

printall = true;

fig = figure;
experiments = {'AON003','AON003','AON005'};
cells = [14,17,1];

ax_psth = [];
ax_raster = [];
loc_rasters = [0.65,0.35,0.05];
loc_psth = [0.83,0.53,0.23];
loc_stim = [0.1, 0.33];

bin_size = 0.04;
sp_edges = [0:bin_size:3];
sigma = 2;
x = -2*sigma:2*sigma;
kernel = exp(-x .^ 2 / (2*sigma ^ 2)); kernel = kernel./sum(kernel);
dur = 3; 
stim = [3 10;
    3 9;
    3 10];

is = 1;

for sel = 1:length(experiments)
    dd = data(strcmp({data.exp_name},experiments{sel}));
    ic = cells(sel);
    
    idx_nov = dd.trial_idx(find(dd.trial_type==stim(sel,1)));
    ts_novel = dd.ts_trials{cells(sel)}(idx_nov);
    
    idx_fam = dd.trial_idx(find(dd.trial_type==stim(sel,2)));
    ts_fam = dd.ts_trials{cells(sel)}(idx_fam);
    
    idx_blank = dd.trial_idx(find(dd.trial_type==1));
    ts_blank = dd.ts_trials{cells(sel)}(idx_blank);
    
    ax_psth = [ax_psth, axes('position',[loc_stim(is),loc_psth(sel),0.2,0.1])]; 
        bsp =[];
        tmp_nov = cell2mat(ts_novel);
        bsp_nov = histc(tmp_nov,sp_edges)/length(idx_nov)/bin_size/dur;
        c_nov = conv(bsp_nov,kernel,'same');
        
        tmp_fam = cell2mat(ts_fam);
        bsp_fam = histc(tmp_fam,sp_edges)/length(idx_fam)/bin_size/dur;
        c_fam = conv(bsp_fam,kernel,'same');
        
        tmp_blank = cell2mat(ts_blank);
        bsp_blank = histc(tmp_blank,sp_edges)/length(idx_blank)/bin_size/dur;
        c_blank = conv(bsp_blank,kernel,'same');
        
        
        
        plot(sp_edges,c_fam,'color','k');
        hold all
        plot(sp_edges,c_nov,'color','b');
        plot(sp_edges,c_blank,'color',[.5 .5 .5]);
        mm = max([c_nov ;c_fam])+2;
        ylim([0 mm])
        plot([max(sp_edges) max(sp_edges)],[mm mm-5],'k','linewidth',1)
        axis('tight')
    
    
%     keyboard
    
    
    ax_raster = [ax_raster, axes('position',[loc_stim(is),loc_rasters(sel),0.2,0.1])];
    
%     sorted_trials = ts();
    plot_rastergram(ts_novel,0,1,'line','color',[0 0 0])
    hold all
    plot_rastergram(ts_fam,length(idx_nov),1,'line','color',[0 0 1])
    
    axis('tight')
    
end

plot([3,3-0.5],[-1,-1],'color','k','clipping','off')
text(3-0.25,-1,'500 ms','fontsize',6,'verticalalignment','top','horizontalalignment','center')

set(ax_psth,'visible','off')
set(ax_raster,'visible','off')

if printall
    % set(gcf,'papersize',[figW*2.5,figH*0.3],'paperposition',[0,0,figW*0.2,figH*0.3],'color','none','paperunits','centimeters')
    set(fig,'papersize',[5,4],'paperposition',[0,0,5,4],'color','white','paperunits','centimeters')
    figfolder = 'figures/';
    print(fig,'-dpdf',sprintf('%s/long_new_figure_example_modulations_piriform.pdf',figfolder))
end


%% selected neurons for aon

printall = true;

fig = figure;

experiments = {'AON013','AON010','AON015'};
cells = [6,1,29];


ax_psth = [];
ax_raster = [];
loc_rasters = [0.65,0.35,0.05];
loc_psth = [0.83,0.53,0.23];
loc_stim = [0.1, 0.33];

bin_size = 0.015;
sp_edges = [0:bin_size:4];
sigma = 3;
x = -2*sigma:3*sigma;
kernel = exp(-x .^ 2 / (2*sigma ^ 2)); kernel = kernel./sum(kernel);
dur = 3; 
stim = [6 13;
    13 11;    
    3 10];


is = 1;

for sel = 1:length(experiments)
    dd = data(strcmp({data.exp_name},experiments{sel}));
    ic = cells(sel);
    
    idx_nov = dd.trial_idx(find(dd.trial_type==stim(sel,1)));
    ts_novel = dd.ts_trials{cells(sel)}(idx_nov);
    
    idx_fam = dd.trial_idx(find(dd.trial_type==stim(sel,2)));
    ts_fam = dd.ts_trials{cells(sel)}(idx_fam);
    
     idx_blank = dd.trial_idx(find(dd.trial_type==1));
    ts_blank = dd.ts_trials{cells(sel)}(idx_blank);
    
    ax_psth = [ax_psth, axes('position',[loc_stim(is),loc_psth(sel),0.2,0.08])]; 
        bsp =[];
        tmp_nov = cell2mat(ts_novel);
        bsp_nov = histc(tmp_nov,sp_edges)/length(idx_nov)/bin_size/dur;
        c_nov = conv(bsp_nov,kernel,'same');
        
        tmp_fam = cell2mat(ts_fam);
        bsp_fam = histc(tmp_fam,sp_edges)/length(idx_fam)/bin_size/dur;
        c_fam = conv(bsp_fam,kernel,'same');
        
        tmp_blank = cell2mat(ts_blank);
        bsp_blank = histc(tmp_blank,sp_edges)/length(idx_blank)/bin_size/dur;
        c_blank = conv(bsp_blank,kernel,'same');
        
        
        plot(sp_edges,c_fam,'color','k');
        hold all
        plot(sp_edges,c_nov,'color','b');
        plot(sp_edges,c_blank,'color',[0.5 0.5 0.5]);
        
        ylim([0 max([c_nov ;c_fam])+2])
        plot([max(sp_edges) max(sp_edges)],[max(ylim) max(ylim)-10],'k','linewidth',1)
%         axis('tight')
    
    
%     keyboard
    
    
    ax_raster = [ax_raster, axes('position',[loc_stim(is),loc_rasters(sel),0.2,0.18])];
    
%     sorted_trials = ts();
    plot_rastergram(ts_novel,0,1,'line','color',[0 0 0])
    hold all
    plot_rastergram(ts_fam,length(idx_nov),1,'line','color',[0 0 1])
    
    axis('tight')
    plot([0 3],[length(idx_nov) length(idx_nov)],'color',[0.5 0.5 0.5],'linewidth',1)
    plot([min(xlim) min(xlim)],[0 length(idx_nov)],'color',[0 0 0],'linewidth',1)
    plot([min(xlim) min(xlim)],[length(idx_nov) length(idx_fam)+length(idx_nov)],'color',[0 0 1],'linewidth',1)
    
    if sel==length(experiments)
    plot([2,2-0.5],[-1,-1],'color','k','clipping','off')
    text(2-0.25,-1,'500 ms','fontsize',6,'verticalalignment','top','horizontalalignment','center')
    end
    
    x = [1:12];
    y = dd.mf0(ic,~blank);
    
    ax_hf0(sel) = axes('position',[0.45,loc_rasters(sel)+.05,0.45,0.15]);hold all
    
    errorbar(x,y,dd.sf0(ic,~blank),'o','capsize',0,...
        'markersize',2,'markerfacecolor','k','markeredgecolor','none','color','k'),hold on
    x_i = 1:0.1:12;
    s = pchip(x,y,x_i);
    plot(x_i,s,'k')
    
    mm = max([dd.mf0(ic,~blank)+ dd.sf0(ic,~blank)]);
    plot([1:12],ones(1,12).*nanmean(dd.mf0(ic,blank)),'--','color',[0.5,0.5,0.5])
    set(ax_hf0(sel),'xlim',[0.5 12.5],'ylim',[0 mm])
%     axis('tight')
    
end

% plot([2,2-0.5],[-1,-1],'color','k','clipping','off')
% text(2-0.25,-1,'500 ms','fontsize',6,'verticalalignment','top','horizontalalignment','center')

set(ax_hf0,'tickdir','out','ticklength',get(gca,'ticklength').*5,'xtick',[1:12],'xticklabel',[])
set(ax_hf0(end),'xtick',[1:12],'xticklabel',dd.chemical_list(~blank),'xticklabelrotation',45)
    
set(ax_psth,'visible','off')
set(ax_raster,'visible','off')

if printall
    % set(gcf,'papersize',[figW*2.5,figH*0.3],'paperposition',[0,0,figW*0.2,figH*0.3],'color','none','paperunits','centimeters')
    set(fig,'papersize',[5,4],'paperposition',[0,0,5,4],'color','white','paperunits','centimeters')
    figfolder = 'figures/';
    print(fig,'-dpdf',sprintf('%s/new_figure_example_modulations_aon.pdf',figfolder))
end




%% selected neurons for piriform

printall = true;

fig = figure;

experiments = {'AON003','AON003','AON005'};
cells = [14,17,1];


ax_psth = [];
ax_raster = [];
loc_rasters = [0.65,0.35,0.05];
loc_psth = [0.83,0.53,0.23];
loc_stim = [0.1, 0.33];

bin_size = 0.015;
sp_edges = [0:bin_size:4];
sigma = 3;
x = -2*sigma:3*sigma;
kernel = exp(-x .^ 2 / (2*sigma ^ 2)); kernel = kernel./sum(kernel);
dur = 3; 
stim = [3 10;
    3 9;
    3 10];


is = 1;

for sel = 1:length(experiments)
    dd = data(strcmp({data.exp_name},experiments{sel}));
    ic = cells(sel);
    
    idx_nov = dd.trial_idx(find(dd.trial_type==stim(sel,1)));
    ts_novel = dd.ts_trials{cells(sel)}(idx_nov);
    
    idx_fam = dd.trial_idx(find(dd.trial_type==stim(sel,2)));
    ts_fam = dd.ts_trials{cells(sel)}(idx_fam);
    
     idx_blank = dd.trial_idx(find(dd.trial_type==1));
    ts_blank = dd.ts_trials{cells(sel)}(idx_blank);
    
    ax_psth = [ax_psth, axes('position',[loc_stim(is),loc_psth(sel),0.2,0.08])]; 
        bsp =[];
        tmp_nov = cell2mat(ts_novel);
        bsp_nov = histc(tmp_nov,sp_edges)/length(idx_nov)/bin_size/dur;
        c_nov = conv(bsp_nov,kernel,'same');
        
        tmp_fam = cell2mat(ts_fam);
        bsp_fam = histc(tmp_fam,sp_edges)/length(idx_fam)/bin_size/dur;
        c_fam = conv(bsp_fam,kernel,'same');
        
        tmp_blank = cell2mat(ts_blank);
        bsp_blank = histc(tmp_blank,sp_edges)/length(idx_blank)/bin_size/dur;
        c_blank = conv(bsp_blank,kernel,'same');
        
        
        plot(sp_edges,c_fam,'color','k');
        hold all
        plot(sp_edges,c_nov,'color','b');
        plot(sp_edges,c_blank,'color',[0.5 0.5 0.5]);
        
        ylim([0 max([c_nov ;c_fam])+2])
        plot([max(sp_edges) max(sp_edges)],[max(ylim) max(ylim)-10],'k','linewidth',1)
%         axis('tight')
    
    
%     keyboard
    
    
    ax_raster = [ax_raster, axes('position',[loc_stim(is),loc_rasters(sel),0.2,0.18])];
    
%     sorted_trials = ts();
    plot_rastergram(ts_novel,0,1,'line','color',[0 0 0])
    hold all
    plot_rastergram(ts_fam,length(idx_nov),1,'line','color',[0 0 1])
    
    axis('tight')
    plot([0 3],[length(idx_nov) length(idx_nov)],'color',[0.5 0.5 0.5],'linewidth',1)
    plot([min(xlim) min(xlim)],[0 length(idx_nov)],'color',[0 0 0],'linewidth',1)
    plot([min(xlim) min(xlim)],[length(idx_nov) length(idx_fam)+length(idx_nov)],'color',[0 0 1],'linewidth',1)
    
    if sel==length(experiments)
    plot([2,2-0.5],[-1,-1],'color','k','clipping','off')
    text(2-0.25,-1,'500 ms','fontsize',6,'verticalalignment','top','horizontalalignment','center')
    end
    
    x = [1:12];
    y = dd.mf0(ic,~blank);
    
    ax_hf0(sel) = axes('position',[0.45,loc_rasters(sel)+.05,0.45,0.15]);hold all
    
    errorbar(x,y,dd.sf0(ic,~blank),'o','capsize',0,...
        'markersize',2,'markerfacecolor','k','markeredgecolor','none','color','k'),hold on
    x_i = 1:0.1:12;
    s = pchip(x,y,x_i);
    plot(x_i,s,'k')
    
    mm = max([dd.mf0(ic,~blank)+ dd.sf0(ic,~blank)]);
    plot([1:12],ones(1,12).*nanmean(dd.mf0(ic,blank)),'--','color',[0.5,0.5,0.5])
    set(ax_hf0(sel),'xlim',[0.5 12.5],'ylim',[0 mm])
%     axis('tight')
    
end


set(ax_hf0,'tickdir','out','ticklength',get(gca,'ticklength').*5,'xtick',[1:12],'xticklabel',[])
set(ax_hf0(end),'xtick',[1:12],'xticklabel',dd.chemical_list(~blank),'xticklabelrotation',45)
    
set(ax_psth,'visible','off')
set(ax_raster,'visible','off')

if printall
    % set(gcf,'papersize',[figW*2.5,figH*0.3],'paperposition',[0,0,figW*0.2,figH*0.3],'color','none','paperunits','centimeters')
    set(fig,'papersize',[5,4],'paperposition',[0,0,5,4],'color','white','paperunits','centimeters')
    figfolder = 'figures/';
    print(fig,'-dpdf',sprintf('%s/new_figure_example_modulations_piriform.pdf',figfolder))
end