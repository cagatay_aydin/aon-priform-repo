clear all
close all
clc


%
varnames = {'exp_name','ts','session_name',...
    'stim_type',...
    'ts_trials',...
    'f0','trial_type','trial_idx',...
    'chemical_list','left_right','trial_occur',...
    'recording_site','mf0','sf0','novelty','blank','sig','resp_h'};
dataset = 'lr';
foldername = '.';
[data] = get_data(foldername,dataset,varnames);
disp('Data loaded.')

%%

tmp = arrayfun(@(x)(x.recording_site),data,'uniformoutput',0);
rsite = horzcat(tmp{:});

all_f0 = [];
all_sig = [];
all_resp = [];
for expt = 1:length(data)
    dd = data(expt);
    tmp = dd.mf0;
    if ~isempty(tmp)
    all_f0 = [all_f0;tmp(:,1:14)];
    all_sig = [all_sig;nansum(dd.sig,2)];
    all_resp = [all_resp;dd.resp_h];
    end
%     all_f0 = vertcat(data.mf0);
end
% all_f0 = vertcat(data.mf0);
blank = logical(data(1).blank);
novelty = logical(data(1).novelty);


%% for 2 groups
clf

printfigure = false;
clib = {[146/255,71/255,149/255],[63/255,143/255,56/255]};
sig_idx = [all_sig>=1]';
resp_idx = [all_resp>0]';
% cond = {strcmp(rsite,'aon')&sig_idx,...
%         strcmp(rsite,'piriform')&sig_idx};
cond = {strcmp(rsite,'aon')&resp_idx,...
        strcmp(rsite,'piriform')&resp_idx};
edges = 1:1:15;
ax = [];
mm = {};
for c = 1:length(cond)
    idx = cond{c};
    mm{c} = all_sig(idx);
    bins=histc(mm{c},edges);
    ss = cumsum(bins);
    ss = ss/max(ss);
    ax = subplot(1,1,1);
    plot(edges,ss,'color',clib{c},'linewidth',1);hold all
%     bb = bar(edges,mb);
%     set(bb,'facecolor',clib{c},'edgecolor','none'),hold all
end


[h p]=kstest2(mm{1},mm{2})

ylabel('CDF')
xlabel('Selectivity(#odors)')

set(ax,'box','off','xlim',[1 15],'ylim',[0 1],'tickdir','out',...
    'ticklength',get(ax(end),'ticklength').*5,'xtick',[1 3 5 7 9 11 13 15]) 


axis('square')
set(gcf,'paperunits','centimeters','papersize',[4.2*1,4.2*1],...
    'paperposition',[0,0,4.2*1,4.2*1])

set(findall(gcf,'-property','FontSize'),'FontSize',8)

printfolder = 'E:\local\users\cagatay\aon-priform\figures\manuscript';
if printfigure
    print(gcf,'-dpdf',sprintf('%s\\selectivity_cumsum_compare.pdf',printfolder))
end

%% bar 2 groups
clf

printfigure = false;
clib = {[146/255,71/255,149/255],[63/255,143/255,56/255]};
sig_idx = [all_sig>=1]';
cond = {strcmp(rsite,'aon')&sig_idx,...
        strcmp(rsite,'piriform')&sig_idx};

edges = 1:3:15;
ax = [];
mm = {};
for c = 1:length(cond)
    idx = cond{c};
    mm{c} = all_sig(idx);
    bins=histc(mm{c},edges);
    ss = cumsum(bins);
    ss = ss/max(ss);
    ax = subplot(1,1,1);
%     plot(edges,ss,'color',clib{c});hold all
    bb = bar(edges,bins./sum(bins));
    set(bb,'facecolor','none','edgecolor',clib{c},'linewidth',1),hold all
end


% [h p]=kstest2(mm{1},mm{2})

ylabel('CDF')
xlabel('Selectivity(#odors)')

% set(ax,'box','off','xlim',[1 15],'ylim',[0 1],'tickdir','out',...
%     'ticklength',get(ax(end),'ticklength').*5,'xtick',[1 3 5 7 9 11 13 15]) 

set(ax,'box','off','xlim',[-1 15],'ylim',[0 .6],'tickdir','out',...
    'ticklength',get(ax(end),'ticklength').*5,'xtick',[1 3 5 7 9 11 13 15]) 

axis('square')
set(gcf,'paperunits','centimeters','papersize',[4.2*1,4.2*1],...
    'paperposition',[0,0,4.2*1,4.2*1])

set(findall(gcf,'-property','FontSize'),'FontSize',8)

printfolder = 'E:\local\users\cagatay\aon-priform\figures\manuscript';
if printfigure
    print(gcf,'-dpdf',sprintf('%s\\bar_cumsum_compare.pdf',printfolder))
end



%% for 3 groups
clf

printfigure = true;
clib = {[146/255,71/255,149/255],[0 0 1],[63/255,143/255,56/255]};
sig_idx = [all_sig>=1]';
cond = {strcmp(rsite,'aon')&sig_idx,...
    strcmp(rsite,'endopiriform')&sig_idx,...
    strcmp(rsite,'piriform')&sig_idx};

edges = 1:1:15;
ax = [];
for c = 1:length(cond)
    idx = cond{c};
    mm = all_sig(idx);
    bins=histc(mm,edges);
    ss = cumsum(bins);
    ss = ss/max(ss);
    ax = subplot(1,1,1);
    plot(edges,ss,'color',clib{c});hold all
%     bb = bar(edges,mb);
%     set(bb,'facecolor',clib{c},'edgecolor','none'),hold all
end

ylabel('CDF')
xlabel('Selectivity(#odors)')

set(ax,'box','off','xlim',[1 15],'ylim',[0 1],'tickdir','out',...
    'ticklength',get(ax(end),'ticklength').*5,'xtick',[1:15])


axis('square')
set(gcf,'paperunits','centimeters','papersize',[4.2*1,4.2*1],...
    'paperposition',[0,0,4.2*1,4.2*1])

set(findall(gcf,'-property','FontSize'),'FontSize',8)

printfolder = 'E:\local\users\cagatay\aon-priform\figures\manuscript';
if printfigure
    print(gcf,'-dpdf',sprintf('%s\\selectivity_cumsum_compare.pdf',printfolder))
end



%%

respidx = [];
for ic = 1:length(all_f0)
    bresp = all_f0(ic,blank);
    fresp = all_f0(ic,~blank&~novelty);
    nresp = all_f0(ic,~blank&novelty);
    
    [hf,pf]=ttest2(bresp,fresp)
    [hn,pn] = ttest2(bresp,nresp)
    
    if hf==1||hn==1
        respidx = [respidx,1];
    else
        respidx = [respidx,0];
    end
    
    
end

%%
respidx = [];
for iexp= 1:length(data)
    dd = data(iexp);
    rtype = nan(length(dd.trial_type),1);
    ttype = dd.trial_type;
    ntype = unique(ttype);
    
    for it = 1:length(ntype)
        
        if blank(it)
            rtype(ttype==it)=1;
        else if novelty(it)
                rtype(find(ttype==it))=3;
            else
                rtype(find(ttype==it))=2;
            end
        end
    end
    
    for ic = 1:length(dd.ts)
        rb = dd.f0{ic}(rtype==1);
        rf = dd.f0{ic}(rtype==2);
        rn = dd.f0{ic}(rtype==3);
        [hf,pf]=ttest2(rb,rf);
        [hn,pn] = ttest2(rb,rn);
    
    if hf==1||hn==1
        respidx = [respidx,1];
    else
        respidx = [respidx,0];
    end
    end
    
    
end



%%

ncluster = 4;
[c, ~,sumd]= kmeans(allresp,ncluster,'Replicates',10);
idx = 1:length(c);
[v,ii] = sort(c);
sortedidx = idx(ii);
sorted_avg = allresp(sortedidx,:);

%%

clf
imagesc(sorted_avg)
