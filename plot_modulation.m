clear all
close all
clc


%
varnames = {'exp_name','ts','session_name',...
    'stim_type',...
    'ts_trials',...
    'f0','trial_type','trial_idx',...
    'chemical_list','left_right','trial_occur',...
    'recording_site','mf0','sf0','novelty','blank','sig','resp_h','mod_ind_nov'};
dataset = 'lr';
foldername = '.';
[data] = get_data(foldername,dataset,varnames);
disp('Data loaded.')

%%

tmp = arrayfun(@(x)(x.recording_site),data,'uniformoutput',0);
rsite = horzcat(tmp{:});
blank = logical(data(1).blank);
novelty = logical(data(1).novelty);


%%
pc_mod_ind = [];
all_hab = [];
ste_hab = [];
all_sig = [];
norm_hab = [];
all_famil = [];
all_novel = [];
all_mag = [];
all_mod_ind = [];
all_resp_idx = [];
for iexpt = 1:length(data)
    
    dd = data(iexpt);
    if ~isempty(dd.f0)
        
        oc = dd.trial_occur;
        ttype = dd.trial_type;
        tidx = dd.trial_idx;
        ntype = unique(dd.trial_type);
        noccur = 6;%max(unique(oc));
        
        hab_resp = nan(length(dd.ts),noccur);
        norm_hab_resp = nan(length(dd.ts),noccur);
        hab_ste = nan(length(dd.ts),noccur);
        sig = nan(length(dd.ts),1);
        resp_h = nan(length(dd.ts),1);
        mag = nan(length(dd.ts),1);
        mod_ind = nan(length(dd.ts),1);
        tmp_fresp = {};
        tmp_nresp = {};
        
        for ic = 1:length(dd.ts)
            bl = mean(dd.mf0(ic,blank));
            resp = mean(dd.mf0(ic,~blank));
            mag(ic) = (resp-bl)/(resp+bl);
            
            sig(ic) = nansum(dd.sig(ic,:),2);
            resp_h(ic) = nansum(dd.resp_h(ic,:),2);
            sf0 = dd.f0{ic}(tidx);
            
            novel_odors = ntype(find(novelty));
            
            indix = [];
            ocidx = nan(noccur,length(novel_odors));
            odidx = nan(noccur,length(novel_odors));
            
            for io = 1:noccur
                
                for in = 1:length(novel_odors)
                    try
                        n = novel_odors(in);
%                         n = ntype(in);
                        ocidx(io,in) = find(io==oc&ttype == n);
                        odidx(io,in) = ttype(find(io==oc&ttype==n));
                    catch fprintf('no trials for %d rep for odor %d\n',io,n)
                    end
                    
                end
                
            end
            
            n_novel_odors = size(ocidx,2);
            nresp = [];
            fresp = [];
            for ii = 1:n_novel_odors
                f_idx = ocidx([4:6],ii); %familiar
                n_idx = ocidx([1:3],ii); %novel
                
                if sum(isnan(f_idx))>1
                    continue
                else
                    fresp = [fresp,nanmean(sf0(f_idx(~isnan(f_idx))))];
                    nresp= [nresp,nanmean(sf0(n_idx(~isnan(n_idx))))];
                end
            end
            %             keyboard
            % a = [fresp;nresp]
            tmp_fresp{ic} = fresp;
            tmp_nresp{ic} = nresp;
            [mod_ind(ic),~] = modulation_index(fresp,nresp);
            
        end
    else
        continue
    end
    
    all_famil = [all_famil tmp_fresp];
    all_novel = [all_novel tmp_nresp];
    all_mod_ind = [all_mod_ind;mod_ind.*100];
    all_mag = [all_mag; mag];
    all_sig = [all_sig;sig];
    all_resp_idx = [all_resp_idx;resp_h];
    pc_mod_ind = [pc_mod_ind,dd.mod_ind_nov.*100];
    
end


%% all modulation index

clf

printfigure = true;
clib = {[146/255,71/255,149/255],[63/255,143/255,56/255]};
sig_idx = [all_sig>=1]';
resp_idx = [all_resp_idx>0]';
% cond = {strcmp(rsite,'aon')&sig_idx,...
%     strcmp(rsite,'piriform')&sig_idx};
cond = {strcmp(rsite,'aon')&resp_idx,...
    strcmp(rsite,'piriform')&resp_idx};



edges = -100:1:150;
ax = [];
mm = {};
for c = 1:length(cond)
    idx = cond{c};
    mm{c} = all_mod_ind(idx);
    bins=histc(mm{c},edges);
    ss = cumsum(bins);
    ss = ss/max(ss);
    ax = subplot(1,1,1);
    plot(edges,ss,'color',clib{c});hold all
    plot([0 0],[0 1],'--','color',[.5 .5 .5])
    %     bb = bar(edges,mb);
    %     set(bb,'facecolor',clib{c},'edgecolor','none'),hold all
end


[h p]=kstest2(mm{1},mm{2})

ylabel('CDF')
xlabel('Modulation Index')

set(ax,'box','off','tickdir','out',...
    'ticklength',get(ax(end),'ticklength').*5,'xlim',[min(edges),max(edges)])


axis('square')
set(gcf,'paperunits','centimeters','papersize',[4.2*1,4.2*1],...
    'paperposition',[0,0,4.2*1,4.2*1])

set(findall(gcf,'-property','FontSize'),'FontSize',8)

printfolder = 'E:\local\users\cagatay\aon-priform\figures\manuscript';
if printfigure
    print(gcf,'-dpdf',sprintf('%s\\modulation_cumsum_compare.pdf',printfolder))
end

%% positive modulation index

clf

printfigure = true;
clib = {[146/255,71/255,149/255],[63/255,143/255,56/255]};
sig_idx = [all_sig>=1]';
% cond = {strcmp(rsite,'aon')&sig_idx&[all_mag>0]',...
%         strcmp(rsite,'piriform')&sig_idx&[all_mag>0]'};

cond = {strcmp(rsite,'aon')&sig_idx&[all_mod_ind>0]',...
    strcmp(rsite,'piriform')&sig_idx&[all_mod_ind>0]'};

edges = 0:1:150;
ax = [];
mm = {};
for c = 1:length(cond)
    idx = cond{c};
    mm{c} = all_mod_ind(idx);
    bins=histc(mm{c},edges);
    ss = cumsum(bins);
    ss = ss/max(ss);
    ax = subplot(1,1,1);
    plot(edges,ss,'color',clib{c});hold all
    
    %     bb = bar(edges,mb);
    %     set(bb,'facecolor',clib{c},'edgecolor','none'),hold all
end


[h p]=kstest2(mm{1},mm{2})

ylabel('CDF')
xlabel('Modulation Index')

set(ax,'box','off','tickdir','out',...
    'ticklength',get(ax(end),'ticklength').*5)


axis('square')
set(gcf,'paperunits','centimeters','papersize',[4.2*1,4.2*1],...
    'paperposition',[0,0,4.2*1,4.2*1])

set(findall(gcf,'-property','FontSize'),'FontSize',8)

printfolder = 'E:\local\users\cagatay\aon-priform\figures\manuscript';
if printfigure
    print(gcf,'-dpdf',sprintf('%s\\positive_modulation_cumsum_compare.pdf',printfolder))
end

%%
clf

printfigure = true;
clib = {[146/255,71/255,149/255],[63/255,143/255,56/255]};
sig_idx = [all_sig>=1]';
% cond = {strcmp(rsite,'aon')&sig_idx&[all_mag>0]',...
%         strcmp(rsite,'piriform')&sig_idx&[all_mag>0]'};

cond = {strcmp(rsite,'aon')&sig_idx&[all_mod_ind<0]',...
    strcmp(rsite,'piriform')&sig_idx&[all_mod_ind<0]'};

edges = 0:1:150;
ax = [];
mm = {};
for c = 1:length(cond)
    idx = cond{c};
    mm{c} = -1.*all_mod_ind(idx);
    bins=histc(mm{c},edges);
    ss = cumsum(bins);
    ss = ss/max(ss);
    ax = subplot(1,1,1);
    plot(edges,ss,'color',clib{c});hold all
    
    %     bb = bar(edges,mb);
    %     set(bb,'facecolor',clib{c},'edgecolor','none'),hold all
end


[h p]=kstest2(mm{1},mm{2})

ylabel('CDF')
xlabel('Modulation Index')

set(ax,'box','off','tickdir','out',...
    'ticklength',get(ax(end),'ticklength').*5,'xtick',[0 50 100 150],...
    'xticklabel',[0 -50 -100 -150])


axis('square')
set(gcf,'paperunits','centimeters','papersize',[4.2*1,4.2*1],...
    'paperposition',[0,0,4.2*1,4.2*1])

set(findall(gcf,'-property','FontSize'),'FontSize',8)

printfolder = 'E:\local\users\cagatay\aon-priform\figures\manuscript';
if printfigure
    print(gcf,'-dpdf',sprintf('%s\\negative_modulation_cumsum_compare.pdf',printfolder))
end




%%

clf

marker_size = 1;
loc_rasters = [0.65,0.45,0.25,0.05];
ax = [];

example_cells = [7 57 10 34 ];



for ii = 1:length(example_cells)
    
    ax = [ax,subplot(2,2,ii)];;
    ic = example_cells(ii);
    msta = all_famil{ic};
    mloc = all_novel{ic};
    mm = max([msta,mloc]);
    
    plot([0,mm],[0,mm],'k'), hold all
    plot(msta,mloc,'ko','markersize',marker_size,'markerfacecolor','none','markeredgecolor','k')
    
    plot(msta(:),mloc(:),'ko','markersize',marker_size,'markerfacecolor','k','markeredgecolor','k')
    
    [modind] = modulation_index(msta,mloc);
    plot([0,mm],polyval([modind+1,0],[0,mm]),'r')
    axis([0,mm*1.01,0,mm*1.01])
    text(mm,0,{rsite{ic}, sprintf('%2.0f',modind*100)},...
        'fontsize',8,'verticalalignment','bottom','horizontalalignment','right','color','r')
    axis square
    
    
    
    
    
    
end

set(ax,'color','w','tickdir','out','ticklength',...
    get(ax(1),'ticklength')*5,'fontsize',8,'linewidth',.5,'box','off')

set(gcf,'papersize',[4.2*2,4.2*2],'paperposition',[0,0,4.2*2,4.2*2],'color','white','paperunits','centimeters')
    figfolder = 'figures/';
    print(gcf,'-dpdf',sprintf('%s/example_modulations.pdf',figfolder))

%% modulation vs positive negative


%%
clf

printfigure = true;
clib = {[146/255,71/255,149/255],[63/255,143/255,56/255]};
sig_idx = [all_sig>=1]';
% cond = {strcmp(rsite,'aon')&sig_idx&[all_mag>0]',...
%         strcmp(rsite,'piriform')&sig_idx&[all_mag>0]'};

cond = {strcmp(rsite,'aon')&sig_idx,...
    strcmp(rsite,'piriform')&sig_idx};

edges = 0:1:150;
ax = [];
mm = {};


for c = 1:length(cond)
    idx = cond{c};
    ax = subplot(1,1,1);
    plot(all_mod_ind(idx),all_mag(idx),'o','color',clib{c},'markersize',1,...
        'markerfacecolor',clib{c})
    hold all
end

ylabel('Sign')
xlabel('Modulation Index')

set(ax,'box','off','tickdir','out',...
    'ticklength',get(ax(end),'ticklength').*5,'xlim',[-100 100],'ylim',[-.6 .6])


axis('square')
set(gcf,'paperunits','centimeters','papersize',[4.2*1,4.2*1],...
    'paperposition',[0,0,4.2*1,4.2*1])

set(findall(gcf,'-property','FontSize'),'FontSize',8)

printfolder = 'E:\local\users\cagatay\aon-priform\figures\manuscript';
if printfigure
    print(gcf,'-dpdf',sprintf('%s\\scatter_sign_modulation.pdf',printfolder))
end

%%


clf

printfigure = false;
clib = {[146/255,71/255,149/255],[63/255,143/255,56/255]};
sig_idx = [all_sig>=1]';
resp_idx = [all_resp_idx>0]';
% cond = {strcmp(rsite,'aon')&sig_idx,...
%     strcmp(rsite,'piriform')&sig_idx};
cond = {strcmp(rsite,'aon')&resp_idx,...
    strcmp(rsite,'piriform')&resp_idx};



edges = -100:1:150;
ax = [];
mm = {};
for c = 1:length(cond)
    idx = cond{c};
    mm{c} = all_mod_ind(idx);
    bins=histc(mm{c},edges);
    ss = cumsum(bins);
    ss = ss/max(ss);
    ax = [ax,subplot(2,1,c)];
    plot(edges,ss,'color',clib{c});hold all
    plot([0 0],[0 1],'--','color',[.5 .5 .5])
    
    mm{c} = pc_mod_ind(idx);
    bins=histc(mm{c},edges);
    ss = cumsum(bins);
    ss = ss/max(ss);
    ax = [ax,subplot(2,1,c)];
    plot(edges,ss,'color',[.5 0 0]);hold all
    plot([0 0],[0 1],'--','color',[.5 .5 .5])
    %     bb = bar(edges,mb);
    %     set(bb,'facecolor',clib{c},'edgecolor','none'),hold all
end


[h p]=kstest2(mm{1},mm{2})

ylabel('CDF')
xlabel('Modulation Index')

set(ax,'box','off','tickdir','out',...
    'ticklength',get(ax(end),'ticklength').*5,'xlim',[min(edges),max(edges)])


axis(ax,'square')
set(gcf,'paperunits','centimeters','papersize',[4.2*1,4.2*1],...
    'paperposition',[0,0,4.2*1,4.2*1])

set(findall(gcf,'-property','FontSize'),'FontSize',8)

printfolder = 'E:\local\users\cagatay\aon-priform\figures\manuscript';
if printfigure
    print(gcf,'-dpdf',sprintf('%s\\modulation_cumsum_compare.pdf',printfolder))
end
    