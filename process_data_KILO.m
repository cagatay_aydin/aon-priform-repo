% Animal name

cd E:\local\users\cagatay\kilosort-aon-pir
clear all
close all
clc

experiments = {'181114_MB050',...
    '181115_MB049',...
    '181115_MB050',...
    '181122_MB052',...
    '181123_MB052',...
    '181130_MB053',...
    '181201_MB053'
    };

% Depth

region = {'piriform',...
    'piriform',...
    'piriform',...
    'aon',...
    'aon',...
    'aon',...
    'aon'...
    };



bregma = [2.68,...
    2.68];

depth_th = [1000,...
    1000,...
    1000,...
    1500,...
    1500,...
    1500,...
    1500,...
    ];

chemical_list = {'Blank',...
    'Anisole',...
    'Ethyl valerate',...
    'Eugenol',...
    'Limonele',...
    '2,3,5-trimethylpyrazine',...
    'Benzyl acetate',...
    'Blank',...
    'Cinnamaldehyde',...
    'Pentenoic acid',...
    'Linalyl formate',...
    'Geraniol',...
    'Thiophene',...
    'Phenethyl alcohol'...
    };

%uses the channel that has proper bits
tval_idx = [1;1;1;1;1;1;1;1;1;1];

%%

sort_folder = 'ext_KILO_data';
events_folder = 'events_data';
tdms_folder = 'tdms_data';
stimlog_folder = 'stimlog_data';
raw_nlx_folder = 'raw_neuropixels_data';

processed_folder = 'processed_data';

% left right
session_name = {'lr'};

ts = [];

for e = 1:length(experiments)%1:length(experiments)%1:length(experiments)% 1:length(experiments)
    
    % no right left only from center
    left_right = [0;0;0;0;0;0;0;0;0;0;0;0;0;0];
    % novelty = 1, not novel = 0
    novelty = [0;0;0;1;1;1;1;0;0;0;1;1;1;1];
    blank =   [1;0;0;0;0;0;0;1;0;0;0;0;0;0];
    fam =     [0;1;1;0;0;0;0;0;1;1;0;0;0;0];
    
    exp_name = experiments{e};
    stim_type = session_name;
    
    % READING EVENTS FROM RAW BIN FILE
    events_destination = fullfile([events_folder,'/',experiments{e}]);
    tmp = list_files(events_destination,'*.mat');
    if isempty(tmp)
        % convert to mat file takes some time and memory
        raw_destination = fullfile([raw_nlx_folder,'/',experiments{e}]);
        events_destination = fullfile([events_folder,'/',experiments{e}]);
        mkdir([events_folder,'/',experiments{e}])
        extract_neuropixels_tiggers(raw_destination,events_destination)
        tmp = list_files(events_destination,'*.mat');
        tm = load(tmp{1});
    else
        tmp = list_files(events_destination,'*.mat');
        tm = load(tmp{1});
    end
    a = tm.allTriggersS{1}{1};
    all_events = a(diff(a)>.015);
    
    no_file = 0;
    %     LOADING TDMS FILE
    tmp = list_files([tdms_folder,'/',experiments{e}],'*.mat');
    if isempty(tmp)
        tmp = list_files([tdms_folder,'/',experiments{e}],'*.tdms');
        if isempty(tmp)
            no_file = 1;
            disp('No TDMS file found')
        else
            
            tm = convertTDMS(true,tmp{1});
            tmp = list_files([tdms_folder,'/',experiments{e}],'*.mat');
            tm = load(tmp{1});
        end
    else
        tmp = list_files([tdms_folder,'/',experiments{e}],'*.mat');
        tm = load(tmp{1});
    end
    
    % READING EVENTS IN TDMS FILE
    if no_file ~= 1
        a=tm.ConvertedData.Data.MeasuredData(3).Data;
        
        srate = 1000;
        time = linspace(0,length(a)/srate,length(a));
        
        ttmp= (a>0.4);
        d_events = diff(ttmp)>0;
        
        tdms_events = time(find(d_events));
        diff_events = min(all_events)-min(tdms_events);
        aligned_events = (tdms_events-min(tdms_events))+min(all_events);
        
        fprintf('ap.bin file has %d events\n',length(all_events));
        fprintf('TDMS file has %d events\n',length(tdms_events));
        fprintf('Difference in time %4.1f s\n',diff_events);
        events.onsets = all_events(2:4:end);
        events.offsets = events.onsets(2:end);
        events.raw = aligned_events;
    else
        fprintf('ap.bin file has %d events\n',length(all_events));
        events.onsets = all_events(2:4:end);
        events.offsets = events.onsets(2:end);
        events.raw = events.onsets;
    end
    
    
    
    file_name = sprintf('%s/%s/%s_behaviourdata_%s.mat',processed_folder,...
        experiments{e},exp_name,stim_type{:});
    
    mkdir([processed_folder,'/',experiments{e}])
    save(file_name,'-struct','events');
    save(file_name,'exp_name','session_name','stim_type','events','-append');
    
    
    %LOADING MCLUST DATA
    tmp_ts = {};
    ts_label = {};
    recording_site = {};
    
    
    tmpfolder = sprintf('%s\\%s',sort_folder,experiments{e});
    [units, tmp_ts, waves, fets, probeinfo, param] = processDataFromKILO_neuropixels(fullfile(tmpfolder),...
        'phase3a', 4500, region{e}, {14}, 20.0);
  
    
    
%     depth_th = 2000;
    dcount = 1;
    ts = {};
    for ii = 1:length(tmp_ts)
        if units(ii).depth<=depth_th(e)
            ts(dcount) = tmp_ts(ii);
            dcount = dcount+1;
        else
            continue
        end
    end
    
    ncells = length(ts);
%     keyboard
    fprintf('Done getting units for %s N= %d\n',exp_name,ncells)
            
        
    
    file_name = sprintf('%s/%s/%s_spikedata_%s.mat',processed_folder,...
        experiments{e},exp_name,stim_type{:});
    save(file_name,...
        'ts','ts_label','exp_name','session_name','stim_type');
    
    
    % LOADING STIMLOG FILE ('.MAT')
    tmp = list_files([stimlog_folder,'/',experiments{e}],'*.mat');
    m = load(tmp{1});
    
    [~, tmp] = sort(m.trial.odor_ID);
    %     keyboard
    if length(tmp)==length(events.onsets)
        [trial_type, trial_idx] = sort(m.trial.odor_ID);
    else
        disp('TDMS events and NLX events are not matching')
        [trial_type, trial_idx] = sort(m.trial.odor_ID(1:length(events.onsets)));
    end
    
    
    trial_occur = m.trial.count(trial_idx);
    %     keyboard
    ttypes = unique(trial_type);
    ntypes = length(ttypes);
    mf0 = nan(length(ts),ntypes);
    sf0 = nan(length(ts),ntypes);
    blf0 = nan(length(ts),ntypes);
    sig_val = nan(length(ts),ntypes);
    sig = nan(length(ts),ntypes);
    c_depth = nan(length(ts),ntypes);
    mod_ind_nov = nan(1,length(ts));
    mod_ind_fam = nan(1,length(ts));
    resp_h = nan(length(ts),1);
    resp_p = nan(length(ts),1);
    ts_trials = {};
    ts_trials_before = {};
    ts_trials_after = {};
    ts_trials_selective = {};
    f0 = {};
    f0_before = {};
    f0_after = {};
    f0_bl = {};
    
    % COMPUTE TRIAL DATA
    %     keyboard
    if isempty(ts)
        ts_trials = {};
        ts_trials_before = {};
        ts_trials_after = {};
        ts_trials_selective = {};
        f0 = {};
        f0_before = {};
        f0_after = {};
        f0_bl = {};
        sig_val =[];
        sig = [];
        resp_h =[];
        resp_p =[];
         mod_ind_nov = nan(1,length(ts));
    mod_ind_fam = nan(1,length(ts));
    else
        for c = 1:ncells
            
            recording_site{c} = region{e};
            ts_trials{c} = extract_trial_spikes(ts{c},events.onsets,events.onsets+3);
            %             ts_trials{c} = extract_trial_spikes(ts{c},events.onsets-1,events.onsets+3);
            [f0{c},f1{c},f2{c}]=compute_fourier_visual_responses(ts_trials{c},...
                3,[0],5,1);
            %             [f0{c},f1{c},f2{c}]=compute_fourier_visual_responses(ts_trials{c},...
            %                 4,[0],5,1);
            
            ts_trials_before{c} = extract_trial_spikes(ts{c},events.onsets,events.onsets+1);
            [f0_before{c},f1{c},f2{c}]=compute_fourier_visual_responses(ts_trials_before{c},...
                3,[0],5,1);
            
            ts_trials_after{c} = extract_trial_spikes(ts{c},events.onsets+1,events.onsets+3);
            [f0_after{c},f1{c},f2{c}]=compute_fourier_visual_responses(ts_trials_after{c},...
                3,[0],5,1);
            
            ts_trials_selective{c} = extract_trial_spikes(ts{c},events.onsets-3,events.onsets);
            [f0_bl{c},f1{c},f2{c}]=compute_fourier_visual_responses(ts_trials_selective{c},...
                3,[0],5,1);
            
            
            
            [resp_h(c), resp_p(c)] = ttest2(f0_before{c},f0_after{c});
            %             keyboard
            sorted_f0 = f0{c}(trial_idx);
            sorted_f0_sel = f0_bl{c}(trial_idx);
            tmp_trials = cell(ntypes,1);
            tmp_trials_sel = cell(ntypes,1);
            count = 0;
            for ii = 1:ntypes
                it = ttypes(ii);
                tmp_trials{ii} = sorted_f0(trial_type==it);
                tmp_trials_sel{ii} = sorted_f0_sel(trial_type==it);
                mf0(c,ii) = mean(sorted_f0(trial_type==it));
%                 keyboard
                blf0(c,ii) = mean(sorted_f0(trial_type==it)-sorted_f0_sel(trial_type==it));
                sf0(c,ii) = std(sorted_f0(trial_type==it))./sqrt(sum(trial_type==it));
            end
%                         keyboard
            
            %           compute modulation
            tmp_resp = nan(length(tmp_trials),2);
            for ii = 1:length(tmp_trials)
                tmp = tmp_trials{ii};
                
                try
                    tmp_resp(ii,1) = nanmean(tmp(1:3));
                    tmp_resp(ii,2) = nanmean(tmp(4:6));
                    
                catch fprintf('no trials for %d rep for odor %d\n',ii)
                end
                
            end
            
            resp = tmp_resp(logical(novelty),1);
            resp = resp(~isnan(resp));
            base = tmp_resp(logical(novelty),2);
            base = base(~isnan(base));
            mm = min([length(base),length(resp)]);
            [mod_ind_nov(c),~] = modulation_index(base(1:mm),resp(1:mm));
            
            resp = tmp_resp(logical(fam),1);
            resp = resp(~isnan(resp));
            base = tmp_resp(logical(fam),2);
            base = base(~isnan(base));
            mm = min([length(base),length(resp)]);
            [mod_ind_fam(c),~] = modulation_index(base(1:mm),resp(1:mm));
            
            
            
            %           bl_trials = f0_selective{c};
            %            bl_trials = horzcat(tmp_trials{logical(blank)});
            odors = find(~blank);
            p = nan(ntypes,1);
            h = nan(ntypes,1);
            for io = 1:length(odors)
                resp_trials =tmp_trials{odors(io)};
                bl_trials = tmp_trials_sel{odors(io)};
                [tmp_h, tmp_p] = ttest2(bl_trials,resp_trials);
                p(odors(io)) = tmp_p;
                h(odors(io)) = tmp_h;
            end
            %            keyboard
            sig_val(c,:) = p;
            sig(c,:) = h;
            
            %compute habutiation
            
            c_depth(c) = units(c).depth;
        end
    end
    
    
    file_name = sprintf('%s/%s/%s_odortrialdata_%s.mat',processed_folder,...
        experiments{e},exp_name,stim_type{:});
    
    save(file_name,...
        'exp_name','session_name','stim_type',...
        'ts_trials','f0','trial_idx','trial_type','chemical_list',...
        'left_right','trial_occur','recording_site','mf0','sf0','novelty',...
        'blank','sig_val','sig','resp_h','resp_p','mod_ind_fam','mod_ind_nov',...
        'c_depth','blf0','f0_bl')
    
    
end