clear all
close all
clc

%%
parent_folder = 'E:\local\users\cagatay\PFC\';
ext_KILO_folder = 'ext_KILO_data\';
raw_KILO_folder = 'raw_neuropixels_data\';
events_folder = 'events_data\';
d = dir(sprintf('%s%s',parent_folder,ext_KILO_folder));
isub = [d(:).isdir];
nameFolds = {d(isub).name}';
nameFolds(ismember(nameFolds,{'.','..'})) = [];

%%
ii = 1;

lfpFs = 2500;  % neuropixels phase3a
nChansInFile = 385;  % neuropixels phase3a, from spikeGLX

tmp = list_files(sprintf('%s%s%s',parent_folder,ext_KILO_folder,nameFolds{ii}),'*.channelmap');

% dlmread(tmp{1},',',1,0)
fileID= fopen(tmp{1});

C = textscan(fileID,'%d %d %d %d %d %d','delimiter',',','headerlines',1);

fclose(fileID);
c_idx = find(C{3}==0&C{5}<3300);
ch_name= C{1}(c_idx);
ch_connect = C{3}(c_idx);
ch_x = C{4}(c_idx);
ch_y = C{5}(c_idx);



%% snapshot of the raw lfp signal
% ii = 3;

n_ch = length(ch_name); 
nChansInFile =385;
% nChansInFile =n_ch;
lfpFs = 2500;

tmp = list_files(sprintf('%s%s%s',parent_folder,raw_KILO_folder,nameFolds{ii}),'*.lf.bin');
lfpFilename = tmp{1};

d= dir(lfpFilename);
nSamps = d.bytes/2/nChansInFile;
nClips = 10;
clipDur = 0.35;

sampStarts = round(linspace(lfpFs*10, nSamps, nClips+1));
nClipSamps = round(lfpFs*clipDur);

[b,a]=butter(3,[300]*2/(lfpFs),'low');

mmf = memmapfile(lfpFilename, 'Format', {'int16', [nChansInFile nSamps], 'x'});

%
% n_ch = 385; 

raw_lfp = zeros(nClips,n_ch,nClipSamps);
lfp = zeros(nClips,n_ch,nClipSamps);
filt_lfp = zeros(nClips,n_ch,nClipSamps);
med_lfp = zeros(nClips,n_ch,nClipSamps);
whit_lfp = zeros(nClips,n_ch,nClipSamps); 

for n = 1:nClips
    fprintf(1, 'clip%d\n', n);
    thisDat = double(mmf.Data.x(:, (1:nClipSamps)+sampStarts(n)));
    tmp = thisDat(c_idx,:);
    raw_lfp(n,:,:) = tmp;
    tmp = bsxfun(@minus, tmp, mean(tmp,2));
    med_lfp(n,:,:) = tmp;
%     tmp = tmp-repmat(median(tmp,1),size(tmp,1),1);
    lfp(n,:,:) = tmp;
      
    for c = 1:n_ch
        %         filt_lfp(n,c,:) = filtfilt(b,a,thisDat(c,:));
        filt_lfp(n,c,:) = filtfilt(b,a,tmp(c,:));
        whit_lfp(n,c,:) = whitenlfp(squeeze(filt_lfp(n,c,:)),lfpFs*2000,2);
        %         keyboard
    end
    
end

%%

clf,
ax = [];

ax = [ax,subplot(1,3,1)];
imagesc(squeeze(mean(raw_lfp,1)))
set(gca,'clim',[-5 5])
colorbar
ax = [ax,subplot(1,3,2)];
imagesc(squeeze(mean(med_lfp,1)))
set(gca,'clim',[-50 50])
colorbar
ax = [ax,subplot(1,3,3)];
imagesc(squeeze(mean(lfp,1)))
set(gca,'clim',[-50 50])
colorbar
% % set(gca,'clim',[-2 2])
% ax = [ax,subplot(1,5,4)];
% imagesc(squeeze(mean(filt_lfp,1)))
% set(gca,'clim',[-10 10])
% colorbar
% ax = [ax,subplot(1,5,5)];
% imagesc(squeeze(mean(whit_lfp,1)))
% set(gca,'clim',[-.5 .5])
% colorbar
linkaxes(ax,'x','y')

% %%
% clf,
% f = squeeze(mean(raw_lfp,1));
% % tcOffsetPlot(f(c_idx,:)',10)
% tcOffsetPlot(f',1);
% 
% %%
% 
% figure,
% 
% f = squeeze(mean(med_lfp,1));
% tcOffsetPlot(f',1);
% 
% 
% %%
% figure,
% 
% 
% f = squeeze(mean(lfp,1));
% tcOffsetPlot(f',0.5);
% 
% % CSD(f(50:100,:)',lfpFs,1e3);
% 
%% downsample lfp







%%
ii = 5;

lfpFs = 2500;  % neuropixels phase3a
nChansInFile = 385;  % neuropixels phase3a, from spikeGLX

tmp = list_files(sprintf('%s%s%s',parent_folder,ext_KILO_folder,nameFolds{ii}),'*.channelmap');

% dlmread(tmp{1},',',1,0)
fileID= fopen(tmp{1});

C = textscan(fileID,'%d %d %d %d %d %d','delimiter',',','headerlines',1);

fclose(fileID);
c_idx = find(C{3}==0&C{5}<3300);
ch_name= C{1}(c_idx);
ch_connect = C{3}(c_idx);
ch_x = C{4}(c_idx);
ch_y = C{5}(c_idx);





%%

myKsDir = sprintf('%s%s%s',parent_folder,ext_KILO_folder,nameFolds{ii});
gwfparams.dataDir = myKsDir;    % KiloSort/Phy output folder

tmp = list_files(sprintf('%s%s%s',parent_folder,raw_KILO_folder,nameFolds{ii}),'*.lf.bin');
lfpFilename = tmp{1};
d= dir(lfpFilename);
oldSamps = d.bytes/2/nChansInFile;


%%
% applyCARtoDat(lfpFilename, nChansInFile, myKsDir);
applyCARandfilttoDat(lfpFilename, nChansInFile,c_idx, myKsDir);

%%

tmp = list_files(sprintf('%s%s%s',parent_folder,ext_KILO_folder,nameFolds{ii}),'*.lf_CAR.bin');
lfpFilename = tmp{1};

%%
nChansInFile = length(c_idx);
clipDur = 0.35;

d= dir(lfpFilename);
nSamps = d.bytes/2/nChansInFile;
%%
tmp = list_files(sprintf('%s%s%s',parent_folder,events_folder,nameFolds{ii}),'*.mat');
tm = load(tmp{1});

a = tm.allTriggersS{1}{1};
all_events = a(diff(a)>.015);
eventTimes = all_events(2:4:end);

ts = linspace(1,nSamps/lfpFs,nSamps);
for it = 1:length(eventTimes)
    sampStarts(it) = find(ts>=eventTimes(it)+1,[1],'first');
end
nClips = length(eventTimes);
%%

% sampStarts = round(linspace(lfpFs*10, nSamps, nClips+1));
nClipSamps = round(lfpFs*clipDur);

[b,a]=butter(3,[300]*2/(lfpFs),'low');

mmf = memmapfile(lfpFilename, 'Format', {'int16', [nChansInFile nSamps], 'x'});

n_ch = length(ch_name); 

raw_lfp = zeros(nClips,n_ch,nClipSamps);
lfp = zeros(nClips,n_ch,nClipSamps);
filt_lfp = zeros(nClips,n_ch,nClipSamps);
med_lfp = zeros(nClips,n_ch,nClipSamps);
whit_lfp = zeros(nClips,n_ch,nClipSamps); 

for n = 1:nClips
    fprintf(1, 'clip%d\n', n);
    thisDat = double(mmf.Data.x(:, (1:nClipSamps)+sampStarts(n)));
    tmp = thisDat;
    raw_lfp(n,:,:) = tmp;
    tmp = bsxfun(@minus, tmp, mean(tmp,2));
    med_lfp(n,:,:) = tmp;
    tmp = tmp-repmat(median(tmp,1),size(tmp,1),1);
    lfp(n,:,:) = tmp;
    
%     for c = 1:n_ch
% %         filt_lfp(n,c,:) = filtfilt(b,a,thisDat(c,:));
%         filt_lfp(n,c,:) = filtfilt(b,a,tmp(c,:));
%         whit_lfp(n,c,:) = whitenlfp(squeeze(filt_lfp(n,c,:)),lfpFs*2000,2);
% %         keyboard
%     end
end
%%

% % sampStarts = round(linspace(lfpFs*10, nSamps, nClips+1));
% nClipSamps = round(lfpFs*clipDur);
% 
% [b,a]=butter(3,[300]*2/(lfpFs),'low');
% 
% mmf = memmapfile(lfpFilename, 'Format', {'int16', [nChansInFile nSamps], 'x'});
% 
% n_ch = length(ch_name); 
% 
% raw_lfp = zeros(nClips,n_ch,nClipSamps);
% lfp = zeros(nClips,n_ch,nClipSamps);
% filt_lfp = zeros(nClips,n_ch,nClipSamps);
% med_lfp = zeros(nClips,n_ch,nClipSamps);
% whit_lfp = zeros(nClips,n_ch,nClipSamps); 
% 
% for n = 1:nClips
%     fprintf(1, 'clip%d\n', n);
%     thisDat = double(mmf.Data.x(:, (1:nClipSamps)+sampStarts(n)));
%     tmp = thisDat;
%     raw_lfp(n,:,:) = tmp;
%     tmp = bsxfun(@minus, tmp, mean(tmp,2));
%     med_lfp(n,:,:) = tmp;
%     tmp = tmp-repmat(median(tmp,1),size(tmp,1),1);
%     lfp(n,:,:) = tmp;
%     
%     for c = 1:n_ch
% %         filt_lfp(n,c,:) = filtfilt(b,a,thisDat(c,:));
%         filt_lfp(n,c,:) = filtfilt(b,a,tmp(c,:));
%         whit_lfp(n,c,:) = whitenlfp(squeeze(filt_lfp(n,c,:)),lfpFs*2000,2);
% %         keyboard
%     end
% end



%%
% % sampStarts = round(linspace(lfpFs*10, nSamps, nClips+1));
% nClipSamps = round(lfpFs*clipDur);
% 
% [b,a]=butter(3,[300]*2/(lfpFs),'low');
% 
% mmf = memmapfile(lfpFilename, 'Format', {'int16', [nChansInFile nSamps], 'x'});
% 
% n_ch = length(ch_name); 
% 
% raw_lfp = zeros(nClips,n_ch,nClipSamps);
% lfp = zeros(nClips,n_ch,nClipSamps);
% filt_lfp = zeros(nClips,n_ch,nClipSamps);
% med_lfp = zeros(nClips,n_ch,nClipSamps);
% whit_lfp = zeros(nClips,n_ch,nClipSamps); 
% 
% for n = 1:nClips
%     fprintf(1, 'clip%d\n', n);
%     thisDat = double(mmf.Data.x(:, (1:nClipSamps)+sampStarts(n)));
%     tmp = thisDat(c_idx,:);
%     raw_lfp(n,:,:) = tmp;
%     tmp = bsxfun(@minus, tmp, mean(tmp,2));
%     med_lfp(n,:,:) = tmp;
%     tmp = tmp-repmat(median(tmp,1),size(tmp,1),1);
%     lfp(n,:,:) = tmp;
%     
%     for c = 1:n_ch
% %         filt_lfp(n,c,:) = filtfilt(b,a,thisDat(c,:));
%         filt_lfp(n,c,:) = filtfilt(b,a,tmp(c,:));
%         whit_lfp(n,c,:) = whitenlfp(squeeze(filt_lfp(n,c,:)),lfpFs*2000,2);
% %         keyboard
%     end
% end

%%

clf,
ax = [];

ax = [ax,subplot(1,5,1)];
imagesc(squeeze(mean(raw_lfp,1)))
set(gca,'clim',[-5 5])
colorbar
ax = [ax,subplot(1,5,2)];
imagesc(squeeze(mean(med_lfp,1)))
set(gca,'clim',[-5 5])
colorbar
ax = [ax,subplot(1,5,3)];
imagesc(squeeze(mean(lfp,1)))
set(gca,'clim',[-5 5])
colorbar
% % set(gca,'clim',[-2 2])
% ax = [ax,subplot(1,5,4)];
% imagesc(squeeze(mean(filt_lfp,1)))
% set(gca,'clim',[-10 10])
% colorbar
% ax = [ax,subplot(1,5,5)];
% imagesc(squeeze(mean(whit_lfp,1)))
% set(gca,'clim',[-.5 .5])
% colorbar
linkaxes(ax,'x','y')





%%
clf,
asd = squeeze(mean(filt_lfp,1));
subplot(2,1,1)
imagesc(asd)
bsd = zeros(size(asd));
for ii = 1:nChansInFile
    bsd(ii,:) = whitenlfp(asd(ii,:),100);
end
subplot(2,1,2)
imagesc(bsd)
set(gca,'clim',[-.5 .5])
colorbar
%%
tmp = squeeze(mean(filt_lfp,1));
fname = 'test.mat';
save(fname,'f','-v7.3')



%%
clf,
ax = [];

ax = [ax,subplot(5,1,1)];
imagesc(squeeze(mean(raw_lfp,1)))
set(gca,'clim',[-10 10])
colorbar
% set(gca,'clim',[-2 2])
ax = [ax, subplot(5,1,2)];
imagesc(squeeze(mean(med_lfp,1)))
set(gca,'clim',[-10 10])
colorbar
ax = [ax,subplot(5,1,3)];
imagesc(squeeze(mean(lfp,1)))
set(gca,'clim',[-10 10])
colorbar
ax = [ax,subplot(5,1,4)];
imagesc(squeeze(mean(filt_lfp,1)))
set(gca,'clim',[-10 10])
colorbar
ax = [ax,subplot(5,1,5)];
imagesc(squeeze(mean(whit_lfp,1)))

set(gca,'clim',[-.5 .5])
colorbar
linkaxes(ax,'x','y')



%%

%%
plot(tmp(1:50,:)')


%%


or = linspace(1,max(double(ch_y)),5000);
y = linspace(1,max(double(ch_y)),n_ch);

x = 1:1:lfpFs*clipDur;
xv = 1:1:lfpFs*clipDur;

tmp = squeeze(mean(filt_lfp,1));

f = interp2(x',y,tmp,xv',or,'makima');
clf,
imagesc(f)

colorbar

%%


CSD(f',lfpFs,1e3);


%%




%%




%filter and downsample all channels
[out,meta] = load_ni_whisper_file(lfpFilename,1,false);
[b,a]=butter(3,[300]*2/(lfpFs),'low');
dat = zeros(length(chanMap),length(out.Data.data));
% dat = zeros(length(chanMap),20000);
sample_size = 1:20000;
dat = double(out.Data.data(chanMap+1,sample_size));
dat = dat - repmat(median(dat,1),size(lfp,1),1);
disp('Loaded data and subtracted median.')

%%
for i = 1:length(chanMap)
    lfp(i,:)=filtfilt(b,a,dat(i,:));
end

%%


%%

fname = 'test.mat';
save(fname,'sample','-v7.3')

%%
allPowerEst = allPowerEst(:,chanMap+1)'; % now nChans x nFreq

% plot LFP power
dispRange = [0 100]; % Hz
marginalChans = [10:50:nC];
freqBands = {[1.5 4], [4 10], [10 30], [30 80], [80 200]};

plotLFPpower(F, allPowerEst, dispRange, marginalChans, freqBands);
%%


%%

apD = dir(fullfile(myKsDir, '*lf*.bin')); % AP band file from spikeGLX specifically
gwfparams.fileName = apD(1).name;         % .dat file containing the raw 
gwfparams.dataType = 'int16';            % Data type of .dat file (this should be BP filtered)
gwfparams.nCh = 385;                      % Number of channels that were streamed to disk in .dat file
gwfparams.wfWin = [-40 41];              % Number of samples before and after spiketime to include in waveform
gwfparams.nWf = 2000;                    % Number of waveforms per unit to pull out
gwfparams.spikeTimes = ceil(sp.st(sp.clu==155)*30000); % Vector of cluster spike times (in samples) same length as .spikeClusters
gwfparams.spikeClusters = sp.clu(sp.clu==155);

wf = getWaveForms(gwfparams);

figure; 
imagesc(squeeze(wf.waveFormsMean))
set(gca, 'YDir', 'normal'); xlabel('time (samples)'); ylabel('channel number'); 
colormap(colormap_BlueWhiteRed); caxis([-1 1]*max(abs(caxis()))/2); box off;


%%

depthBins = 0:40:3840;
window = [-2 5]; % look at spike times from 0.3 sec before each event to 1 sec after
depthBinSize =80; % in units of the channel coordinates, in this case �m
timeBinSize = 0.02; % seconds
bslWin = [-2 -0.5]; % window in which to compute "baseline" rates for normalization
psthType = 'norm'; % show the normalized version
eventName = 'stimulus onset'; % for figure labeling
printfigure = 1;

for ii =7:length(nameFolds)
    
    
    myKsDir = sprintf('%s%s%s',parent_folder,ext_KILO_folder,nameFolds{ii});
    sp = loadKSdir(myKsDir);
    [spikeTimes, spikeAmps, spikeDepths, spikeSites] = ksDriftmap(myKsDir,'true');
    ampBins = 0:30:min(max(spikeAmps),800);
    recordingDur = sp.st(end);
        
%     [pdfs, cdfs] = computeWFampsOverDepth(spikeAmps, spikeDepths, ampBins, depthBins, recordingDur);
%     plotWFampCDFs(pdfs, cdfs, ampBins, depthBins);

    tmp = list_files(sprintf('%s%s%s',parent_folder,events_folder,nameFolds{ii}),'*.mat');
    tm = load(tmp{1});
    
    a = tm.allTriggersS{1}{1};
    all_events = a(diff(a)>.015);
    eventTimes = all_events(2:4:end);
       

    [timeBins, depthBins, allP, normVals] = psthByDepth(spikeTimes, spikeDepths, ...
        depthBinSize, timeBinSize, eventTimes, window, bslWin);
    
    plotPSTHbyDepth(timeBins, depthBins, allP, eventName, psthType);
    title(sprintf('%s',nameFolds{ii}),'interpreter','none')
    set(gcf,'paperunits','centimeters','papersize',[4.2*3,4.2*3],...
        'paperposition',[0,0,4.2*3,4.2*3])
    
    set(findall(gcf,'-property','FontSize'),'FontSize',8)
    
    printfolder = 'E:\local\users\cagatay\kilosort-aon-pir\figures\spike_depth_profile\';
    if printfigure
        print(gcf,'-dpdf',sprintf('%s%s_selec.pdf',printfolder,nameFolds{ii}))
    end
    clf
    
end



%%



%%

myKsDir = 'E:\local\users\cagatay\kilosort-aon-pir\ext_KILO_data\181122_MB052';
sp = loadKSdir(myKsDir);
%%
[spikeTimes, spikeAmps, spikeDepths, spikeSites] = ksDriftmap(myKsDir,'true');
figure; plotDriftmap(spikeTimes, spikeAmps, spikeDepths);

%%

depthBins = 0:40:3840;
ampBins = 0:30:min(max(spikeAmps),800);
recordingDur = sp.st(end);

[pdfs, cdfs] = computeWFampsOverDepth(spikeAmps, spikeDepths, ampBins, depthBins, recordingDur);
plotWFampCDFs(pdfs, cdfs, ampBins, depthBins);

%%

proc_dir = 'E:\local\users\cagatay\kilosort-aon-pir\processed_data\181122_MB052\181122_MB052_behaviourdata_lr.mat';
a = load(proc_dir);
eventTimes = a.onsets;

%%

[timeBins, depthBins, allP, normVals] = psthByDepth(spikeTimes, spikeDepths, ...
    depthBinSize, timeBinSize, eventTimes, window, bslWin);

figure;
plotPSTHbyDepth(timeBins, depthBins, allP, eventName, psthType);