function [data] = get_data(foldername,dataset,varnames,experiment_idx,print_variables)
% GET_DATA Gathers data from pre-processed.
% [data] = get_lgnmov_data(foldername,experiment,varnames,print_variables)
%   - foldername is the name of the folder where the data is.
%   - dataset is 'lr' to select the dataset
%   - varnames the name of the variables to be loaded (cell array).
%   - experiment_idx is the index of the experiments, pass [] for all.
%   - print_variables is a flag that prints the variables to be loaded.
%
% Example:
%   Load the 'stim_tf','f1','vevoked_idx' fro the tf dataset from the first
%   3 experiments and plot the name off all variables available.
%   data = get_lgnmov_data('.','tf',{'stim_tf','f1','vevoked_idx'},[1:3],1);
%
%
processed_data = 'processed_data';


spikedata_files = ['*_spikedata_',dataset,'.mat'];
behaviourdata_files = ['*_behaviourdata_',dataset,'.mat'];
odortrialdata_files = ['*_odortrialdata_',dataset,'.mat'];
snifftrialdata_files = ['*_sniffdata_',dataset,'.mat'];

repvisual = @(x)strrep(x,spikedata_files(2:end),odortrialdata_files(2:end));
repbehav = @(x)strrep(x,spikedata_files(2:end),behaviourdata_files(2:end));

filelist = {spikedata_files,odortrialdata_files,...
    behaviourdata_files,snifftrialdata_files};


repname = @(x,y)strrep(x,spikedata_files(2:end),y(2:end));

% This is a non efficient way of loading data...
data = [];
% Load variable list
vars = {};
for i = 1:length(filelist)
    spkfiles = list_files([foldername,'/',processed_data],filelist{i});
    try
        tmp = repname(spkfiles{1},filelist{i});
        %      if i == length(filelist)
        %             keyboard
        %         end
        if exist(tmp,'file')
            tmp = whos('-file',tmp);
            vars{i} = {tmp.name};
            
        end
    catch
        fprintf(1,'Skipping %s',filelist{i})
    end
end
spkfiles = list_files([foldername,'/',processed_data],filelist{1});

% Print variable names
if ~exist('print_variables','var')
    print_variables = false;
end
if print_variables
    for ii = 1:length(filelist)
        fprintf(1,['\nVariables in ',filelist{ii},' files:\n\t'])
        try
            fprintf(1,strjoin(vars{ii},'\n\t'))
        catch
            keyboard
        end
    end
    fprintf(1,'\n')
end
% Check which variable is in which file
if ~exist('varnames','var')
    varnames = {};
end
varnames = unique([varnames,'exp_name','session_name']);
loclist = [];
for var = varnames
    tmp = find(cellfun(@(x)sum(strcmp(x,var{1})),vars,'uniformoutput',1));
    if isempty(tmp) 
        warning(sprintf('Variable %s does not exist in files.',var{1}))
        tmp = 0;
    end
    loclist = [loclist,tmp(1)];
end
%
% Allocate data (not really needed... or done properly)
clear data
if ~exist('experiment_idx','var')
    experiment_idx = [];
end
if isempty(experiment_idx)
    experiment_idx = 1:length(spkfiles);
end
% Fill data structure
for i = experiment_idx(:)'
    for f = varnames;data(i).(f{1}) = {};end
end

for i = experiment_idx(:)'
    for j = 1:length(filelist)
        if sum(loclist == j)
            clear tt
            tmp =repname(spkfiles{i},filelist{j});
            if exist(tmp,'file')
                tt = load(tmp,varnames{loclist == j});
                for f = fields(tt)'
                    data(i).(f{1}) = tt.(f{1});
                end
            end
        end
    end
end

