function [modind,rsum] = modulation_index(baseline,response,pval,method)
% MODULATION_INDEX Compute the modulation index
% [modind,rsum] = modulation_index(baseline,response)
% Compute the modulation index to the scatter contraint to 0 start
if ~exist('pval','var')
    pval = 0.1;
end
if ~exist('method','var')
    method = 'bootstrap';
    method = 'rsum';
    method = 'rsum_per_trial';
end

if ~iscell(baseline)
    % Computes ranksum significance
    modind  = comp_modulation(baseline,response);
    [rsum,H] = ranksum(baseline,response,pval);
else
    if strcmp(method,'bootstrap')
        % Bootstraps for significance
        
        nsamples = 100;
        nbootstrap = 1000;
        % Remove trials with no result.
        tmpidx = cellfun(@(x,y)length(x)>1 & length(y)>1,baseline,response);
        response = response(tmpidx);
        baseline = baseline(tmpidx);
        if length(response)<2
            modind = NaN;
            rsum = NaN;
            return;
        end
        
        % Resample dataset
        B = cellfun(@(x)datasample(x,nsamples,'Replace',true),baseline,'uniformoutput',0);
        R = cellfun(@(x)datasample(x,nsamples,'Replace',true),response,'uniformoutput',0);
        allresp = cellfun(@(x,y)[x,y],baseline,response,'uniformoutput',0);
        B = cell2mat(B);
        R = cell2mat(R);
        modind  = comp_modulation(B,R);
        % Bootstrap for significance.
        bootres = nan(nbootstrap,1);
        for i = 1:nbootstrap
            A0 = cellfun(@(x)datasample(x,nsamples,'Replace',true),allresp,'uniformoutput',0);
            A1 = cellfun(@(x)datasample(x,nsamples,'Replace',true),allresp,'uniformoutput',0);
            A0 = cell2mat(A0);
            A1 = cell2mat(A1);
            bootres(i) = comp_modulation(A0,A1);
                   clf
                   plot([0,55],[0,55],'k--')
                   hold all
                   plot(B,R,'ro','markerfacecolor','r','markersize',5)
            
            
                   plot([0,55],polyval([modind+1,0],[0,55]),'r-')
                   plot(A0,A1,'ko','markerfacecolor','k','markersize',5)
                   plot([0,55],polyval([bootres(i)+1,0],[0,55]),'b-','linewidth',2)
                   axis([0,55,0,55])
            
            %        pause
        end
        %%
%         keyboard

          if modind>=mean(bootres)
            rsum = sum(bootres>modind)/nbootstrap;
        else
            rsum = sum(bootres<modind)/nbootstrap;
        end
    elseif strcmp(method,'rsum')
        %
        %             plotRastergram(baseline)
        %             plotRastergram(response,'color','r')
        %
        baseline = cellfun(@abs,baseline,'uniformoutput',0);
        response = cellfun(@abs,response,'uniformoutput',0);
        nsamples = 1000;            % Remove trials with no result.
        tmpidx = cellfun(@(x,y)length(x)>1 & length(y)>1,baseline,response);
        response = response(tmpidx);
        baseline = baseline(tmpidx);
        if length(response)<2
            modind = NaN;
            rsum = NaN;
            return;
        end
        
        % Resample dataset
        B = cellfun(@(x)datasample(x,nsamples,'Replace',true),baseline,'uniformoutput',0);
        R = cellfun(@(x)datasample(x,nsamples,'Replace',true),response,'uniformoutput',0);
        B = cell2mat(B);
        R = cell2mat(R);
        %             close all
        %
        
        modind  = comp_modulation(B,R)
        [rsum,H] = ranksum(B,R,pval)
        
        return
        %%
             
        close all
        hold all
        tt = max(max([B(:),R(:)]))
        edges = linspace(0,tt,30);
        bins = hist3([B(:),R(:)],{edges,edges});
        colormap(1-gray)
        imagesc(edges,edges,bins',[0,10])
        plot(B,R,'ro')
        axis tight
        
        plot([0,tt],polyval([modind+1,0],[0,tt]),'k')
        axis([0,tt,0,tt])
        plot([0,tt],[0,tt],'b--')
        axis square
        pause
        %%
        %
        %
        %          clf
        %     hold all
        %     hist(bootres,30)
        %     plot(modind+[0,0],ylim,'r--')
        %     keyboard
    elseif strcmp(method,'rsum_per_trial')
        
        baseline = cellfun(@abs,baseline,'uniformoutput',0);
        response = cellfun(@abs,response,'uniformoutput',0);
        tmpidx = cellfun(@(x,y)length(x)>1 & length(y)>1,baseline,response);
        response = response(tmpidx);
        baseline = baseline(tmpidx);
        
        if length(response)<2 || length(baseline)<2 
            modind = NaN;
            rsum = NaN;
            return;
        end
        
        mbase = cellfun(@nanmean,baseline);
        mresp = cellfun(@nanmean,response);
        modind  = comp_modulation(mbase,mresp);
        % Use the minimum number of trials, compare the if the response
        % could of come from random sampling of the baseline 
        nsamples = cellfun(@(x,y)min([length(x),length(y)]),baseline,response,'uniformoutput',0);
        
        B = cellfun(@(x,y)datasample(x,y,'Replace',false),baseline,nsamples,'uniformoutput',0);
        R = cellfun(@(x,y)datasample(x,y,'Replace',false),response,nsamples,'uniformoutput',0);
        rsum = sum(cellfun(@(x,y)ranksum(x,y,pval),B,R)<pval);
        return
        clf;hold all
        plot(mbase)
        plot(mresp)
        pause
        
    end
end
return;


function res = comp_modulation(baseline,response)

constraint = [1,0];
oFunc = @(p) sum((response - polyval(p(:).*constraint(:),baseline)).^2);
p0 = [1,0]';
tempFit = fminsearch(oFunc,p0);
res = tempFit(1)-1; % wrt to the the unity line