clear all
close all
clc

cd ('E:\local\users\cagatay\histology_conf')

%%
animal_name = 'CA014';
%%
allen_folder = 'E:\local\users\cagatay\histology_conf\allen_files\';

tv = readNPY(sprintf('%s%s',allen_folder,'template_volume_10um.npy')); % grey-scale "background signal intensity"
av = readNPY(sprintf('%s%s',allen_folder,'annotation_volume_10um_by_index.npy')); % the number at each pixel labels the area, see note below
st = loadStructureTree(sprintf('%s%s',allen_folder,'structure_tree_safe_2017.csv')); % a table of what all the labels mean

file_save_location = sprintf('%s%s%s',pwd,'\proc_files\',animal_name); % where will the probe locations be saved
probe_name = 'test'; % name probe to avoid overwriting

f = allenAtlasBrowser(tv, av, st, file_save_location, probe_name);