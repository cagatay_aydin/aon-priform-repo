clear all
close all
clc


% foldername = 'E:\local\users\cagatay\KF\sniff_data\190407_CA014';
foldername = 'E:\local\users\cagatay\KF\sniff_data\190409_CA016';
cd(foldername)
tmp_list = list_files(foldername);


%%

pos_rect = [];
inh_frames = {};
inh_times = {};
synt = [];
tt = [];

for iv = 1:length(tmp_list)
fprintf('%d',iv);
tmp_dat = load(tmp_list{iv});
tmp_vid = im2double(tmp_dat.frames);
nframes = size(tmp_vid,3);


%% compute variance
tmp_rs = permute(tmp_vid,[3 1 2]);
var_data = squeeze(var(tmp_rs));


%% selection of a region
if iv ==1
imagesc(var_data);
h_rect = imrect();
% Rectangle position is given as [xmin, ymin, width, height]
pos_rect = h_rect.getPosition();
% Round off so the coordinates can be used as indices
pos_rect = round(pos_rect);
end

%%

% take the mean of the square
for ii = 1:nframes
    tmp = tmp_vid(pos_rect(2) + (0:pos_rect(4)), pos_rect(1) + (0:pos_rect(3)),ii);
   an_dat(ii) =  mean(tmp(:));
end

% clf
% opol = 8;
% % td_andat = detrend(an_dat);
% [p,s,mu] = polyfit(1:nframes,an_dat,opol);
% f_y = polyval(p,1:nframes,[],mu);
% 
% baseline_removed = an_dat - f_y;

baseline_removed = an_dat(1:660);
% clf,hold on, plot(baseline_removed)
fun=@(x) exp(-0.03*x/10);  %relative thresholding
y=fun(round(std(baseline_removed)));
[inh_times{iv}, inh_frames{iv}]=findpeaks(baseline_removed,'MinPeakProminence',y*std(baseline_removed),'MinPeakDistance',3,'MinPeakWidth',y);
% plot(inha,a,'ro'),hold on
% plot(an_dat)
synt(iv,:) = baseline_removed;


[pathstr, name, ext] = fileparts(tmp_list{iv});
[~, rm]= strtok(name,'_');
tr = datetime(rm,'InputFormat','_HH_mm_ss_SSS');
tt(iv) =  datenum(tr)*24*3600;
% keyboard
end
%%

cam.onsets = [(tt-tt(1))]';
breath.inh_onsets = inh_frames;
breath.cam_resp = synt;
save('variance_extracted_sniff','cam','breath');


%%
clear all
close all
clc

load variance_extracted_sniff.mat

%%

imagesc(breath.cam_resp)
