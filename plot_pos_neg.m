clear all
close all
clc


%
varnames = {'exp_name','ts','session_name',...
    'stim_type',...
    'ts_trials',...
    'f0','trial_type','trial_idx',...
    'chemical_list','left_right','trial_occur',...
    'recording_site','mf0','sf0','novelty','blank','sig','resp_h'};
dataset = 'lr';
foldername = '.';
[data] = get_data(foldername,dataset,varnames);
disp('Data loaded.')


%%

tmp = arrayfun(@(x)(x.recording_site),data,'uniformoutput',0);
rsite = horzcat(tmp{:});

all_f0 = [];
all_sig = [];
all_resp = [];
for expt = 1:length(data)
    dd = data(expt);
    tmp = dd.mf0;
    if ~isempty(tmp)
    all_f0 = [all_f0;tmp(:,1:14)];
    all_sig = [all_sig;nansum(dd.sig,2)];
    all_resp = [all_resp;dd.resp_h];
    end
%     all_f0 = vertcat(data.mf0);
end
% all_f0 = vertcat(data.mf0);
blank = logical(data(1).blank);
novelty = logical(data(1).novelty);


%% with 2 groups

clib = {[146/255,71/255,149/255],[63/255,143/255,56/255]};
sig_idx = [all_sig>=1]';
resp_idx = [all_resp>0]';
% cond = {strcmp(rsite,'aon')&sig_idx&resp_idx,...
%     strcmp(rsite,'piriform')&sig_idx&resp_idx};
cond = {strcmp(rsite,'aon')&resp_idx,...
    strcmp(rsite,'piriform')&resp_idx};



for ii = 1:length(cond)
   idx = cond{ii};
    bl = mean(all_f0(idx,blank),2);
    fl = mean(all_f0(idx,~blank),2);
%    keyboard
   d{ii} = bsxfun(@rdivide,(fl-bl),(fl+bl))
   
end


%%

clf
printfigure = true;
edges = [-1:0.1:1];

ii = 2;
for ii = 1:length(cond)

bins = hist(d{ii},edges);

% bb = bar(edges,bins/sum(bins),1);hold on
% set(bb,'facecolor','none','edgecolor',clib{ii})
ss = cumsum(bins);
ss = ss/max(ss);
plot([0 0],[0 1],'--','color',[.5 .5 .5])
plot(edges,ss,'color',clib{ii},'linewidth',1),hold all


end

[h p] = kstest2(d{1},d{2})


ylabel(sprintf('CDF'),'fontsize',7)
xlabel(sprintf('ON-OFF index'),'fontsize',7)
set(gca,'box','off','tickdir','out',...
    'ticklength',get(gca(1),'ticklength').*5)

axis('square')
set(gcf,'paperunits','centimeters','papersize',[4.2*1,4.2*1],...
    'paperposition',[0,0,4.2*1,4.2*1])

set(findall(gcf,'-property','FontSize'),'FontSize',8)

printfolder = 'E:\local\users\cagatay\aon-priform\figures\manuscript';
if printfigure
    print(gcf,'-dpdf',sprintf('%s\\on_off_idx_cumsum_compare.pdf',printfolder))
end





