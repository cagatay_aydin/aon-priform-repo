
function events = extract_oe_triggers(rawfile,timefile,savefile,fs,nchannels,chidx)
% syncData = function extractSyncChan(lfpFilename)
tic
d = dir(rawfile);
nSamps = d.bytes/2/nchannels;
mmf = memmapfile(rawfile, 'Format', {'int16', [nchannels nSamps], 'x'});
% keyboard
tt = double(loadnpy(timefile));

events = {};



% [path,name,ext] = fileparts(savefile);

for ii= 1:length(chidx)
    syncData = double(mmf.Data.x(chidx(ii),:));
    th = mean(syncData).*2;
    [~, tmp_idx]=findpeaks(syncData,tt,...
        'minpeakprominence',th);
    
    nfile = sprintf('onsets_%d',chidx(ii));
    events.(nfile) = tmp_idx;
    nfile = sprintf('th_%d',chidx(ii));
    events.(nfile) = tt(syncData>th);
    nfile = sprintf('raw_%d',chidx(ii));
    events.(nfile) = syncData;
    nfile = sprintf('time_%d',chidx(ii));
    events.(nfile) = tt;
    
%     cd(path);
%     ffile = sprintf('%s%s',name,ext);
% keyboard
%     s = fullfile(path,sprintf('%s%s',name,'.mat'));
    if isdir(savefile)
        save(savefile,'-struct','events','-append');
    else
        save(savefile,'-struct','events');
    end

end
toc

