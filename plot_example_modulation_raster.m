clear all
close all
clc


%
varnames = {'exp_name','ts','session_name',...
    'stim_type',...
    'ts_trials','mod_ind_nov','mod_ind_fam',...
    'f0','trial_type','trial_idx',...
    'chemical_list','left_right','trial_occur',...
    'recording_site','mf0','sf0','novelty','blank','sig'};
dataset = 'lr';
foldername = '.';
[data] = get_data(foldername,dataset,varnames);
disp('Data loaded.')

%%

tmp = arrayfun(@(x)(x.recording_site),data,'uniformoutput',0);
rsite = horzcat(tmp{:});
blank = logical(data(1).blank);
novelty = logical(data(1).novelty);


%% selected neurons for piriform

printall = false;

fig = figure;
experiments = {'AON003','AON003','AON005'};
cells = [14,17,1];

ax_psth = [];
ax_raster = [];
loc_rasters = [0.65,0.35,0.05];
loc_psth = [0.83,0.53,0.23];
loc_stim = [0.1, 0.33];

bin_size = 0.04;
sp_edges = [0:bin_size:3];
sigma = 2;
x = -2*sigma:2*sigma;
kernel = exp(-x .^ 2 / (2*sigma ^ 2)); kernel = kernel./sum(kernel);
dur = 3; 
stim = [3 10;
    3 9;
    3 10];

is = 1;

for sel = 1:length(experiments)
    dd = data(strcmp({data.exp_name},experiments{sel}));
    ic = cells(sel);
        
    clf
    ax = [];
    ax = [ax,subplot(1,9,[1 6])];
    all_f0 = dd.f0{cells(sel)};
    [b idx] = sort(all_f0);
    
    tmp_cell = dd.ts_trials{cells(sel)}(idx);
    plot_rastergram(tmp_cell,0,1,'line','color',[0 0 0])    
    axis(ax,'tight')
    
    ax = [ax,subplot(1,9,7)];
    plot(all_f0(idx),1:length(idx))
    set(ax(end),'xlim',[0 max(all_f0)])
    
    ax = [ax,subplot(1,9,8)];
    plot(dd.trial_occur(idx),1:length(idx))
    set(ax(end),'xlim',[0 max(dd.trial_occur)])
    
    ax = [ax,subplot(1,9,9)];
    plot(dd.trial_type(idx),1:length(idx))
    set(ax(end),'xlim',[0 max(dd.trial_type)])
    keyboard
    
end

%% selected neurons for piriform

printall = true;

% fig = figure;
% experiments = {'AON003','AON003','AON005'};
% cells = [14,17,1];
experiments = {'180322_MB019','AON003'};%'AON007','AON003'
cells = [8,14]; %,61,
 


ax_psth = [];
ax_raster = [];
loc_rasters = [0.65,0.35,0.05];
loc_psth = [0.83,0.53,0.23];
loc_stim = [0.1, 0.33];

bin_size = 0.05;
sp_edges = [0:bin_size:3];
sigma = 2;
x = -2*sigma:2*sigma;
kernel = exp(-x .^ 2 / (2*sigma ^ 2)); kernel = kernel./sum(kernel);
dur = 3; 

is = 1;
ax = [];
for sel = 1:length(experiments)
    clf
    ax = [];
    dd = data(strcmp({data.exp_name},experiments{sel}));
      
    list_odor_code = unique(dd.trial_type);
    nn= list_odor_code(logical(dd.novelty));
    ff = list_odor_code(logical(~dd.novelty&~dd.blank));
    bb= list_odor_code(logical(dd.blank));
    
%     find the novel trials
    novel_trials = ismember(dd.trial_type,nn);
    familiar_trials = ismember(dd.trial_type,ff);
    blank_trials = ismember(dd.trial_type,bb);
    
    first_trials = ismember(dd.trial_occur,1:3);
    last_trials = ismember(dd.trial_occur,4:6);
    
    sorted_trials = dd.ts_trials{cells(sel)}(dd.trial_idx);
    
    all_nov_occur = {};
    tmp_check = unique(dd.trial_occur(novel_trials));
    mm_nov = nan(length(tmp_check)-1,length(sp_edges));
    for io = 1:length(tmp_check)
        tmp_idx = novel_trials&dd.trial_occur==io;
        tmp = sorted_trials(tmp_idx);
        tmp_vec= nan(length(tmp),length(sp_edges));
        for ii = 1:length(tmp)
            bsp = histc(tmp{ii},sp_edges)/bin_size/dur;
            tmp_vec(ii,:) = conv(bsp,kernel,'same');
        end
        all_nov_occur{io} = tmp_vec;
        if size(tmp_vec,1)==1
        continue
        else
        mm_nov(io,:) = nanmean(tmp_vec);
        end
    end
    
    all_fam_occur = {};
    tmp_check = unique(dd.trial_occur(familiar_trials));
    mm_fam = nan(length(tmp_check)-1,length(sp_edges));
    for io = 1:length(tmp_check)
        tmp_idx = familiar_trials&dd.trial_occur==io;
        tmp = sorted_trials(tmp_idx);
        tmp_vec= nan(length(tmp),length(sp_edges));
        for ii = 1:length(tmp)
            bsp = histc(tmp{ii},sp_edges)/bin_size/dur;
            tmp_vec(ii,:) = conv(bsp,kernel,'same');
        end
        all_fam_occur{io} = tmp_vec;
        if size(tmp_vec,1)==1
        continue
        else
        mm_fam(io,:) = nanmean(tmp_vec);
        end
    end
    
    
    all_blank_occur = {};
    tmp_check = unique(dd.trial_occur(blank_trials));
    mm_blank = nan(length(tmp_check),length(sp_edges));
    for io = 1:length(tmp_check)
        tmp_idx = blank_trials&dd.trial_occur==io;
        tmp = sorted_trials(tmp_idx);
        tmp_vec= nan(length(tmp),length(sp_edges));
        for ii = 1:length(tmp)
            bsp = histc(tmp{ii},sp_edges)/bin_size/dur;
            tmp_vec(ii,:) = conv(bsp,kernel,'same');
        end
        all_blank_occur{io} = tmp_vec;
        if size(tmp_vec,1)==1
        continue
        else
        mm_blank(io,:) = nanmean(tmp_vec);
        end
    end
    
    
    clf,
    ax = [ax,subplot(2,2,1)];
    imagesc(mm_nov),
    colormap(hot)
    colorbar,
    ax = [ax,subplot(2,2,2)];
    imagesc(mm_fam),
    colorbar
    
    mm = max([max(mm_nov),max(mm_fam)]);
    set(ax,'clim',[0 mm])
        
    ax = [ax,subplot(2,2,3)];
    nov_resp = mean(mm_nov(1:3,:));
    nov_ste = std(mm_nov(1:3,:))./sqrt(3);
    famnov_resp = mean(mm_nov(4:6,:));
    famnov_ste = std(mm_nov(4:6))./sqrt(3);
    errorbar_patch(sp_edges,nov_resp,nov_ste,[1 0 0]);
    hold all
    errorbar_patch(sp_edges,famnov_resp,famnov_ste,[0 0 0]);hold all
%     errorbar_patch(sp_edges,nanmean(mm_blank),nanstd(mm_blank)./sqrt(size(mm_blank,1)),...
%         [.5 .5 .5])
%     cnov = winter(size(mm_nov,1));
%     
%     for it = 1:size(mm_nov,1)
%     plot(sp_edges,mm_nov(it,:),'color',cnov(it,:)),hold all
%     end
    
    ax = [ax,subplot(2,2,4)];
    fam_resp = mean(mm_fam(1:3,:));
    fam_ste = std(mm_fam(1:3,:))./sqrt(3);
    famfam_resp = mean(mm_fam(4:6,:));
    famfam_ste = std(mm_fam(4:6,:))./sqrt(3);
    errorbar_patch(sp_edges,fam_resp,fam_ste,[1 0 0]);
    hold all
    errorbar_patch(sp_edges,famfam_resp,famfam_ste,[0 0 0]);
%     errorbar_patch(sp_edges,mean(mm_blank),std(mm_blank)./sqrt(size(mm_blank,1)),...
%         [.5 .5 .5])
    
%     cfam = winter(size(mm_fam,1));
%     for it = 1:size(mm_fam,1)
%     plot(sp_edges,mm_fam(it,:),'color',cfam(it,:)),hold all
%     
    mm = max([max(mm_nov),max(mm_fam)]);
    
    set(ax(end-1:end),'ylim',[0 mm],'box','off','tickdir','out','ticklength',...
        get(ax(end),'ticklength').*5)
    
%     end

if printall
    % set(gcf,'papersize',[figW*2.5,figH*0.3],'paperposition',[0,0,figW*0.2,figH*0.3],'color','none','paperunits','centimeters')
    set(gcf,'papersize',[6.2*2,4.2*2],'paperposition',[0,0,6.2*2,4.2*2],'color','white','paperunits','centimeters')
    figfolder = 'figures/';
    print(gcf,'-dpdf',sprintf('%s/example_modulation%d.pdf',figfolder,sel))
end

    keyboard
end


%%
    
   for ii = 1:length(b) % dummy
    idx_nov = dd.trial_idx(find(dd.trial_type==stim(sel,1)));
    ts_novel = dd.ts_trials{cells(sel)}(idx_nov);
    
    idx_fam = dd.trial_idx(find(dd.trial_type==stim(sel,2)));
    ts_fam = dd.ts_trials{cells(sel)}(idx_fam);
    
    idx_blank = dd.trial_idx(find(dd.trial_type==1));
    ts_blank = dd.ts_trials{cells(sel)}(idx_blank);
    
    ax_psth = [ax_psth, axes('position',[loc_stim(is),loc_psth(sel),0.2,0.1])]; 
        bsp =[];
        tmp_nov = cell2mat(ts_novel);
        bsp_nov = histc(tmp_nov,sp_edges)/length(idx_nov)/bin_size/dur;
        c_nov = conv(bsp_nov,kernel,'same');
        
        tmp_fam = cell2mat(ts_fam);
        bsp_fam = histc(tmp_fam,sp_edges)/length(idx_fam)/bin_size/dur;
        c_fam = conv(bsp_fam,kernel,'same');
        
        tmp_blank = cell2mat(ts_blank);
        bsp_blank = histc(tmp_blank,sp_edges)/length(idx_blank)/bin_size/dur;
        c_blank = conv(bsp_blank,kernel,'same');
        
        
        
        plot(sp_edges,c_fam,'color','k');
        hold all
        plot(sp_edges,c_nov,'color','b');
        plot(sp_edges,c_blank,'color',[.5 .5 .5]);
        mm = max([c_nov ;c_fam])+2;
        ylim([0 mm])
        plot([max(sp_edges) max(sp_edges)],[mm mm-5],'k','linewidth',1)
        axis('tight')
    
    
%     keyboard
    
    
    ax_raster = [ax_raster, axes('position',[loc_stim(is),loc_rasters(sel),0.2,0.1])];
    
%     sorted_trials = ts();
    plot_rastergram(ts_novel,0,1,'line','color',[0 0 0])
    hold all
    plot_rastergram(ts_fam,length(idx_nov),1,'line','color',[0 0 1])
    
    axis('tight')
    
end

plot([3,3-0.5],[-1,-1],'color','k','clipping','off')
text(3-0.25,-1,'500 ms','fontsize',6,'verticalalignment','top','horizontalalignment','center')

set(ax_psth,'visible','off')
set(ax_raster,'visible','off')

if printall
    % set(gcf,'papersize',[figW*2.5,figH*0.3],'paperposition',[0,0,figW*0.2,figH*0.3],'color','none','paperunits','centimeters')
    set(fig,'papersize',[5,4],'paperposition',[0,0,5,4],'color','white','paperunits','centimeters')
    figfolder = 'figures/';
    print(fig,'-dpdf',sprintf('%s/long_new_figure_example_modulations_piriform.pdf',figfolder))
end


%% selected neurons for aon

printall = true;

fig = figure;

experiments = {'AON013','AON010','AON015'};
cells = [6,1,29];


ax_psth = [];
ax_raster = [];
loc_rasters = [0.65,0.35,0.05];
loc_psth = [0.83,0.53,0.23];
loc_stim = [0.1, 0.33];

bin_size = 0.015;
sp_edges = [0:bin_size:4];
sigma = 3;
x = -2*sigma:3*sigma;
kernel = exp(-x .^ 2 / (2*sigma ^ 2)); kernel = kernel./sum(kernel);
dur = 3; 
stim = [6 13;
    13 11;    
    3 10];


is = 1;

for sel = 1:length(experiments)
    dd = data(strcmp({data.exp_name},experiments{sel}));
    ic = cells(sel);
    
    idx_nov = dd.trial_idx(find(dd.trial_type==stim(sel,1)));
    ts_novel = dd.ts_trials{cells(sel)}(idx_nov);
    
    idx_fam = dd.trial_idx(find(dd.trial_type==stim(sel,2)));
    ts_fam = dd.ts_trials{cells(sel)}(idx_fam);
    
     idx_blank = dd.trial_idx(find(dd.trial_type==1));
    ts_blank = dd.ts_trials{cells(sel)}(idx_blank);
    
    ax_psth = [ax_psth, axes('position',[loc_stim(is),loc_psth(sel),0.2,0.08])]; 
        bsp =[];
        tmp_nov = cell2mat(ts_novel);
        bsp_nov = histc(tmp_nov,sp_edges)/length(idx_nov)/bin_size/dur;
        c_nov = conv(bsp_nov,kernel,'same');
        
        tmp_fam = cell2mat(ts_fam);
        bsp_fam = histc(tmp_fam,sp_edges)/length(idx_fam)/bin_size/dur;
        c_fam = conv(bsp_fam,kernel,'same');
        
        tmp_blank = cell2mat(ts_blank);
        bsp_blank = histc(tmp_blank,sp_edges)/length(idx_blank)/bin_size/dur;
        c_blank = conv(bsp_blank,kernel,'same');
        
        
        plot(sp_edges,c_fam,'color','k');
        hold all
        plot(sp_edges,c_nov,'color','b');
        plot(sp_edges,c_blank,'color',[0.5 0.5 0.5]);
        
        ylim([0 max([c_nov ;c_fam])+2])
        plot([max(sp_edges) max(sp_edges)],[max(ylim) max(ylim)-10],'k','linewidth',1)
%         axis('tight')
    
    
%     keyboard
    
    
    ax_raster = [ax_raster, axes('position',[loc_stim(is),loc_rasters(sel),0.2,0.18])];
    
%     sorted_trials = ts();
    plot_rastergram(ts_novel,0,1,'line','color',[0 0 0])
    hold all
    plot_rastergram(ts_fam,length(idx_nov),1,'line','color',[0 0 1])
    
    axis('tight')
    plot([0 3],[length(idx_nov) length(idx_nov)],'color',[0.5 0.5 0.5],'linewidth',1)
    plot([min(xlim) min(xlim)],[0 length(idx_nov)],'color',[0 0 0],'linewidth',1)
    plot([min(xlim) min(xlim)],[length(idx_nov) length(idx_fam)+length(idx_nov)],'color',[0 0 1],'linewidth',1)
    
    if sel==length(experiments)
    plot([2,2-0.5],[-1,-1],'color','k','clipping','off')
    text(2-0.25,-1,'500 ms','fontsize',6,'verticalalignment','top','horizontalalignment','center')
    end
    
    x = [1:12];
    y = dd.mf0(ic,~blank);
    
    ax_hf0(sel) = axes('position',[0.45,loc_rasters(sel)+.05,0.45,0.15]);hold all
    
    errorbar(x,y,dd.sf0(ic,~blank),'o','capsize',0,...
        'markersize',2,'markerfacecolor','k','markeredgecolor','none','color','k'),hold on
    x_i = 1:0.1:12;
    s = pchip(x,y,x_i);
    plot(x_i,s,'k')
    
    mm = max([dd.mf0(ic,~blank)+ dd.sf0(ic,~blank)]);
    plot([1:12],ones(1,12).*nanmean(dd.mf0(ic,blank)),'--','color',[0.5,0.5,0.5])
    set(ax_hf0(sel),'xlim',[0.5 12.5],'ylim',[0 mm])
%     axis('tight')
    
end

% plot([2,2-0.5],[-1,-1],'color','k','clipping','off')
% text(2-0.25,-1,'500 ms','fontsize',6,'verticalalignment','top','horizontalalignment','center')

set(ax_hf0,'tickdir','out','ticklength',get(gca,'ticklength').*5,'xtick',[1:12],'xticklabel',[])
set(ax_hf0(end),'xtick',[1:12],'xticklabel',dd.chemical_list(~blank),'xticklabelrotation',45)
    
set(ax_psth,'visible','off')
set(ax_raster,'visible','off')

if printall
    % set(gcf,'papersize',[figW*2.5,figH*0.3],'paperposition',[0,0,figW*0.2,figH*0.3],'color','none','paperunits','centimeters')
    set(fig,'papersize',[5,4],'paperposition',[0,0,5,4],'color','white','paperunits','centimeters')
    figfolder = 'figures/';
    print(fig,'-dpdf',sprintf('%s/new_figure_example_modulations_aon.pdf',figfolder))
end




%% selected neurons for piriform

printall = true;

fig = figure;

experiments = {'AON003','AON003','AON005'};
cells = [14,17,1];


ax_psth = [];
ax_raster = [];
loc_rasters = [0.65,0.35,0.05];
loc_psth = [0.83,0.53,0.23];
loc_stim = [0.1, 0.33];

bin_size = 0.015;
sp_edges = [0:bin_size:4];
sigma = 3;
x = -2*sigma:3*sigma;
kernel = exp(-x .^ 2 / (2*sigma ^ 2)); kernel = kernel./sum(kernel);
dur = 3; 
stim = [3 10;
    3 9;
    3 10];


is = 1;

for sel = 1:length(experiments)
    dd = data(strcmp({data.exp_name},experiments{sel}));
    ic = cells(sel);
    
    idx_nov = dd.trial_idx(find(dd.trial_type==stim(sel,1)));
    ts_novel = dd.ts_trials{cells(sel)}(idx_nov);
    
    idx_fam = dd.trial_idx(find(dd.trial_type==stim(sel,2)));
    ts_fam = dd.ts_trials{cells(sel)}(idx_fam);
    
     idx_blank = dd.trial_idx(find(dd.trial_type==1));
    ts_blank = dd.ts_trials{cells(sel)}(idx_blank);
    
    ax_psth = [ax_psth, axes('position',[loc_stim(is),loc_psth(sel),0.2,0.08])]; 
        bsp =[];
        tmp_nov = cell2mat(ts_novel);
        bsp_nov = histc(tmp_nov,sp_edges)/length(idx_nov)/bin_size/dur;
        c_nov = conv(bsp_nov,kernel,'same');
        
        tmp_fam = cell2mat(ts_fam);
        bsp_fam = histc(tmp_fam,sp_edges)/length(idx_fam)/bin_size/dur;
        c_fam = conv(bsp_fam,kernel,'same');
        
        tmp_blank = cell2mat(ts_blank);
        bsp_blank = histc(tmp_blank,sp_edges)/length(idx_blank)/bin_size/dur;
        c_blank = conv(bsp_blank,kernel,'same');
        
        
        plot(sp_edges,c_fam,'color','k');
        hold all
        plot(sp_edges,c_nov,'color','b');
        plot(sp_edges,c_blank,'color',[0.5 0.5 0.5]);
        
        ylim([0 max([c_nov ;c_fam])+2])
        plot([max(sp_edges) max(sp_edges)],[max(ylim) max(ylim)-10],'k','linewidth',1)
%         axis('tight')
    
    
%     keyboard
    
    
    ax_raster = [ax_raster, axes('position',[loc_stim(is),loc_rasters(sel),0.2,0.18])];
    
%     sorted_trials = ts();
    plot_rastergram(ts_novel,0,1,'line','color',[0 0 0])
    hold all
    plot_rastergram(ts_fam,length(idx_nov),1,'line','color',[0 0 1])
    
    axis('tight')
    plot([0 3],[length(idx_nov) length(idx_nov)],'color',[0.5 0.5 0.5],'linewidth',1)
    plot([min(xlim) min(xlim)],[0 length(idx_nov)],'color',[0 0 0],'linewidth',1)
    plot([min(xlim) min(xlim)],[length(idx_nov) length(idx_fam)+length(idx_nov)],'color',[0 0 1],'linewidth',1)
    
    if sel==length(experiments)
    plot([2,2-0.5],[-1,-1],'color','k','clipping','off')
    text(2-0.25,-1,'500 ms','fontsize',6,'verticalalignment','top','horizontalalignment','center')
    end
    
    x = [1:12];
    y = dd.mf0(ic,~blank);
    
    ax_hf0(sel) = axes('position',[0.45,loc_rasters(sel)+.05,0.45,0.15]);hold all
    
    errorbar(x,y,dd.sf0(ic,~blank),'o','capsize',0,...
        'markersize',2,'markerfacecolor','k','markeredgecolor','none','color','k'),hold on
    x_i = 1:0.1:12;
    s = pchip(x,y,x_i);
    plot(x_i,s,'k')
    
    mm = max([dd.mf0(ic,~blank)+ dd.sf0(ic,~blank)]);
    plot([1:12],ones(1,12).*nanmean(dd.mf0(ic,blank)),'--','color',[0.5,0.5,0.5])
    set(ax_hf0(sel),'xlim',[0.5 12.5],'ylim',[0 mm])
%     axis('tight')
    
end


set(ax_hf0,'tickdir','out','ticklength',get(gca,'ticklength').*5,'xtick',[1:12],'xticklabel',[])
set(ax_hf0(end),'xtick',[1:12],'xticklabel',dd.chemical_list(~blank),'xticklabelrotation',45)
    
set(ax_psth,'visible','off')
set(ax_raster,'visible','off')

if printall
    % set(gcf,'papersize',[figW*2.5,figH*0.3],'paperposition',[0,0,figW*0.2,figH*0.3],'color','none','paperunits','centimeters')
    set(fig,'papersize',[5,4],'paperposition',[0,0,5,4],'color','white','paperunits','centimeters')
    figfolder = 'figures/';
    print(fig,'-dpdf',sprintf('%s/new_figure_example_modulations_piriform.pdf',figfolder))
end