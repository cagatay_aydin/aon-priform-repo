function [hab_resp, hab_ste] = compute_habituation(condition,occur_th,trial_type,trial_occur,sf0)
% condition is logical vector for being novel, familiar or blank
% condition = [1 0 1 0 0 0 0 ...];
% occur_th = max number of representations
% trial_type = identity of odor can be [1 3 6 7 ...] or [21 22 31 ...]
% sf0  = mean firing rate or sniffing frequency with the same number
% of trial type

% keyboard
ntype = unique(trial_type);
odor_ID =  ntype(condition);
ocidx = nan(occur_th,length(odor_ID));
hab_resp = nan(length(occur_th),1);
hab_ste = nan(length(occur_th),1);
for io = 1:occur_th
    
    for in = 1:length(odor_ID)
        try
            n = odor_ID(in);
            ocidx(io,in) = find(io==trial_occur&trial_type == n);
        catch fprintf('no trials for %d rep for odor %d\n',io,n)
        end
        
    end

    idx = ocidx(io,:);
    if sum(isnan(idx))>0
        idx = idx(~isnan(idx));
        hab_resp(io) = nanmean(sf0(idx));
        hab_ste(io) = nanstd(sf0(idx))./sqrt(length(idx));
        
    else if sum(isnan(idx))==length(odor_ID)
            hab_resp(io) = nan;
            hab_ste(io) = nan;
            
        else
            hab_resp(io)= nanmean(sf0(idx));
            hab_ste(io)= nanstd(sf0(idx))./sqrt(length(idx));
        end
    end
    
end