clear all
close all
clc


%
varnames = {'exp_name','ts','session_name',...
    'stim_type',...
    'ts_trials',...
    'f0','trial_type','trial_idx',...
    'chemical_list','left_right','trial_occur',...
    'recording_site','mf0','sf0','novelty','blank','sig',...
    'sig_val','resp_h','cam','breath','events','mod_ind_fam',...
    'mod_ind_nov'};
dataset = 'lr';
foldername = '.';
[data] = get_data(foldername,dataset,varnames);
disp('Data loaded.')


%%


blank = logical(data(1).blank);
novelty = logical(data(1).novelty);

all_f0 = [];
all_sig = [];
all_resp_sig = [];
all_mag = [];
all_bsp = [];
all_mod_ind_fam = [];
all_mod_ind_nov = [];
all_bursts = [];
rsite = [];
stimdur = 5;
binsize = 0.06;
samp_freq=60;
edges = 0:binsize:stimdur;

sigma =2;
x = -4*sigma:4*sigma;
kernel = exp(-x .^ 2 / (2*sigma ^ 2)); kernel = kernel./sum(kernel);

for expt = 1:length(data)
    dd = data(expt);
    mag = nan(length(dd.ts),1);
%     keyboard
   
    if ~isempty(dd.ts_trials)&&~isempty(dd.cam)
         dd.cam.onsets = dd.cam.onsets+min(dd.events.raw);
        ntrials = length(dd.ts_trials{1});
        mean_bursts = nan(length(dd.ts),1);
        finhtime = nan(1,ntrials);
        bin_resp = nan(length(dd.ts),length(edges));
        recwind =  length(dd.breath.cam_resp{1});
       
        if recwind ==0
            recwind = 663;
        end
%         tstamps = nan(ntrials,length(dd.breath.cam_resp{1}));
        tstamps = nan(ntrials,recwind);
        for ii = 1:ntrials -1
            tstamps(ii,:)= linspace(dd.cam.onsets(ii),dd.cam.onsets(ii)+(recwind/samp_freq),recwind);
            inhstamps = tstamps(ii,dd.breath.inh_onsets{ii});
%             finhtime(ii) = inhstamps(find(inhstamps>dd.events.onsets(ii)+0.1,1,'first'));
            finhtime(ii) = inhstamps(find(inhstamps>dd.events.onsets(ii),1,'first'));
        end
        
%         keyboard
        for ic = 1:length(dd.ts)
            
            %compute bursts
%             find blank trials

            ttypes = unique(dd.trial_type);
            bl_stim = find(blank==1);
            tstim = [];
            for ii = 1:length(bl_stim)
                tstim = [tstim; find(dd.trial_type==ttypes(bl_stim(ii)))];
            end

           spks = dd.ts_trials{ic}(tstim);
           burst_count = 0;
           ts = cell2mat(spks);
           isis = cellfun(@diff,spks,'uniformoutput',0);
           
           bursts = cell(size(isis));
           for b = 1:length(isis)
               burst_idx = find(isis{b}<0.004);
               for ii = 1:length(burst_idx)
                   try
                       if isis{b}(burst_idx(ii)+1)>0.1
                           burst_count = burst_count+1;
                           bursts{b} = [bursts{b},spks(burst_idx(ii))];
                       end
                   end
               end
           end
           mean_bursts(ic) = burst_count./length(tstim);
            
            %compute cycle averages
            tts_trials{ic} = extract_trial_spikes(dd.ts{ic},finhtime-1,finhtime+stimdur);
%             keyboard
            bsp = nan(length(edges),1);
            bl_bsp = nan(length(edges),1);
            % positive and negative index
            bl = mean(dd.mf0(ic,blank),2);
            fl = mean(dd.mf0(ic,~blank),2);
            mag(ic) = (fl-bl)/fl+bl;
            
%            [~,pidx] = nanmax(dd.mf0(ic,:)); %find the most significant response
%            tidx = find(dd.trial_type==pidx);
           ttrial = unique(dd.trial_type);
%            r_idx = [find(dd.trial_type==ttrial(2));find(dd.trial_type==ttrial(3));...
%                find(dd.trial_type==ttrial(4));find(dd.trial_type==ttrial(5));...
%                find(dd.trial_type==ttrial(6));find(dd.trial_type==ttrial(7));...
%                find(dd.trial_type==ttrial(9));find(dd.trial_type==ttrial(10));...
%                find(dd.trial_type==ttrial(11));find(dd.trial_type==ttrial(12));...
%                find(dd.trial_type==ttrial(13));find(dd.trial_type==ttrial(14))];
           
           
               r_idx = [find(dd.trial_type==ttrial(4));find(dd.trial_type==ttrial(5));...
               find(dd.trial_type==ttrial(6));find(dd.trial_type==ttrial(7));...
               find(dd.trial_type==ttrial(11));find(dd.trial_type==ttrial(12));...
               find(dd.trial_type==ttrial(13));find(dd.trial_type==ttrial(14))];
           
                     
           tmp_ts = cell2mat(tts_trials{ic}(dd.trial_idx(r_idx))');
           
%            tmp_ts = cell2mat(dd.ts_trials{ic});
           bsp = histc(tmp_ts,edges)./length(dd.trial_idx(r_idx));
           bsp_conv = nan(1,length(bsp));
           bsp_conv = squeeze(conv(bsp,kernel,'same'));

             
           bl_conv = nan(1,length(bsp));
           bl_idx = [find(dd.trial_type==ttrial(1));find(dd.trial_type==ttrial(8))];
           tmp_blank = cell2mat(tts_trials{ic}(dd.trial_idx(bl_idx))');
           bl_bsp = histc(tmp_blank,edges)./length(bl_idx);
           bl_conv = squeeze(conv(bl_bsp,kernel,'same'));
%            keyboard
           
%            bin_resp(ic,:) = bsp_conv./max(bsp_conv);
           if isequal(size(bl_conv),size(bsp_conv))
           tmp_conv  = bsxfun(@minus,bsp_conv,bl_conv);
%            tmp_conv  = bsxfun(@minus,bsp_conv,min(bsp_conv));
%            tmp_conv  = bsxfun(@minus,bsp_conv,bl_conv)./(bsp_conv+bl_conv);
            
           bin_resp(ic,:) = zscore(tmp_conv);
           else
%            tmp_conv  = bsxfun(@minus,bsp_conv,bl_conv')./(bsp_conv+bl_conv');
           tmp_conv  = bsxfun(@minus,bsp_conv,bl_conv');
           bin_resp(ic,:) = zscore(tmp_conv);
           end      

        end
        
        rsite = [rsite,dd.recording_site];
        all_bsp = [all_bsp;bin_resp];
        all_mag = [all_mag;mag];
        all_sig = [all_sig;nansum(dd.sig,2)];
        all_resp_sig = [all_resp_sig;dd.resp_h];
        all_mod_ind_fam = [all_mod_ind_fam, dd.mod_ind_fam];
        all_mod_ind_nov = [all_mod_ind_nov, dd.mod_ind_nov];
        all_bursts = [all_bursts;mean_bursts];
    
    end
%     all_f0 = vertcat(data.mf0);
end
% all_f0 = vertcat(data.mf0);

%%

resp_idx = [all_resp_sig>0]';
% cond = {strcmp(rsite,'aon')&sig_idx&resp_idx,...
%     strcmp(rsite,'piriform')&sig_idx&resp_idx};
cond = {[strcmp(rsite,'aon')&resp_idx],...
    strcmp(rsite,'piriform')&resp_idx};
a = nan(length(all_resp_sig),1);
a(find (strcmp(rsite,'aon')&resp_idx))=1;
a(find (strcmp(rsite,'piriform')&resp_idx))=2;

cell_idx = cond{1};
% |cond{2};
dat = all_bsp(cell_idx,:);
expind = a(cell_idx);
mm_nov = all_mod_ind_nov(cell_idx);
mm_fam = all_mod_ind_fam(cell_idx);
s_bur = all_bursts(cell_idx);
% imagesc(all_bsp(cond{1}|cond{2},:))
% dat = all_bsp(cond{1}|cond{2},:);
% isnan(mean(dat))
%%

ncluster = 3;
[c, ~,sumd]= kmeans(dat,ncluster,'Replicates',10);
idx = 1:length(c);
[v,ii] = sort(c);
sortedidx = idx(ii);
sorted_cycle_avg = dat(sortedidx,:);
clind = arrayfun(@(x) find(v==x,1,'last'),unique(v));

%%
linewidth = 1;
printfigure = false;
% figure,
clf
clib = jet(ncluster+5);
%  {[146/255,71/255,149/255],[0 0 1],[63/255,143/255,56/255]};
% PLOT CYCLE AVERAGES ALL
axclusters = axes('position',[.02,.2,.28,.77]);
imagesc([sorted_cycle_avg, zeros(length(idx),3)],[0,1]),hold all
colormap(b2r(-0.5,0.5))

set(axclusters,'visible','off','box','off')
% colorbar
ttlen = get(gca,'ticklength')*5;
cb = colorbar('location','manual','position',[0.92,0.04,0.01,0.1],...
    'ticks',[0,1],'tickdirection','out',...
    'ticklength',ttlen(1),'ticklabels',{'0','1'});
xlabel(cb,sprintf('Normalized\nFiring Rate'))

plot(1:size(dat,2),repmat(clind,1,size(dat,2)),'color',[1,0,0],'linewidth',1)
axis tight

tmp_time = linspace(-1,stimdur,size(sorted_cycle_avg,2));
zz = find(tmp_time>=0,1,'first');
plot([zz,zz],ylim,'r--','linewidth',1)


cc = lines(max(expind));
expinds = zeros(length(expind),1,3);

% for i = 1:2
%     expinds(i==expind,:,:) = repmat(cc(i,:),[sum(expind == i),1,1]);
% end

% axexp = axes('position',[.31,.01,.01,.98]);
% imagesc(expinds(sortedidx,:,:),[0 1]),hold all
% set(axexp,'box','off','visible','off')
% text(min(xlim),min(ylim),sprintf('Exp'),'fontsize',7)

% PLOT MEAN CYCLE AVERAGES


m_ax = [0.88,0.73,0.58];

% axmeanresponses = axes('position',[.35,.54,.24,.47]);
hold all
cont = 1;
cutbins = 3;
for kk = length(unique(v)):-1:1%1:length(unique(v))
    mean_resp = nanmean(sorted_cycle_avg(v==kk,:));
     axmeanresponses = axes('position',[.35,m_ax(kk),.24,.10]); hold on
   
    plot(tmp_time,mean_resp,'color',clib(kk,:),'linewidth',1),
   
    set(gca,'fontsize',7,'tickdir','out','ylim',[0 1.5],...
        'xlim',[min(tmp_time) max(tmp_time)],'color','none')
    cont = cont+1;
end


m2_ax = [0.80,0.55,0.30];
axmodf0 = [];
% axmodf0 = axes('position',[.68,.50,.1,.2]);
for kk = length(unique(v)):-1:1    
    all_idx = sortedidx(find(v==kk)');
    edges = [-1:binsize:2];
    tempdd{kk} = mm_nov(all_idx);
    bins = histc(tempdd{kk},edges);
    ss = cumsum(bins);
    ss = ss/max(ss);
    axmodf0 = [axmodf0,axes('position',[.68,m2_ax(kk),.1,.2])]; hold on
    plot(edges,ss,'color',clib(kk,:),'linewidth',linewidth),hold all,
    tempfd{kk} = mm_fam(all_idx);
    bins = histc(tempfd{kk},edges);
    ss = cumsum(bins);
    ss = ss/max(ss);
    plot(edges,ss,'color',[.5 .5 .5],'linewidth',linewidth),hold all,
    ylim([0,1.01])
    xlim([min(edges),max(edges)])
    [h p] = kstest2(tempdd{kk},tempfd{kk});
    text(0.1,0.1,sprintf('p = %1.3f',p),'fontsize',8);
    %         set(gca,'color','none','box','off','fontsize',8)
end
axis(axmodf0,'square')
ylabel('CDF','fontsize',8)
% xlabel('Modulation (%)','fontsize',8)
% title('F0','fontweight','bold','fontsize',8)
set(axmodf0,'fontsize',8,'box','off','tickdir','out',...
    'color','none','ticklength',get(gca,'ticklength')*5,'xtick',[-1 0 1 2],...
    'xticklabel',[-100 0 100 200])



% PLOT BURST RATE
axburst = axes('position',[.42,.30,.1,.2]);
binsize = 0.01;

for kk = length(unique(v)):-1:1
    all_idx = sortedidx(find(v==kk));
    tempdd_sta{kk} = s_bur(all_idx);
    
    mm1 = nanmean(tempdd_sta{kk});
    ss1 = nanstd(tempdd_sta{kk});
    le1 = length(tempdd_sta{kk});
    ste1 = ss1./sqrt(le1);

    ee1 = errorbar([kk],[mm1],[ste1],'-');hold on,
    set(ee1,'color',clib(kk,:),'markersize',3,'markerfacecolor',[0 0 0],...
        'markeredgecolor','none','LineWidth',linewidth);

    ylim([0,1])
    set(gca,'color','none','box','off','fontsize',8)
    set(gca,'xtick',1:3,'xticklabel',{'C1','C2','C3'},'fontsize',8,...
        'xlim',[0.7 3.3])
end
ylabel(sprintf('Burst Rate\nFraction'),'fontsize',8)
set(axburst,'ticklength',get(gca,'ticklength')*5,'tickdir','out')


if printfigure
set(gcf,'paperunits','centimeter','paperposition',[0,0,22,12],'papersize',[22,12])
foldername = 'figures\';

fname = sprintf('%s\\sniff_cycle_average_clusters_sta_only%d%s.pdf',...
    foldername,ncluster,'PIR');
print(gcf,'-dpdf','-painters','-loose',...
    fname)
end

%%

clf
X = sorted_cycle_avg;
N = length(X);

x_mean = mean(X, 1);

X = bsxfun(@minus,X,x_mean);

covar_m = 1/(N-1)*(X'*X);

% find the eigenvectors and eigenvalues
% PC are the principal components, i.e. eigenvectors
% and V are the corresponding eigenvalues
[PC, V] = eig(covar_m);

PC(:,1)'*PC(:,2)
% imagesc(PC)
%%
V = diag(V);

[tmp, ridx] = sort(V, 1, 'descend');
V = V(ridx);
PC = PC(:,ridx);
%%
clf
% Show the data in original basis
scatter(X(:,1), X(:,2));
xlabel('x'); ylabel('y');
axis([-2 2 -2 2])
hold on;
% Plot the first new basis
hh = refline(PC(2,1)/PC(1,1), 0);
hh.Color = 'r';
hold on;
% Plot the second new basis
hh = refline(PC(1,2)/PC(2,2), 0);
hh.Color = [0.7 0.7 0.1];
axis([-2 2 -2 2])
box on;

%%
clib = {[1,0,0],[0,1,0],[0,0,1],[1,0,1],[0,1,1]};

cnew = nan(size(dat,1),3);
for kk = 1:length(unique(c))
    
    idx = find(v ==kk);
    for ij = 1:length(idx)
        cnew(idx(ij),:,:) = clib{kk};
    end
%     cnerw(idx,:) = clib{kk};
    
    
    
end


%%
clf
% Transform the original data X to the principal subspace
PC_X = X * PC;
% Plot the data in the new basis
ax = subplot(1,1,1);

scatter3(PC_X(:,1),PC_X(:,2),PC_X(:,3),ones(length(PC_X(:,1)),1).*20,cnew)
% scatter(PC_X(:,1), PC_X(:,2),ones(1,size(dat,1)).*8,cnew)
xlabel('1st Principal Component');
ylabel('2nd Principal Component');

xlim([-6 12])
ylim([-8 6])
set(ax,'box','off','tickdir','out',...
    'fontsize',8,'ticklength',get(ax(end),'ticklength')*5,...
    'linewidth',0.5)



set(gcf,'paperunits','centimeter','paperposition',[0,0,4,4],'papersize',[4,4])
            foldername = 'figures\';
            fname = sprintf('%s\\pca.pdf',...
                foldername);
            print(gcf,'-dpdf','-painters','-loose',...
                fname)



% set(gca)
% xlim([-2.3 2.3]); ylim([-2.3 2.3])

%% 3 clusters

clf
% Transform the original data X to the principal subspace
PC_X = X * PC;
% Plot the data in the new basis
ax = subplot(1,1,1);

scatter3(PC_X(:,1), PC_X(:,2),PC_X(:,3),ones(1,size(dat,1)).*8,cnew)
xlabel('1st Principal Component');
ylabel('2nd Principal Component');

xlim([-6 12])
ylim([-8 6])
set(ax,'box','off','tickdir','out',...
    'fontsize',8,'ticklength',get(ax(end),'ticklength')*5,...
    'linewidth',0.5)



set(gcf,'paperunits','centimeter','paperposition',[0,0,4,4],'papersize',[4,4])
            foldername = 'figures\';
            fname = sprintf('%s\\pca.pdf',...
                foldername);
            print(gcf,'-dpdf','-painters','-loose',...
                fname)



set(gca)
% xlim([-2.3 2.3]); ylim([-2.3 2.3])

%%

tree = linkage(PC_X(:,1),'average');
dendrogram(tree)


%%

vv = dd.breath.cam_resp{3};


N = size(vv, 1);
nfft = 2^nextpow2(2*N-1);
F=fft(vv(:,2), nfft);
M=fft(vv(:,3), nfft);

R=F.*conj(M);

r = ones(nfft,1);

for k = 0.1:0.05:0.9
    r = ifft(R(1:end-ceil(k*nfft)));
    r = r.*r;
end

rmax = max(abs(r));
x = find(abs(r) == rmax);  

