clear all
close all
clc


%
varnames = {'exp_name','ts','session_name',...
    'chemical_list','trial_occur',...
    'recording_site','sig','resp_h',...
    'mod_ind_nov','mod_ind_fam','mf0'};
dataset = 'lr';
foldername = '.';
[data] = get_data(foldername,dataset,varnames);
disp('Data loaded.')

%%

tmp = arrayfun(@(x)(x.recording_site),data,'uniformoutput',0);
rsite = horzcat(tmp{:});

all_f0 = [];
all_sig = [];
all_resp = [];
all_mod_ind_fam = [];
all_mod_ind_nov = [];
for expt = 1:length(data)
    dd = data(expt);
    tmp = dd.mf0;
    if ~isempty(tmp)
    all_sig = [all_sig;nansum(dd.sig,2)];
    all_resp = [all_resp;dd.resp_h];
    all_mod_ind_nov = [all_mod_ind_nov,dd.mod_ind_nov];
    all_mod_ind_fam = [all_mod_ind_fam,dd.mod_ind_fam];
    end
%     all_f0 = vertcat(data.mf0);
end


%% aon piriform superimposed
clf
printfigure = true;
clib = {[146/255,71/255,149/255],[63/255,143/255,56/255]};
% sig_idx = [all_sig>=1]';
resp_idx = [all_resp>0]';
% cond = {strcmp(rsite,'aon')&sig_idx,...
%     strcmp(rsite,'piriform')&sig_idx};
cond = {strcmp(rsite,'aon')&resp_idx,...
    strcmp(rsite,'piriform')&resp_idx};



edges = -100:1:110;
ax = [];
mm = {};
for c = 1:length(cond)
    idx = cond{c};
    sum(idx)
    mm{c} = all_mod_ind_nov(idx).*100;
    bins=histc(mm{c},edges);
    ss = cumsum(bins);
    ss = ss/max(ss);
    ax = subplot(1,1,1);
    plot(edges,ss,'color',clib{c});hold all
    plot([0 0],[0 1],'--','color',[.5 .5 .5])
    %     bb = bar(edges,mb);
    %     set(bb,'facecolor',clib{c},'edgecolor','none'),hold all
end


[h p]=kstest2(mm{1},mm{2},'tail','unequal')

text(10,.10,sprintf('p=%1.3f',p))
ylabel('CDF')
xlabel('Modulation Index')

set(ax,'box','off','tickdir','out',...
    'ticklength',get(ax(end),'ticklength').*5,'xlim',[min(edges),max(edges)])


axis('square')
set(gcf,'paperunits','centimeters','papersize',[4.2*1,4.2*1],...
    'paperposition',[0,0,4.2*1,4.2*1])

set(findall(gcf,'-property','FontSize'),'FontSize',8)

printfolder = '\figures\';
if printfigure
    print(gcf,'-dpdf',sprintf('%s\\%s\\modulation_cumsum_compare_nn.pdf',pwd,printfolder))
end

%% familiar novel superimposed for both regions
clf

printfigure = true;
clib = {[146/255,71/255,149/255],[63/255,143/255,56/255]};
sig_idx = [all_sig>=1]';
resp_idx = [all_resp>0]';
% cond = {strcmp(rsite,'aon')&sig_idx,...
%     strcmp(rsite,'piriform')&sig_idx};
cond = {strcmp(rsite,'aon')&resp_idx,...
    strcmp(rsite,'piriform')&resp_idx};


edges = -100:1:100;
ax = [];
mm = {};
for c = 1:length(cond)
    ax = [ax,subplot(1,2,c)];
    idx = cond{c};
    mm{c} = all_mod_ind_nov(idx).*100;
    bins=histc(mm{c},edges);
    ss = cumsum(bins);
    ss = ss/max(ss);
    
    plot(edges,ss,'color',clib{c},'linewidth',1);hold all
    
    mmf{c} = all_mod_ind_fam(idx).*100;
    bins=histc(mmf{c},edges);
    ss = cumsum(bins);
    ss = ss/max(ss);
    
    plot(edges,ss,'color',[0.5 0.5 0.5],'linewidth',1);
    plot([0 0],[0 1],'--','color',[.5 .5 .5])
    [h p]=kstest2(mm{c},mmf{c});
    text(10,0.1,sprintf('p = %1.4f',p))
    ylabel('CDF')
    xlabel('Modulation index (%)')
    %     bb = bar(edges,mb);
    %     set(bb,'facecolor',clib{c},'edgecolor','none'),hold all
end

set(ax,'box','off','tickdir','out',...
    'ticklength',get(ax(end),'ticklength').*5,'xlim',[min(edges),max(edges)])


axis(ax,'square')
set(gcf,'paperunits','centimeters','papersize',[4.2*2,4.2*1],...
    'paperposition',[0,0,4.2*2,4.2*1])

set(findall(gcf,'-property','FontSize'),'FontSize',8)

printfolder = '\figures\';
if printfigure
    print(gcf,'-dpdf',sprintf('%s\\%s\\modulation_cumsum_fam_nov_compare_nn.pdf',pwd,printfolder))
end


%%
[h p]=kstest2(mm{1},mm{2})

ylabel('CDF')
xlabel('Modulation Index')

set(ax,'box','off','tickdir','out',...
    'ticklength',get(ax(end),'ticklength').*5,'xlim',[min(edges),max(edges)])


axis('square')
set(gcf,'paperunits','centimeters','papersize',[4.2*1,4.2*1],...
    'paperposition',[0,0,4.2*1,4.2*1])

set(findall(gcf,'-property','FontSize'),'FontSize',8)

printfolder = 'E:\local\users\cagatay\aon-priform\figures\manuscript';
if printfigure
    print(gcf,'-dpdf',sprintf('%s\\modulation_cumsum_compare.pdf',printfolder))
end

%%

for expt = 1:length(data)
    dd = data(expt);
    
    
end
