 clear all
close all
clc


%
varnames = {'exp_name','ts','session_name',...
    'stim_type','events',...
    'ts_trials','ts',...
    'f0','trial_type','trial_idx',...
    'chemical_list','left_right','trial_occur',...
    'recording_site','mf0','sf0','novelty','blank','sig',...
    'resp_h','sig_val',...
    'cam','breath'};
dataset = 'lr';
foldername = '.';
[data] = get_data(foldername,dataset,varnames);
disp('Data loaded.')

%%

all_nov = [];
all_fam = [];
all_bla = [];

for expt = 1:length(data)
    
    dd = data(expt);
%     keyboard
    ttype = dd.trial_type;
    ntype = unique(ttype);
    oc = dd.trial_occur;
    noccur = 40; %max number of representations
    
        %compute number of sniffs for each trial
        stim_on = dd.events.onsets;
        cam_on = dd.cam.onsets+min(dd.events.raw);
        
        ntrials = length(stim_on);
        recwind =  length(dd.breath.cam_resp{1});
        if recwind == 0
            recwind = 663;
        end
        tstamps = nan(ntrials,recwind);
        sniff0 = nan(ntrials,1);
        nov = logical(dd.novelty);
        bla = logical(dd.blank);
        fam = ~logical(dd.novelty)&~logical(dd.blank);
        
        samp_freq = 60;
        
        for ii = 1:ntrials
            it = dd.trial_idx(ii);
            tstamps(it,:)= linspace(cam_on(ii),cam_on(ii)+(recwind/samp_freq),recwind);
            inhstamps = tstamps(it,dd.breath.inh_onsets{it});
%             base = length(inhstamps(find(inhstamps>stim_on(it)-1.3 & inhstamps<stim_on(it)+0.3)));
             base = 0;
            resp = length(inhstamps(find(inhstamps>stim_on(ii)+0.3 & inhstamps<stim_on(ii)+3.3)))./3;
            sniff0(ii) = resp - base;
%             stim_on(ii)
        end
        
        
        cond = {ntype(nov),ntype(fam),ntype(bla)};
        
        for icond = 1:length(cond)
            tmp_odors = cond{icond};
            I = arrayfun(@(x,y) find(ttype==x,3), tmp_odors,'UniformOutput', false);
            idx = cell2mat(I);
            tmp_sniff{icond} = sniff0(idx);
        end
   all_nov = [all_nov; tmp_sniff{1}];
   all_fam = [all_fam; tmp_sniff{2}];
   all_bla = [all_bla; tmp_sniff{3}];
end

%%
clib = [.5 .5 .5; 0 0 0 ; 0 0 1];
ax = [];
clf,

edges = 0:0.5:10;

ax = [ax,subplot(2,1,1)];

tempdd = all_nov;
bins = histc(tempdd,edges);
% bb = bar(edges(1:end-1),bins/sum(bins));
plot(edges,bins/sum(bins),'color',clib(3,:),'linewidth',ll);
hold all

tempdd = all_fam;
bins = histc(tempdd,edges);
% bb = bar(edges(1:end-1),bins/sum(bins));
plot(edges,bins/sum(bins),'color',clib(2,:),'linewidth',ll);


tempdd = all_bla;
bins = histc(tempdd,edges);
% bb = bar(edges(1:end-1),bins/sum(bins));
plot(edges,bins/sum(bins),'color',clib(1,:),'linewidth',ll);


%%


ax = [];
ll = 1;
printfigure = true;
clf,

% edges = 1:0.1:10;

ax = [ax,subplot(1,1,1)];

tempdd = all_nov;
bins = histc(tempdd,edges);
ss = cumsum(bins);
ss = ss/max(ss);
% bb = bar(edges(1:end-1),bins/sum(bins));
plot(edges,ss,'color',clib(3,:),'linewidth',ll);
hold all

tempdd = all_fam;
bins = histc(tempdd,edges);
ss = cumsum(bins);
ss = ss/max(ss);
plot(edges,ss,'color',clib(2,:),'linewidth',ll);


tempdd = all_bla;
bins = histc(tempdd,edges);
ss = cumsum(bins);
ss = ss/max(ss);
% bb = bar(edges(1:end-1),bins/sum(bins));
plot(edges,ss,'color',clib(1,:),'linewidth',ll);

set(ax,'box','off','tickdir','out',...
    'ticklength',get(ax(end),'ticklength').*5,'xlim',[min(edges),max(edges)],...
    'xtick',[0 5 10],'ytick',[0 0.5 1])


axis('square')
set(gcf,'paperunits','centimeters','papersize',[4.2*1,4.2*1],...
    'paperposition',[0,0,4.2*1,4.2*1])

set(findall(gcf,'-property','FontSize'),'FontSize',8)

printfolder = 'E:\local\users\cagatay\aon-priform\figures\manuscript';
if printfigure
    print(gcf,'-dpdf',sprintf('%s\\sniff_distributions_3sniff.pdf',printfolder))
end



%% % example traces for sniff

expt = 1;

dd =data(expt);

ttype = dd.trial_type;
ntype = unique(ttype);
oc = dd.trial_occur;
noccur = 40; %max number of representations

%compute number of sniffs for each trial
stim_on = dd.events.onsets;
sorted_stimon = stim_on(dd.trial_idx);
cam_on = dd.cam.onsets+min(dd.events.raw);

ntrials = length(stim_on);
recwind =  length(dd.breath.cam_resp{1});
if recwind == 0
    recwind = 663;
end
tstamps = nan(ntrials,recwind);
sniff0 = nan(ntrials,1);
nov = logical(dd.novelty);
bla = logical(dd.blank);
fam = ~logical(dd.novelty)&~logical(dd.blank);

samp_freq = 60;
tmpb = nan(ntrials,recwind);
for ii = 1:ntrials
    it = dd.trial_idx(ii);
    tstamps(ii,:)= linspace(cam_on(ii),cam_on(ii)+(recwind/samp_freq),recwind);
    inhstamps{ii} = tstamps(it,dd.breath.inh_onsets{ii});
    tmpb(ii,:) = dd.breath.cam_resp{ii};
end

 cond = {ntype(nov),ntype(fam),ntype(bla)};
 sorted_tstamps = tstamps(dd.trial_idx,:);
 sorted_tmpb = tmpb(dd.trial_idx,:);
 tmp_sniff = {};
 tmp_time = {};
 tmp_onsets = {};
 tmp_inh = {};

 
 for icond = 1:length(cond)
     tmp_odors = cond{icond};
     I = arrayfun(@(x,y) find(ttype==x), tmp_odors,'UniformOutput', false);
     idx = cell2mat(I);
     tmp_sniff{icond} = sorted_tmpb(idx,:);
     tmp_time{icond} = sorted_tstamps(idx,:);
     tmp_onsets{icond} = sorted_stimon(idx);
     sorted_inh
 end

 
 %%
 clf
 opol = 6;
 ax = [];
 
 ax = [ax,subplot(3,1,1)];
 tr = 7;
 sidx = find(tmp_time{1}(tr,:)>tmp_onsets{1}(tr),1,'first');
 tt = tmp_time{1}(tr,:)-min(tmp_time{1}(tr,:));
 t = tmp_sniff{1}(tr,:);
 [p,s,mu] = polyfit(tt,t,opol);
 f_y = polyval(p,tt,[],mu);
 rr=t - f_y;
 rr = (rr-min(rr))/(max(rr)-min(rr));
 plot(tt,rr),hold on
 plot([tt(sidx) tt(sidx)],[0 1])
 plot([max(tt)-2 max(tt)],[-.1 -.1],'color','k')
 
 ax = [ax,subplot(3,1,2)];
 tr = 47;
 sidx = find(tmp_time{2}(tr,:)>tmp_onsets{2}(tr),1,'first');
 tt = tmp_time{2}(tr,:)-min(tmp_time{2}(tr,:));
 rr = tmp_sniff{2}(tr,:);
 rr = (rr-min(rr))/(max(rr)-min(rr));
 plot(tt,rr),hold on
 plot([tt(sidx) tt(sidx)],[0 1])
 plot([max(tt)-2 max(tt)],[-.1 -.1],'color','k')
 
 ax = [ax,subplot(3,1,3)];
 tr = 18;
 sidx = find(tmp_time{3}(tr,:)>tmp_onsets{3}(tr),1,'first');
 tt = tmp_time{3}(tr,:)-min(tmp_time{3}(tr,:));
 rr = tmp_sniff{3}(tr,:);
 rr = (rr-min(rr))/(max(rr)-min(rr));
 plot(tt,rr),hold on
 plot([tt(sidx) tt(sidx)],[0 1])
 plot([max(tt)-2 max(tt)],[-.1 -.1],'color','k')
 
 
set(ax,'box','off','visible','off')
set(gcf,'paperunits','centimeters','papersize',[4.2*2,6*1],...
    'paperposition',[0,0,4.2*2,6*1])

set(findall(gcf,'-property','FontSize'),'FontSize',8)

printfolder = 'E:\local\users\cagatay\aon-priform\figures\manuscript';

if printfigure
    print(gcf,'-dpdf',sprintf('%s\\example_sniff.pdf',printfolder))
end

 
 
