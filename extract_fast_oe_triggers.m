
function events = extract_fast_oe_triggers(rawfile,savefile,fs,nchannels,chidx)
% syncData = function extractSyncChan(lfpFilename)
tic
d = dir(rawfile);
nSamps = d.bytes/2/nchannels;
mmf = memmapfile(rawfile, 'Format', {'int16', [nchannels nSamps], 'x'});
% keyboard

events = {};
syncData = double(mmf.Data.x(chidx(1),:));
tt = linspace(0,length(syncData)./fs,length(syncData));
ifs = fs/30;%1000
itt = 0:1/ifs:length(syncData)/fs;
for ii= 1:length(chidx)
    syncData = double(mmf.Data.x(chidx(ii),:));
    xq = interp1(tt,syncData,itt);
    xq = (xq-min(xq))./(max(xq)-min(xq));%normalize 0-1
    th = 0.5;
    if chidx(ii)==69
        [tmp_idx,~] = peakseek(xq,[ifs*0.01],th); %0.5s to second pulse
    else
        [tmp_idx,~] = peakseek(xq,[ifs*.5],th); %0.5s to second pulse
    end

    %     th = 0.5;
%     tmp_idx = find(xq>=th);

    %     th = mean(syncData).*2;
%     [tmp_idx,~] = peakseek(syncData,[fs*.5],mean(syncData).*2);
%     [~, tmp_idx]=findpeaks(syncData,tt,...
%         'minpeakprominence',th);
    
    nfile = sprintf('onsets_%d',chidx(ii));
    events.(nfile) = tmp_idx;
    nfile = sprintf('th_%d',chidx(ii));
    events.(nfile) = itt(tmp_idx);
    nfile = sprintf('raw_%d',chidx(ii));
    events.(nfile) = xq;
    nfile = sprintf('time_%d',chidx(ii));
    events.(nfile) = itt;
    
%     cd(path);
%     ffile = sprintf('%s%s',name,ext);
% keyboard
%     s = fullfile(path,sprintf('%s%s',name,'.mat'));
    if isdir(savefile)
        save(savefile,'-struct','events','-append');
    else
        save(savefile,'-struct','events');
    end

end
toc

