clear all
close all
clc


%
varnames = {'exp_name','ts','session_name',...
    'stim_type','events',...
    'ts_trials','ts',...
    'f0','trial_type','trial_idx',...
    'chemical_list','left_right','trial_occur',...
    'recording_site','mf0','sf0','novelty','blank','sig',...
    'resp_h','sig_val',...
    'cam','breath'};
dataset = 'lr';
foldername = '.';
[data] = get_data(foldername,dataset,varnames);
disp('Data loaded.')


%%
clc
expt = 1;

dd = data(expt)
%%

a = dd.events.onsets;


samp_freq=60;
ntrials = length(dd.cam.onsets);
recwind =  length(dd.breath.cam_resp{1});
tstamps = nan(ntrials,recwind);
tt = 180;
bidx = nan(ntrials,tt);
tmpidx = [];
finhtime = nan(ntrials,1);
ninh = nan(ntrials,1);
for ii = 1:ntrials
tstamps(ii,:)= linspace(dd.cam.onsets(ii),dd.cam.onsets(ii)+(recwind/samp_freq),recwind);
inhstamps = tstamps(ii,dd.breath.inh_onsets{ii});
ninhstamps = tstamps(ii,dd.breath.inh_onsets{ii});
finhtime(ii) = inhstamps(find(inhstamps>dd.events.onsets(ii)+0.2,1,'first'));
ninh(ii) = length(inhstamps(inhstamps>=dd.events.onsets(ii)&inhstamps<=dd.events.onsets(ii)+2));
% tmpidx = find(tstamps(ii,:)>=dd.events.onsets(ii));
tmpidx = find(tstamps(ii,:)>=finhtime(ii));
inhtimes{ii} =  inhstamps(find(inhstamps>dd.events.onsets(ii)+0.2));

tmp = dd.breath.cam_resp{ii}(tmpidx(1:tt));
opol = 6;
t = 1:length(tmp);
[p,s,mu] = polyfit(t,tmp,opol);
f_y = polyval(p,t,[],mu);

c= tmp - f_y;
% clf
% plot(c,'r'),hold on
% yyaxis('right')
% plot(tmp,'b')
% keyboard
bidx(ii,:) = (c-min(c))/(max(c)-min(c)) ;
end



%%
clf,
c =1;

ax = [];
     
ax = [ax,subplot(3,2,[1 2])];
hold all
tts_trials{c} = extract_trial_spikes(dd.ts{c},finhtime,finhtime+1);
sorted_trials = tts_trials{c}(dd.trial_idx);
plot_rastergram(sorted_trials,0,1,'convimg','color',[0 0 0]),hold all
% for ii = 1:length(line_sep)
%     plot([1 5000],[line_sep(ii) line_sep(ii)],'color',cc(ii,:),'linewidth',1)
% end

set(ax(end),'visible','off')
axis('tight')

ax = [ax,subplot(3,2,[3 4])];
hold all
sorted_breath = bidx(dd.trial_idx,:);
imagesc(sorted_breath)
% for ii = 1:length(line_sep)
%     plot([1 5000],[line_sep(ii) line_sep(ii)],'color',cc(ii,:),'linewidth',1)
% end

set(ax(end),'visible','off')
axis('tight')

ax = [ax,subplot(3,2,[5 6])];
hold all

sorted_trials = dd.ts_trials{c}(dd.trial_idx);
plot_rastergram(sorted_trials,0,1,'convimg','color',[0 0 0]),hold all

% for ii = 1:length(line_sep)
%     plot([1 5000],[line_sep(ii) line_sep(ii)],'color',cc(ii,:),'linewidth',1)
% end

set(ax(end),'visible','off')
axis('tight')

%% plot aligned with the first sniff

a = dd.events.onsets;

samp_freq=60;
ntrials = length(dd.cam.onsets);
recwind =  length(dd.breath.cam_resp{1});
tstamps = nan(ntrials,recwind);
tt = 180;
bidx = nan(ntrials,tt);
tmpidx = [];
finhtime = nan(ntrials,1);
sinhtime = nan(ntrials,1);
tinhtime = nan(ntrials,1);
ninh = nan(ntrials,1);
diffevents = dd.events.onsets- dd.events.raw(2:4:end)';
aligned_onsets = dd.events.raw(2:4:end)';
for ii = 1:ntrials
tstamps(ii,:)= linspace(dd.cam.onsets(ii),dd.cam.onsets(ii)+(recwind/samp_freq),recwind);
inhstamps = tstamps(ii,dd.breath.inh_onsets{ii});
ninhstamps = tstamps(ii,dd.breath.inh_onsets{ii});

finhtime(ii) = inhstamps(find(inhstamps>aligned_onsets(ii)+0.3,1,'first'));
sinhtime(ii) = inhstamps(find(inhstamps>finhtime(ii),1,'first'));
tinhtime(ii) = inhstamps(find(inhstamps>sinhtime(ii),1,'first'));

ninh(ii) = length(inhstamps(inhstamps>=dd.events.onsets(ii)&inhstamps<=dd.events.onsets(ii)+2));
% tmpidx = find(tstamps(ii,:)>=dd.events.onsets(ii));
tmpidx = find(tstamps(ii,:)>=finhtime(ii));
inhtimes{ii} =  inhstamps(find(inhstamps>dd.events.onsets(ii)+0.2));

tmp = dd.breath.cam_resp{ii}(tmpidx(1:tt));
opol = 6;
t = 1:length(tmp);
[p,s,mu] = polyfit(t,tmp,opol);
f_y = polyval(p,t,[],mu);

c= tmp - f_y;
% clf
% plot(c,'r'),hold on
% yyaxis('right')
% plot(tmp,'b')
% keyboard
bidx(ii,:) = (c-min(c))/(max(c)-min(c)) ;
end

%%
clf
ax = [];
c = 14;
nbins = 21;
tts_trials{c} = extract_trial_spikes(dd.ts{c},finhtime-.5,finhtime+1);
[b, in] = sort(sinhtime-finhtime,'ascend');
% in = dd.trial_idx;

tmp_spk = {};
for ij = 1:length(tts_trials{c})
    tmp_spk{ij} = tts_trials{c}{ij}-diffevents(ij);
end

sniffsortedspk = tmp_spk(in)';
% sniffsortedbreath = bidx(in,:);
diffinh = (sinhtime-finhtime);
sdiffinh = (tinhtime-finhtime);

ax = [ax,subplot(3,1,1)];
plot_rastergram(sniffsortedspk,0,1,'line'),hold all
plot(diffinh(in)+.5,1:length(sinhtime),'color',[.7 .7 .7])
plot(sdiffinh(in)+.5,1:length(sinhtime),'color',[.7 .7 .7])
plot([0.5 0.5],[1 length(sinhtime)],'k')


ax = [ax subplot(3,1,2)];
spks = sniffsortedspk;
% check individual spike trains


% edges = (0:dt:stimduration);
% edges = (0:dt:stimduration);
frestimate = zeros(nbins,length(spks));
cyclestimate = nan(nbins,length(spks));
for trial = 1:length(spks)
    
    edges = linspace(0.5,diffinh(trial)+.5,nbins)';
    dt = min(diff(edges));
    tmp = histc(spks{trial}, edges)./dt;
    if ~isempty(tmp)
        frestimate(:,trial) = conv(tmp,kernel,'same');
        cyclestimate(:,trial) = tmp;
    end
end

sigma = 5*1e-3;
binsize = 4;
dt = binsize*1e-3;

% Estimates firing rate by convolving the binary spiketrains with a
% gaussian kernel
kernel = normpdf(-sigma*5:dt:5*sigma,0,sigma);
kernel = kernel/sum(kernel);
 
mm = mean(cyclestimate(1:end-1,:)');
convolved = conv(mm,kernel,'same');

% imagesc(flipud(frestimate'))
piedges = linspace(0,2*pi,nbins);hold all
plot(piedges(1:end-1),mm);
plot(piedges(1:end-1),convolved)
hold on
plot(piedges(1:end-1),(cos(piedges(1:end-1))*4+mean(mm)))
% polar(piedges(1:end-1),mean(cyclestimate'))

subplot(3,1,3)

polar([piedges],[mm mm(1) ])

% in = dd.trial_idx;
% sniffsortedspk = tts_trials{c}(in);
% sniffsortedbreath = bidx(in,:);
% diffinh = (tinhtime-sinhtime);
% plot_rastergram(sniffsortedspk,0,1,'line'),hold all
% plot(diffinh(in)+.5,1:length(sinhtime),'color',[.7 .7 .7])
% plot([0.5 0.5],[1 length(sinhtime)],'k')


set(ax(1),'xlim',[0 1.5],'ylim',[0 length(sinhtime)])

%%
spks = tts_trials{c};
% check individual spike trains

sigma = 5*1e-3;
binsize = 1;
stimduration = 3;

dt = binsize*1e-3;

% Estimates firing rate by convolving the binary spiketrains with a
% gaussian kernel
kernel = normpdf(-sigma*5:dt:5*sigma,0,sigma);
kernel = kernel/sum(kernel);

edges = (0:dt:stimduration);
% edges = (0:dt:stimduration);
frestimate = zeros(length(edges),length(spks));
for trial = 1:length(spks)
    % This mess is just to deal with empty spiketrains because matlab
    % sucks.
    tmp = histc(spks{trial}, edges)./dt;
    if ~isempty(tmp)
        frestimate(:,trial) = conv(tmp,kernel,'same');
    end
end


%%
clf,

trial = 73;     
tt = 180;
plot(frestimate(:,trial));
b = bidx(trial,:);
tmpidx = find(tstamps(trial,:)>=finhtime(trial));
t = tstamps(trial,1:tt);
ttmp = (t-min(t)).*1000;
hold on
plot(ttmp,b.*50,'r')

%% check the phase difference between 2 signals

N = size(vv, 1);
nfft = 2^nextpow2(2*N-1);
F=fft(vv(:,2), nfft);
M=fft(vv(:,3), nfft);

R=F.*conj(M);


%%
all_sniff = [];
% frestimate = zeros(length(edges),length(spks));
frestimate = {};
dt = {};
spks = tts_trials{c};
sigma = 5*1e-3;
kernel = normpdf(-sigma*5:dt:5*sigma,0,sigma);
kernel = kernel/sum(kernel);

for ii = 1:length(inhtimes)
    tt = inhtimes{ii}- inhtimes{ii}(1);
    edges = tt(tt<3);
%     edges = inhtimes{ii}-inhtimes{ii}(1);
    dt{ii} = diff(edges);

    tmp = histc(spks{ii}, edges(2:end))./dt{ii}';
%     keyboard
    if ~isempty(tmp)
        frestimate{ii} = conv(tmp,kernel,'same');
    end
%     edges = min(t):
    
%     all_sniff = [all_sniff diff(tmp)];
end
%%



clf
for ii = 1:length(inhtimes)
    semilogx(dt{ii},frestimate{ii},'o'),hold on
end



%%
clf
dd = horzcat(dt{:});
edges = 0.01:0.01:1;
plot(dd,'o')
bb = histc(dd,edges);
bar(edges,bb)
% semilogx(edges,bb)
% plot(edges,bb)
% histc()

%%
clf
maxsniff = 10;
all_tri = nan(length(inhtimes),maxsniff);
for ii = 1:length(inhtimes)
%     if
   ll = length(frestimate{ii});
   all_tri(ii,1:ll) = frestimate{ii};
end

imagesc(all_tri)

%%

cc = colormap(jet(16));
printfigure = true;

for iexpt =  1%1:length(data)
    
    dd = data(iexpt);
    
    ttypes = unique(dd.trial_type);
    ntypes = length(ttypes);
    
    if ~isempty(dd.ts)
        for c = 1:length(dd.ts)
            if sum(dd.f0{c})>0
            sorted_f0 = dd.f0{c}(dd.trial_idx);
            tf0 = [];
            sf0 = [];
            
            count = 0;
            for ii = 1:ntypes
                it = ttypes(ii);
                
                tf0(ii) = mean(sorted_f0(dd.trial_type==it));
                sf0(ii) = std(sorted_f0(dd.trial_type==it))./sqrt(sum(dd.trial_type==it));
                count = count+sum(dd.trial_type==it);
                line_sep(ii) = count;
                
            end
            clf
            ax = [];
            sig = nansum(dd.sig(c,:),2);
            sig_resp = dd.resp_h(c);
%             keyboard
            sig_val = nanmean(dd.sig_val(c,:),2);
            reg = dd.recording_site{c};
            
            
            keyboard
            t_b = cell2mat(dd.breath.cam_resp(dd.trial_idx)');
            
            
            ax = [ax,subplot(3,2,1)];
            errorbar(1:length(tf0),tf0,sf0);
            
            title(sprintf('exp %s cell %d site %s',dd.exp_name,c,reg),'fontsize',7)
            ylabel('Firing rate (spikes/s)','fontsize',7)
            set(ax(end),'xtick',[1:ntypes])
            xlim([0.9 ntypes+.1])
            
            ax = [ax,subplot(3,2,2)];
            rres = mean(tf0(1:7));
            rste = std(tf0(1:7))/sqrt(length(rres));
            lres = mean(tf0(7:end));
            lste = std(tf0(7:end))/sqrt(length(lres));
            mmax = max([max(rres+rste) max(lres+lste)]);
            e = errorbar(1:2,[rres lres],[rste lste]);
            set(e,'color','k')
            set(ax(end),'xlim',[0.9 2.1],'xtick',[1 2],'xticklabel',...
                {'Left','Right'},'ylim',[0 mmax+2])
            title(sprintf('sig %d sig_resp %1.5f',sig,sig_resp),'fontsize',7)
            ylabel('Firing rate (spikes/s)','fontsize',7)
            
            
            ax = [ax,subplot(3,2,[3 4 5 6])];
            sorted_trials = dd.ts_trials{c}(dd.trial_idx);
            plot_rastergram(sorted_trials,0,1,'convimg','color',[0 0 0]),hold all
            for ii = 1:length(line_sep)
                plot([1 5000],[line_sep(ii) line_sep(ii)],'color',cc(ii,:),'linewidth',1)
            end
            plot([-1 -1],[0 find(dd.trial_type==7,1,'last')],'k')
            set(ax(end),'visible','off')
%             keyboard
           
            
            set(ax,'box','off','fontsize',7)
             if printfigure
         set(gcf,'papersize',[10,12],'paperposition',[0,0,10,12],'color','white','paperunits','centimeters')
         figfolder = 'figures/cell_rasters';
%         animal = '170912_JMCA001';
       print(gcf,'-dpdf',sprintf('%s/%s_cell_%d_cond_%s.pdf',figfolder,dd.exp_name,c,reg))
            
             end
%         keyboard
            end
      
    end
    end
    
    
end
